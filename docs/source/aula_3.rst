Equações de Poisson e Laplace
=============================

A determinação do potencial e o campo elétrico por meio de duas
definições é viável somente no caso em que a distribuição de carga
apresenta alta simetria. Este é o caso das distribuições que
consideramos até aqui onde tínhamos um fio, um disco, etc., e
pretendíamos calcular o potencial (ou o campo elétrico) em um ponto
conveniente do espaço para que as integrações apresentem uma forma
simples. Com isso, no caso mais geral onde temos distribuições
arbitrárias de carga precisamos lançar mão de uma metodologia mais
geral. É neste sentido que iremos considerar as equações de Poisson e
Laplace que nos permitem obter soluções para o potencial elétrico em
situações muito mais elaboradas e gerais que consideramos até o momento.

Equações de Poisson e Laplace
-----------------------------

BConforme discutimos, uma dada densidade de carga :math:`\rho` dá origem
a um campo elétrico :math:`\vec{E}` dado pela lei de Gauss:

.. math::
   :label: gauss

   \begin{aligned}
   \nabla\cdot\vec{E}=\dfrac{\rho}{\epsilon_{0}}\end{aligned}

e considerando que o campo elétrico pode ser expresso com o gradiente do
potencial eletrostático :math:`\vec{E}=-\nabla\varphi`, então podemos
escrever a lei de Gauss d seguinte forma:

.. math::
   :label: Poisson

   \begin{aligned}
   \nabla^{2}\varphi=-\dfrac{\rho}{\epsilon_{0}}.\end{aligned}

A equação acima é uma equação diferencial parcial para o potencial
eletrostático e é chamada de **equação de Poisson**. Esta equação
permite determinar o potencial eletrostático correspondente a uma dada
densidade de carga :math:`\rho`. Assim, para resolver a equação
precisamos conhecer a forma da distribuição volumétrica de carga.

No caso mais geral, que vamos considerar aqui, a densidade de carga é
nula na maior parte do espaço. Este é caso quando lidamos com cargas
pontuais e condutores cuja densidade de carga se situa na superfície do
condutor. Neste caso, a equação de Poisson é simplificada fazendo-se
:math:`\rho=0` o que nos permite escrever:

.. math::
   :label: Laplace

   \begin{aligned}
   \nabla^{2}\varphi=0.\end{aligned}

E a :eq:`Laplace` é chamada **equação de Laplace**. Esta
equação é novamente resolvida especificando-se o potencial elétrico em
determinadas regiões do espaço de modo que o potencial geral obtido a
partir da :eq:`Laplace` deve satisfazer tais condições.
Estas são as chamadas condições de contorno do problema.

Solução da Equação de Laplace
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A solução da equação diferencial dada pela :eq:`Laplace`
é obtida primeiramente escrevendo-se o Laplaciano :math:`\nabla^{2}` em
um determinado sistema de coordenadas. Obviamente as soluções terão
formas diferentes em diferentes sistemas de coordenadas. Infelizmente,
devido ao tempo exíguo não iremos discutir a solução da equação de
Laplace em detalhes, porém, o leitor pode verificar a solução em
qualquer livro de equações diferenciais. Em linhas gerais, uma equação
diferencial parcial é uma equação para uma função de várias variáveis.
No caso presente, o potencial elétrico é função das coordenadas
espaciais, ou seja, :math:`\varphi=\varphi(x,y,z)` no caso do sistema de
coordenadas retangulares. No caso da equação de Laplace, a estratégia de
solução baseia-se em supor que a função de várias variáveis é um produto
de três funções de uma variável. Com isso, é possível transformar a
equação diferencial parcial em três equações diferenciais ordinárias que
podem ser facilmente resolvidas pelos métodos padrão de equações
diferenciais.

Neste curso vamos focar na solução da equação de Laplace em coordenadas
esféricas e coordenadas cilíndricas. Isso irá nos permitir explorar
vários problemas relevantes que aparecem em teoria eletromagnética.

Solução da Equação de Laplace em Coordenadas Esféricas
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Em coordenadas esféricas, o potencial eletrostático é função das
coordenadas :math:`r`, :math:`\theta` e :math:`\phi`. Assim, podemos
escrever :math:`\varphi=\varphi(r,\theta,\phi)` e o laplaciano em
coordenadas esféricas pode ser escrito da seguinte forma:

.. math::
   :label: laplaciano:esfericas

   \begin{aligned}
   \nabla^{2}\varphi=\dfrac{1}{r^{2}}\dfrac{\partial}{\partial r}\left(r^{2}\dfrac{\partial\varphi}{\partial r}\right)
   +
   \dfrac{1}{r^{2}\sin\theta}\dfrac{\partial}{\partial\theta}\left(\sin\theta\dfrac{\partial\varphi}{\partial\theta}\right)
   +
   \dfrac{1}{r^{2}\sin^{2}\theta}\dfrac{\partial^{2}\varphi}{\partial\phi^{2}}\end{aligned}

e, portanto, a :eq:`Laplace` torna-se:

.. math::
   :label: eq:laplace:esfericas

   \begin{aligned}
   \dfrac{1}{r^{2}}\dfrac{\partial}{\partial r}\left(r^{2}\dfrac{\partial\varphi}{\partial r}\right)
   +
   \dfrac{1}{r^{2}\sin\theta}\dfrac{\partial}{\partial\theta}\left(\sin\theta\dfrac{\partial\varphi}{\partial\theta}\right)
   +
   \dfrac{1}{r^{2}\sin^{2}\theta}\dfrac{\partial^{2}\varphi}{\partial\phi^{2}}=0\end{aligned}

e precisamos achar uma função :math:`\varphi=\varphi(r,\theta,\phi)` que
substituída na :eq:`eq:laplace:esfericas`
resulte em uma valor nulo. Para isso, assumimos que
:math:`\varphi=R(r)\Theta(\theta)\Phi(\phi)` o que nos permite reduzir a
equação acima em três equações diferenciais ordinárias separadas para
:math:`R`, :math:`\Theta` e :math:`\Phi`. Com a solução destas equações
diferenciais, podemos escrever a solução de geral da equação diferencial
parcial da seguinte forma:

.. math::
   :label: solucaogeral:esfericas

   \begin{aligned}
   \varphi(r,\theta, \phi)=(a_{0}+b_{0}\phi)\sum_{l=0}^{\infty}(C_{l}r^{l}+D_{l}r^{-(l+1)})P_{l}(\cos\theta)\end{aligned}

que é válida somente para casos em que termos simetria axial em um torno
de um eixo, como é caso de um esfera, um hemisfério, um anel, disco.
etc. É importante relembrar aqui os intervalos em que as variáveis estão
definidas: :math:`0<\phi<2\pi`, :math:`0<\theta<\pi` e
:math:`0<r<\infty`. Assim, em problemas em que :math:`\phi` pode variar
indefinidamente, então devemos fazer a constante :math:`b_{0}=0`. As
demais constantes :math:`a_{0}` e :math:`C_{l}` e :math:`D_{l}` são
determinadas a partir das condições de contorno o problema. Finalmente,
:math:`P_{l}(\theta)` são os chamados polinômios de Legendre. Abaixo,
citamos alguns exemplos:

.. math::

   \begin{aligned}
   \begin{array}{|c|c|}\hline
   l&P_{l}(\cos\theta)
   \\\hline
   0&1\\
   1&\cos\theta\\
   2&\dfrac{1}{2}(3\cos^{2}\theta-1)\\
   3&\dfrac{1}{2}(5\cos^{3}\theta-3\cos\theta)\\
   \vdots&\vdots\\\hline
   \end{array}\end{aligned}

Vamos considerar alguns exemplos de aplicação da :eq:`solucaogeral:esfericas` que nos permite
determinar o potencial eletrostático em sistemas simples.

Exemplos
^^^^^^^^

**1.** *Considere um campo elétrico uniforme*
:math:`\vec{E}=E_{0}\hat{\mathbf{k}}`, *produzido, por exemplo, por duas
placas metálicas cujo campo já calculamos anteriormente. Dentro da
região entre as duas placas metálicas é colocada uma esfera condutora
descarregada e isolada, de raio* :math:`R`. *O raio da esfera condutora,
quando comparada com a distância entre as placas, é tal que ela não
perturba a uniformidade do campo nas regiões afastadas dela, mas, na
regiões próximas, o campo é afetado. Dentro destas condições, determine
o potencial na região externa à esfera.*


.. _laplaceesfera:

.. figure:: _images/LaplaceSphere.png
      :scale: 51%
      :align: center

      Esfera condutora imersa em um campo elétrico uniforme :math:`\vec{E}=E_{0}\hat{\mathbf{k}}`, o eixo :math:`z` está na direção horizontal valores positivos de :math:`z` para a direita).


.. centered:: Solução 

   

Um esquema mostrando as linhas de campo e a esfera imersa no mesmo é
mostrado na :numref:`laplaceesfera`. Para isso, notamos então que as
linhas do campo elétrico são absolutamente deformadas próximo da esfera
condutora mas tendem a se tornar uniformes longe da mesma. Assim, a
primeira condição de contorno do problema é que o campo elétrico é dado
por :math:`\vec{E}=E_{0}\hat{\mathbf{k}}` para :math:`r\gg R`. Vemos que
o problema apresenta simetria axial, pois a distribuição do campo
elétrico não muda quando giramos em torno do eixo :math:`z` que é
definido conforme mostrado na Fig. `1.1 <#lapal:fig1>`__. Com isso,
podemos notar que o ângulo :math:`\phi` (definido no plano perpendicular
a :math:`z`) é ilimitado de modo que podemos fazer :math:`b_{0}=0` na
solução da equação de Laplace. Assim, podemos escrever:

.. math::
   :label: exemplo1:eq1

   \begin{aligned}
   \varphi(r,\theta, \phi)=\sum_{l=0}^{\infty}(C_{l}r^{l}+D_{l}r^{-(l+1)})P_{l}(\cos\theta)\end{aligned}

onde absorvemos a constante :math:`a_{0}` nas constantes :math:`C_{l}` e
:math:`D_{l}`.

Vamos considerar agora a condição de contorno para :math:`r\gg R`:
primeiro, como temos uma equação para o potencial elétrico, devemos
determinar o forma que o potencial deve ter para gerar um campo elétrico
constante no infinito. Para obter tal potencial, calculamos o potencial
a partir do campo elétrico de acordo com

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\varphi(\vec{r}_{0})-\int_{\vec{r}_{0}}^{\vec{r}}\vec{E}\cdot d\vec{r}\end{aligned}

considerando que a esfera é condutora forma uma superfície equipotencial
e assim o potencial na superfície da esfera é uma constante
:math:`\varphi_{0}`. Com a esfera está descarregada, devemos no final
fazer :math:`\varphi_{0}=0`. Para resolver a integral usamos
:math:`d\vec{r}=dz\hat{\mathbf{k}}` de modo que

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\varphi_{0}-\int_{0}^{z}E_{0}\hat{\mathbf{k}}\cdot dz\hat{\mathbf{k}}\end{aligned}

onde estendemos o limite da integração até zero porque o potencial no
interior da esfera é constante. Assim, ficamos com a solução final

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\varphi_{0}-E_{0}z\end{aligned}

e, em coordenadas esféricas, :math:`z=r\cos\theta` o que nos permite
escrever:

.. math::
   :label: exemplo1:eq2

   \begin{aligned}
   \varphi(\vec{r})=\varphi_{0}-E_{0}r\cos\theta,\qquad r\gg R.\end{aligned}

Uma vez que temos a expressão de interesse, sabemos que o potencial deve ser reduzido à :eq:`exemplo1:eq2`  quando fazemos :math:`r\gg R`. Para isso, começamos escrevendo o potencial dado pela :eq:`exemplo1:eq1` explicitamente:

.. math::

   \begin{gathered}
   \varphi(r,\theta, \phi)
   =
   (C_{0}+D_{1}r^{-1})P_{0}(\cos\theta)+(C_{1}r+D_{1}r^{-2})P_{1}(\cos\theta)
   +
   (C_{2}r^{2}+D_{2}r^{-3})P_{2}(\cos\theta)\\+(C_{3}r^{3}+D_{l}r^{-4})P_{4}(\cos\theta)+\cdots\end{gathered}

e substituindo os polinômnios correspondentes, temos ainda:

.. math::

   \begin{gathered}
   \varphi(r,\theta, \phi)
   =
   C_{0}+D_{1}r^{-1}+C_{1}r\cos\theta+D_{1}r^{-2}\cos\theta
   +
   \dfrac{1}{2}C_{2}r^{2}(3\cos^{2}\theta-1)\\+\dfrac{1}{2}D_{2}r^{-3}(3\cos^{2}\theta-1)+\cdots\end{gathered}

e fazendo :math:`r\gg R` podemos escrever:

.. math::

   \begin{aligned}
   \varphi(r\gg R,\theta, \phi)
   \sim
   C_{0}+C_{1}r\cos\theta
   +
   \dfrac{1}{2}C_{2}r^{2}(3\cos^{2}\theta-1)+\cdots\end{aligned}

e devemos comparar com a condição de contorno obtida acima. Assim,
podemos escrever:

.. math::

   \begin{aligned}
   \varphi_{0}-E_{0}r\cos\theta=C_{0}+C_{1}r\cos\theta
   +
   \dfrac{1}{2}C_{2}r^{2}(3\cos^{2}\theta-1)+\cdots\end{aligned}

e comparando obtemos :math:`\varphi_{0}=C_{0}`, :math:`-E_{0}r=C_{1}` e
:math:`C_{l}=0`, :math:`l>1.`

Com isso, podemos escrever a solução da seguinte forma:

.. math::

   \begin{aligned}
   \varphi(r,\theta, \phi)
   =
   \varphi_{0}-E_{0}r\cos\theta+D_{1}r^{-2}\cos\theta
   +
   \dfrac{1}{2}D_{2}r^{-3}(3\cos^{2}\theta-1)+\cdots\end{aligned}

Conforme havíamos comentado acima, o potencial na superfície da esfera é
constante e vale :math:`\varphi_{0}`. Assim, fazendo :math:`r=R`,
devemos ter :math:`\varphi(r,\theta, \phi)=\varphi_{0}`, o que nos
permite escrever:

.. math::

   \begin{aligned}
   \varphi_{0}
   =
   \varphi_{0}-E_{0}R\cos\theta+D_{1}R^{-2}\cos\theta
   +
   \dfrac{1}{2}D_{2}R^{-3}(3\cos^{2}\theta-1)+\cdots\end{aligned}

de onde obtemos:

.. math::

   \begin{aligned}
   \varphi_{0}-E_{0}R\cos\theta+D_{1}R^{-2}\cos\theta+\cdots
   =\varphi_{0}\end{aligned}

onde :math:`D_{2}, D_{3}, \cdots=0`. Com isso, podemos obter a constante
:math:`D_{1}`, a única diferente de zero. Segue então que:

.. math::

   \begin{aligned}
   -E_{0}R\cos\theta+D_{1}R^{-2}\cos\theta
   =0,\qquad D_{1}=E_{0}R^{3}.\end{aligned}

Substituindo estas definições, vamos obter:

.. math::

   \begin{aligned}
   \varphi(r,\theta, \phi)
   =
   \varphi_{0}-E_{0}r\cos\theta+E_{0}R^{3}r^{-2}\cos\theta\end{aligned}

.. math::

   \begin{aligned}
   \varphi(r,\theta, \phi)
   =
   \varphi_{0}-E_{0}r\cos\theta\left(1-\dfrac{R^{3}}{r^{3}}\right)\end{aligned}

considerando que a esfera está descarregada, então podemos fazer
:math:`\varphi=0` o que nos permite escrever finalmente:

.. math::

   \begin{aligned}
   \varphi(r,\theta, \phi)
   =
   -E_{0}r\cos\theta\left(1-\dfrac{R^{3}}{r^{3}}\right)\end{aligned}

.. math::

   \begin{aligned}
   \label{exemplo1:eq3}
   \varphi(r,\theta, \phi)
   =
   -E_{0}r\cos\theta+\dfrac{E_{0}R^{3}}{r^{2}}\cos\theta\end{aligned}

note que o primeiro termo é o potencial que gera o campo elétrico
constante e o segundo termo nada mais é do que o potencial induzido na
esfera. Vemos que a dependência do potencial com o inverso do quadrado é
característico do potencial de um dipolo. Com efeito, a aplicação do
campo elétrico em uma esfera condutora induz um momento de dipolo na
esfera. O potencial gerado pelo momento de dipolo é dado por

.. math::

   \begin{aligned}
   \varphi(r)=\dfrac{1}{4\pi\epsilon_{0}}\dfrac{pr\cos\theta}{r^{3}}=\dfrac{1}{4\pi\epsilon_{0}}\dfrac{p\cos\theta}{r^{2}}\end{aligned}

e comparando com o segundo termo podemos determinar o momento de dipolo
induzido:

.. math::

   \begin{aligned}
   p=4\pi\epsilon_{0}E_{0}R^{3}\end{aligned}

e desde que a orientação do momento de dipolo deve estar ao longo do
eixo :math:`z`, segue que:

.. math::

   \begin{aligned}
   \vec{p}=4\pi\epsilon_{0}E_{0}R^{3}\hat{\mathbf{k}}.\end{aligned}

Solução da Equação de Laplace em Coordenadas Cilíndricas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Em coordenadas cilíndricas, o laplaciano é escrito na forma:

.. math::

   \begin{aligned}
   \nabla^{2}\varphi=\dfrac{1}{\rho}\dfrac{\partial}{\partial\rho}\left(\rho\dfrac{\partial\varphi}{\partial\rho}\right)
   +
   \dfrac{1}{\rho^{2}}\dfrac{\partial^{2}\varphi}{\partial\phi^{2}}+\dfrac{\partial^{2}\varphi}{\partial z^{2}}\end{aligned}

de modo que a equação de Laplace pode ser escrita na forma:

.. math::
   :label: laplaciano:cilindricas
   
   \begin{aligned}
   \dfrac{1}{\rho}\dfrac{\partial}{\partial\rho}\left(\rho\dfrac{\partial\varphi}{\partial\rho}\right)
   +
   \dfrac{1}{\rho^{2}}\dfrac{\partial^{2}\varphi}{\partial\phi^{2}}+\dfrac{\partial^{2}\varphi}{\partial z^{2}}=0.\end{aligned}

As considerações sobre a metodologia de solução da equação de Laplace
são as mesmas que foram feitas para o caso das coordenadas esféricas. A
solução é dada por:

.. math::
   :label: solucao:cilindricas

   \begin{aligned}
   \varphi(\rho,\theta)=\left(A_{0}+B_{0}\ln\dfrac{\rho}{\rho_{0}}\right)(C_{0}+D_{0}\theta)+\sum_{\nu\neq0}(A_{\nu}\rho^{\nu}+B
   _{\nu}\rho^{-\nu})(C_{\nu}\cos\nu\theta+D_{\nu}\sin\nu\theta)\end{aligned}

que é válida para distribuições que produzam potenciais que não dependem
da coordenada :math:`z`. Esta solução é aplicável, portanto, a sistemas
como cilindros infinitos, fios, etc.

Os procedimentos utilizados para determinar o potencial para qualquer
distribuição de carga são os mesmos que foram realizados para a
determinação do potencial par distribuições com simetria esférica. A
seguir, vamos estudar alguns exemplos de aplicação.

.. _exemplos-1:

Exemplos
^^^^^^^^

**1.** *Considere um cilindro metálico, de raio* :math:`R` *e comprimento
infinito, imerso em um campo elétrico uniforme com direção perpendicular
ao eixo axial do cilindro. Determine o potencial elétrico para qualquer
ponto* :math:`\rho>R`.

.. centered:: Solução

Este problema é análogo ao caso da esfera condutora imersa no campo
elétrico, exceto que agora temos um cilindro. Vamos considerar que o
campo elétrico externo está dirigido ao longo da direção :math:`x`
enquanto o comprimento do cilindro está ao longo do eixo :math:`z`.
Assim, notamos que o campo elétrico pode ser escrito como
:math:`\vec{E}=E_{0}\hat{\mathbf{i}}` para distâncias muito maiores do
que o raio do cilindro. Precisamos identificar as condições de contorno
do problema. Para isso, notamos que o cilindro é um condutor e, deste
modo, o potencial deve ser constante sobre ele. Vamos assumir que o
potencial para :math:`\varphi(\rho\leq R,\theta)=\varphi_{0}`. Esta é
uma das condições de contorno. Notamos também que o campo deve ter
simetria cilíndrica, ou seja, o potencial deve ter o mesmo valor quando
:math:`\theta` varia de :math:`2\pi`. Deste modo, o potencial não pode
depender linearmente com :math:`\theta`. Obtemos ainda uma terceira
condição de contorno para :math:`\rho\gg R` onde o campo elétrico deve
ser uniforme e dado por :math:`\vec{E}=E_{0}\hat{\mathbf{i}}`. Neste
caso, podemos integrar a relação :math:`\vec{E}=-\nabla\varphi` para
obter a equação para o potencial correspondente. Explicitamente,
obtemos:

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\varphi(\vec{r}_{0})-\int_{\vec{r}_{0}}^{\vec{r}}\vec{E}\cdot d\vec{r},\end{aligned}

e considerando que o cilindro é condutor :math:`\varphi=\varphi_{0}`
para :math:`\rho\leq R`. Assim, podemos usar o ponto de referência
:math:`\hat{r}_{0}` como sendo a origem do sistema de coordenadas, i.e.,
:math:`\hat{r}_{0}=0`. Além disso, :math:`\vec{E}=E_{0}\hat{\mathbf{i}}`
o que implica em :math:`\vec{E}\cdot d\vec{r}=E_{0}dx`

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\varphi_{0}-\int_{0}^{x}E_{0}dx\end{aligned}

e resolvendo a integral, obtemos:

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\varphi_{0}-E_{0}x\end{aligned}

e, em coordenadas cilíndricas, :math:`x=\rho\cos\theta` o que nos
permite escrever:

.. math::
   :label: exemplo2:eq1
   
   \begin{aligned}
   \varphi(\vec{r})=\varphi_{0}-E_{0}\rho\cos\theta,\qquad r\gg R.\end{aligned}

Assim, podemos resumir as condições de contorno conforme segue:

a. O potencial :math:`\varphi(\rho,\theta)` não depende linearmente de :math:`\theta` pois este é ilimitado;

b. para pontos muito distantes devemos ter um potencial dado pela :eq:`exemplo2:eq1`, i.e., :math:`\varphi(\rho\gg R,\theta)=\varphi_{0}-E_{0}\rho\cos\theta`;

c. :math:`\varphi(\rho=R,\theta)=\varphi_{0}`

Vamos agora aplicar estas condições à solução geral dada pela :eq:`solucao:cilindricas`, ou seja,

.. math::

   \begin{aligned}
   \varphi(\rho,\theta)=\left(A_{0}+B_{0}\ln\dfrac{\rho}{\rho_{0}}\right)(C_{0}+D_{0}\theta)+\sum_{\nu\neq0}(A_{\nu}\rho^{\nu}+B
   _{\nu}\rho^{-\nu})(C_{\nu}\cos\nu\theta+D_{\nu}\sin\nu\theta)\end{aligned}

e a partir da condição dada por (a), devemos fazer :math:`D_{0}=0`.
Neste caso, segue que:

.. math::
   :label: exemplo2:eq2
   
   \begin{aligned}
   \varphi(\rho,\theta)=A^{\prime}_{0}+B^{\prime}_{0}\ln\dfrac{\rho}{\rho_{0}}+\sum_{\nu\neq0}(A_{\nu}\rho^{\nu}+B
   _{\nu}\rho^{-\nu})(C_{\nu}\cos\nu\theta+D_{\nu}\sin\nu\theta)\end{aligned}

onde :math:`A^{\prime}_{0}=A_{0}C_{0}` e
:math:`B^{\prime}_{0}=B_{0}C_{0}` são as novas constantes. Podemos
novamente renomeá-las de :math:`A_{0}` e :math:`B_{0}` desde que as
constantes são desconhecidas e arbitrárias até que se aplique as
condições de contorno.

Vamos agora aplicar a condição de contorno (b). Para isso, vamos
escrever explicitamente alguns dos termos da solução dada pela :eq:`exemplo2:eq2`,

.. math::
   :label: exemplo2:eq3

   \begin{gathered}
   \varphi(\rho,\theta)=A^{\prime}_{0}+B^{\prime}_{0}\ln\dfrac{\rho}{\rho_{0}}
   +
   (A_{1}\rho+B_{1}\rho^{-1})(C_{1}\cos\theta+D_{1}\sin\theta)
   \\+
   (A_{2}\rho^{2}+B_{2}\rho^{-2})(C_{2}\cos2\theta+D_{2}\sin2\theta)
   +
   (A_{3}\rho^{3}+B_{3}\rho^{-3})(C_{3}\cos3\theta+D_{3}\sin3\theta)+\cdots\end{gathered}

e fazendo os produtos, podemos escrever:

.. math::
   :label: exemplo2:eq4

   \begin{gathered}
   \varphi(\rho,\theta)=A^{\prime}_{0}+B^{\prime}_{0}\ln\dfrac{\rho}{\rho_{0}}
   +
   A_{1}C_{1}\rho\cos\theta
   +
   A_{1}D_{1}\rho\sin\theta
   +
   B_{1}C_{1}\rho^{-1}\cos\theta
   +
   B_{1}D_{1}\rho^{-1}\sin\theta
   \\+
   C_{2}A_{2}\rho^{2}\cos2\theta
   +
   D_{2}A_{2}\rho^{2}\sin2\theta
   +
   C_{2}B_{2}\rho^{-2}\cos2\theta
   +
   D_{2}B_{2}\rho^{-2}\sin2\theta
   \\+
   C_{3}A_{3}\rho^{3}\cos3\theta
   +
   D_{3}A_{3}\rho^{3}\sin3\theta
   +
   C_{3}B_{3}\rho^{-3}\cos3\theta
   +
   D_{3}B_{3}\rho^{-3}\sin3\theta+\cdots,\end{gathered}

e aqui novamente podemos definir novas constantes para agrupar os
produtos arbitrários, assim, definimos
:math:`A^{\prime}_{1}=A_{1}C_{1}`, :math:`A^{\prime}_{2}=C_{2}A_{2}`,
:math:`A^{\prime}_{3}=C_{3}A_{3}`, :math:`B^{\prime}_{1}=A_{1}D_{1}`,
:math:`B^{\prime}_{2}=A_{2}D_{2}`, :math:`B^{\prime}_{3}=A_{3}D_{3}`,
:math:`C^{\prime}_{1}=B_{1}C_{1}`, :math:`C^{\prime}_{2}=C_{2}B_{2}`,
:math:`C^{\prime}_{3}=C_{3}B_{3}`, :math:`D^{\prime}_{1}=B_{1}D_{1}`,
:math:`D^{\prime}_{2}=B_{2}D_{2}`, :math:`D^{\prime}_{3}=B_{3}D_{3}`,
:math:`\cdots`

.. math::
   :label: exemplo2:eq5

   \begin{gathered}
   \varphi(\rho,\theta)=A^{\prime}_{0}+B^{\prime}_{0}\ln\dfrac{\rho}{\rho_{0}}
   +
   A^{\prime}_{1}\rho\cos\theta
   +
   B^{\prime}_{1}\rho\sin\theta
   +
   C^{\prime}_{1}\rho^{-1}\cos\theta
   +
   D^{\prime}_{1}\rho^{-1}\sin\theta
   \\+
   A^{\prime}_{2}\rho^{2}\cos2\theta
   +
   B^{\prime}_{2}\rho^{2}\sin2\theta
   +
   C^{\prime}_{2}\rho^{-2}\cos2\theta
   +
   D^{\prime}_{2}\rho^{-2}\sin2\theta
   \\+
   A^{\prime}_{3}\rho^{3}\cos3\theta
   +
   B^{\prime}_{3}\rho^{3}\sin3\theta
   +
   C^{\prime}_{3}\rho^{-3}\cos3\theta
   +
   D^{\prime}_{3}\rho^{-3}\sin3\theta+\cdots.\end{gathered}

É importante notar que aparece uma constante :math:`\rho_{0}` no termo
envolvendo o logaritmo de :math:`\rho/\rho_{0}`; este deve ser o valor
para o qual conhecemos o potencial. Sabemos que para
:math:`\varphi=\varphi_{0}`, :math:`\rho=\rho_{0}`. Deste modo, podemos
fazer :math:`\rho_{0}=R`. Assim, a :eq:`exemplo2:eq5` pode ser escrita na forma:

.. math::
   :label: exemplo2:eq6

   \begin{gathered}
   \varphi(\rho,\theta)=A^{\prime}_{0}+B^{\prime}_{0}\ln\dfrac{\rho}{R}
   +
   A^{\prime}_{1}\rho\cos\theta
   +
   B^{\prime}_{1}\rho\sin\theta
   +
   C^{\prime}_{1}\rho^{-1}\cos\theta
   +
   D^{\prime}_{1}\rho^{-1}\sin\theta
   \\+
   A^{\prime}_{2}\rho^{2}\cos2\theta
   +
   B^{\prime}_{2}\rho^{2}\sin2\theta
   +
   C^{\prime}_{2}\rho^{-2}\cos2\theta
   +
   D^{\prime}_{2}\rho^{-2}\sin2\theta
   \\+
   A^{\prime}_{3}\rho^{3}\cos3\theta
   +
   B^{\prime}_{3}\rho^{3}\sin3\theta
   +
   C^{\prime}_{3}\rho^{-3}\cos3\theta
   +
   D^{\prime}_{3}\rho^{-3}\sin3\theta+\cdots\end{gathered}

e agora estamos em posição de aplicar a condição de contorno (b). Assim,
fazendo :math:`\rho\gg R` na :eq:`exemplo2:eq6`
assume a seguinte forma assintótica:

.. math::
   :label: exemplo2:eq7

   \begin{gathered}
   \varphi(\rho\gg R,\theta)=A^{\prime}_{0}+B^{\prime}_{0}\ln\dfrac{\rho}{R}
   +
   A^{\prime}_{1}\rho\cos\theta
   +
   B^{\prime}_{1}\rho\sin\theta
   +
   A^{\prime}_{2}\rho^{2}\cos2\theta
   +
   B^{\prime}_{2}\rho^{2}\sin2\theta
   \\+
   A^{\prime}_{3}\rho^{3}\cos3\theta
   +
   B^{\prime}_{3}\rho^{3}\sin3\theta
   \cdots\end{gathered}

e substituindo
:math:`\varphi(\rho\gg R,\theta)=\varphi_{0}-E_{0}\rho\cos\theta` no
primeiro membro, obtemos:

.. math::
   :label: exemplo2:eq8
   
   \begin{gathered}
   \varphi_{0}-E_{0}\rho\cos\theta=A^{\prime}_{0}+B^{\prime}_{0}\ln\dfrac{\rho}{R}
   +
   A^{\prime}_{1}\rho\cos\theta
   +
   B^{\prime}_{1}\rho\sin\theta
   +
   A^{\prime}_{2}\rho^{2}\cos2\theta
   +
   B^{\prime}_{2}\rho^{2}\sin2\theta
   \\+
   A^{\prime}_{3}\rho^{3}\cos3\theta
   +
   B^{\prime}_{3}\rho^{3}\sin3\theta
   \cdots\end{gathered}

e comparando ambos os membros da :eq:`exemplo2:eq8`
segue que :math:`A^{\prime}_{0}=\varphi_{0}`,
:math:`A^{\prime}_{1}=-E_{0}`, :math:`B^{\prime}_{0}=0`,
:math:`B^{\prime}_{1}=0` e :math:`A_{l}=B_{l}=0` para :math:`l\geq2`.
Com estas quantidades, a solução geral dada pela :eq:`exemplo2:eq6` assume a forma:

.. math::
   :label: exemplo2:eq9
   
   \begin{gathered}
   \varphi(\rho,\theta)=\varphi_{0}
   -E_{0}\rho\cos\theta
   +
   C^{\prime}_{1}\rho^{-1}\cos\theta
   +
   D^{\prime}_{1}\rho^{-1}\sin\theta
   +
   C^{\prime}_{2}\rho^{-2}\cos2\theta
   +
   D^{\prime}_{2}\rho^{-2}\sin2\theta
   \\+
   C^{\prime}_{3}\rho^{-3}\cos3\theta
   +
   D^{\prime}_{3}\rho^{-3}\sin3\theta+\cdots.\end{gathered}

Agora resta aplicar a condição de contorno (c), assim, segue que:

.. math::
   :label: exemplo2:eq9
   
   \begin{gathered}
   \varphi(R,\theta)=\varphi_{0}
   -E_{0}R\cos\theta
   +
   C^{\prime}_{1}R^{-1}\cos\theta
   +
   D^{\prime}_{1}R^{-1}\sin\theta
   +
   C^{\prime}_{2}R^{-2}\cos2\theta
   +
   D^{\prime}_{2}R^{-2}\sin2\theta
   \\+
   C^{\prime}_{3}R^{-3}\cos3\theta
   +
   D^{\prime}_{3}R^{-3}\sin3\theta+\cdots=\varphi_{0},\end{gathered}

desde que :math:`\varphi(R,\theta)=\varphi_{0}`. Novamente, para que a
igualdade seja satisfeita, devemos ter :math:`C_{l}=0` para
:math:`l\geq2=0` e :math:`D_{l}=0` com :math:`l\geq1`. Resta então
determinar :math:`C^{\prime}_{1}` cujo valor é dado pela seguinte
igualdade:

.. math::

   \begin{aligned}
   \label{exemplo2:eq10}
   \varphi_{0}=\varphi_{0}-E_{0}R\cos\theta+C^{\prime}_{1}R^{-1}\cos\theta\end{aligned}

e portanto,

.. math::

   \begin{aligned}
   C^{\prime}_{1}=E_{0}R^{2}.\end{aligned}

Finalmente, a :eq:`exemplo2:eq9`, que é a nossa solução geral, fica na forma final:

.. math::
   :label: ex2:result
   
   \begin{aligned}
   \varphi(R,\theta)=\varphi_{0}-E_{0}\rho\cos\theta
   +
   E_{0}R^{2}\rho^{-1}\cos\theta\end{aligned}

e notamos que o último termo corresponde ao potencial induzido no
cilindro devido ao campo magnético externo. Podemos escrever a solução
dada pela :eq:`ex2:result` na forma mais compacta:

.. math::

   \begin{aligned}
   \label{ex2:result2}
   \varphi(R,\theta)=\varphi_{0}-E_{0}\rho\cos\theta\left(1-\dfrac{R^{2}}{\rho^{2}}\right).\end{aligned}

**2.** *Considere dois cilindros coaxiais de raios* :math:`R_{a}` *e*
:math:`R_{b}`, *tal que* :math:`R_{b}>R_{a}`, *que apresentam potenciais
constantes denotados por* :math:`\varphi_{a}` *e* :math:`\varphi_{b}`,
*respectivamente. Determine o potencial na região entre os cilindros.*

.. centered:: Solução

Aqui as condições de contorno são óbvias:

(a) :math:`\varphi(\rho=R_{a},\theta)=\varphi_{a}`,

(b) :math:`\varphi(\rho=R_{b},\theta)=\varphi_{b}`.

A solução geral é dada pela :eq:`solucao:cilindricas` a qual repetimos aqui,
por conveniência:

.. math::

   \begin{aligned}
   \varphi(\rho,\theta)=\left(A_{0}+B_{0}\ln\dfrac{\rho}{\rho_{0}}\right)(C_{0}+D_{0}\theta)+\sum_{\nu\neq0}(A_{\nu}\rho^{\nu}+B
   _{\nu}\rho^{-\nu})(C_{\nu}\cos\nu\theta+D_{\nu}\sin\nu\theta),\end{aligned}

e notamos que devemos fazer :math:`D_{0}=0` visto que o potencial na
região entre os cilindros não pode variar quando :math:`\theta` varia de
um múltiplo de :math:`2\pi`. Com isso, podemos escrever a :eq:`solucao:cilindricas` na forma,

.. math::

   \begin{aligned}
   \varphi(\rho,\theta)=A^{\prime}_{0}+B^{\prime}_{0}\ln\dfrac{\rho}{R_{a}}+\sum_{\nu\neq0}(A_{\nu}\rho^{\nu}+B
   _{\nu}\rho^{-\nu})(C_{\nu}\cos\nu\theta+D_{\nu}\sin\nu\theta),\end{aligned}

onde :math:`A^{\prime}_{0}=A_{0}C_{0}` e
:math:`B^{\prime}_{0}=B_{0}C_{0}` são as novas constantes; aqui já
impomos também que :math:`\rho_{0}=R_{a}` como a distância de
referência, onde :math:`\varphi=\varphi_{a}`.

Vamos aplicar a condição de contorno (a) à equação acima, assim, fazendo
:math:`\rho=R_{a}`, segue que:

.. math::

   \begin{aligned}
   \varphi(\rho=R_{a},\theta)=A^{\prime}_{0}+B^{\prime}_{0}\ln\dfrac{R_{a}}{R_{a}}+\sum_{\nu\neq0}(A_{\nu}R_{a}^{\nu}+B
   _{\nu}R_{a}^{-\nu})(C_{\nu}\cos\nu\theta+D_{\nu}\sin\nu\theta),\end{aligned}

e como :math:`\ln 1=0`, obtemos:

.. math::

   \begin{aligned}
   \varphi_{a}=A^{\prime}_{0}+\sum_{\nu\neq0}(A_{\nu}R_{a}^{\nu}+B
   _{\nu}R_{a}^{-\nu})(C_{\nu}\cos\nu\theta+D_{\nu}\sin\nu\theta),\end{aligned}

onde já fizemos a troca no primeiro membro
:math:`\varphi(\rho=R_{a},\theta)=\varphi_{a}`. Desde que o primeiro
membro é uma constante, devemos fazer :math:`C_{\nu}=D_{\nu}=0` para
todos os valores de :math:`\nu`. Com efeito, isto elimina a dependência
com o ângulo :math:`\theta`. Notamos também que devemos ter

.. math::

   \begin{aligned}
   A^{\prime}_{0}=\varphi_{a}.\end{aligned}

A solução geral é então dada por:

.. math::
   :label: ex3:eq1

   \begin{aligned}
   \varphi(\rho)=\varphi_{a}+B^{\prime}_{0}\ln\dfrac{\rho}{R_{a}}.\end{aligned}

Agora resta aplicar a condição de contorno (b) na :eq:`ex3:eq1` para determinar :math:`B^{\prime}_{0}`. Assim,
segue que:

.. math::
   :label: ex3:eq2

   \begin{aligned}
   \varphi(\rho=R_{b},\theta)=\varphi_{a}+B^{\prime}_{0}\ln\dfrac{R_{b}}{R_{a}}=\varphi_{b}\end{aligned}

ou seja,

.. math::

   \begin{aligned}
   \varphi_{a}+B^{\prime}_{0}\ln\dfrac{R_{b}}{R_{a}}=\varphi_{b}\end{aligned}

o que nos permite escrever:

.. math::

   \begin{aligned}
   \varphi_{a}+B^{\prime}_{0}\ln\dfrac{R_{b}}{R_{a}}=\varphi_{b}\end{aligned}

ou ainda,

.. math::

   \begin{aligned}
   B^{\prime}_{0}=\dfrac{\varphi_{b}-\varphi_{a}}{\ln\dfrac{R_{b}}{R_{a}}}.\end{aligned}

Assim, substituindo-se :math:`B^{\prime}_{0}` na :eq:`ex3:eq1`, obtemos o resultado final:

.. math::

   \begin{aligned}
   \label{ex3:eq11}
   \varphi(\rho)=\varphi_{a}+\dfrac{(\varphi_{b}-\varphi_{a})}{\ln\dfrac{R_{b}}{R_{a}}}\ln\dfrac{\rho}{R_{a}}.\end{aligned}

Se tivéssemos utilizado :math:`\rho_{0}=R_{b}` como o parâmetro de
referência, teríamos obtido uma solução na forma:

.. math::

   \begin{aligned}
   \label{ex3:eq2}
   \varphi^{\prime}(\rho)=\varphi_{b}+\dfrac{(\varphi_{a}-\varphi_{b})}{\ln\dfrac{R_{a}}{R_{b}}}\ln\dfrac{\rho}{R_{b}}.\end{aligned}

As soluções dadas pelas :eq:`ex3:eq1` e
:eq:`ex3:eq2` descrevem o mesmo potencial entre os dois
cilindros. Desta forma, devem ser idênticas. Com efeito, podemos
verificar isso calculando a diferença:

.. math::

   \begin{aligned}
   \varphi(\rho)-\varphi^{\prime}(\rho)
   &=
   \varphi_{a}+\dfrac{(\varphi_{b}-\varphi_{a})}{\ln\dfrac{R_{b}}{R_{a}}}\ln\dfrac{\rho}{R_{a}}
   -
   \left[\varphi_{b}+\dfrac{(\varphi_{a}-\varphi_{b})}{\ln\dfrac{R_{a}}{R_{b}}}\ln\dfrac{\rho}{R_{b}}\right]
   \\
   &=
   \varphi_{a}-\varphi_{b}
   +\dfrac{(\varphi_{b}-\varphi_{a})}{\ln\dfrac{R_{b}}{R_{a}}}\ln\dfrac{\rho}{R_{a}}
   -\dfrac{(\varphi_{a}-\varphi_{b})}{\ln\dfrac{R_{a}}{R_{b}}}\ln\dfrac{\rho}{R_{b}}
   \\&=
   \varphi_{a}-\varphi_{b}
   +(\varphi_{b}-\varphi_{a})\left[\dfrac{1}{\ln\dfrac{R_{b}}{R_{a}}}\ln\dfrac{\rho}{R_{a}}
   +\dfrac{1}{\ln\dfrac{R_{a}}{R_{b}}}\ln\dfrac{\rho}{R_{b}}\right]\end{aligned}

ou ainda,

.. math::

   \begin{aligned}
   \varphi(\rho)-\varphi^{\prime}(\rho)
   &=
   \varphi_{a}-\varphi_{b}
   +(\varphi_{b}-\varphi_{a})\dfrac{1}{\ln\dfrac{R_{b}}{R_{a}}}\left[\ln\dfrac{\rho}{R_{a}}-\ln\dfrac{\rho}{R_{b}}\right]
   \\&=
   \varphi_{a}-\varphi_{b}
   +(\varphi_{b}-\varphi_{a})\dfrac{1}{\ln\dfrac{R_{b}}{R_{a}}}\ln\dfrac{R_{b}}{R_{a}}\end{aligned}

.. math::

   \begin{aligned}
   \varphi(\rho)-\varphi^{\prime}(\rho)
   =
   \varphi_{a}-\varphi_{b}
   +\varphi_{b}-\varphi_{a}=0\end{aligned}

e portanto,

.. math::

   \begin{aligned}
   \varphi(\rho)=\varphi^{\prime}(\rho).\end{aligned}

Isto mostra que as duas soluções são equivalentes, como queríamos
demonstrar.