O campo elétrico em meios dielétricos
=====================================

Nos capítulos anteriores, discutimos apenas campos elétricos no vácuo ou
em condutores. A partir de agora vamos considerar o campo elétrico em
meios dielétricos (ou isolantes) que se distinguem dos condutores por
não terem cargas livres em seu interior. Estes materiais, porém, se
constituem de estruturas atômicas que respondem ao campo elétrico
externo.

Os átomos são constituídos por elétrons (cargas negativas) e núcleos
atômicos (cargas positivas) e, assim, quando o campo elétrico é
aplicado, há um deslocamento de cargas: as cargas positivas no mesmo
sentido do campo e as cargas negativas no sentido contrário. O efeito
macroscópico é um deslocamento global de toda a carga positiva em
relação à negativa. Neste sentido, dizemos que o dielétrico está
*polarizado*.

Polarização
-----------

Considere um pedaço finito de um material dielétrico, de volume
:math:`V^{\prime}`, que está polarizado em uma certa direção, veja :numref:`voldieletrico`. Isso significa que o material tem a presença de
dipolos elétricos em seu interior que podem ser considerados puntuais.
Estes dipolos estão orientados na mesma direção do campo elétrico
externo aplicado (não ilustrado na :numref:`voldieletrico`). Assim,
um pequeno elemento de volume :math:`\Delta V^{\prime}` do material
dielétrico, localizado na posição :math:`\vec{r}^{\prime}`, produz um
potencial :math:`\Delta\varphi`, na posição :math:`\vec{r}`.
Considerando a expressão para o potencial elétrico devido a um dipolo
puntual, podemos escrever:

.. math::
   :label: deltaphi:eq1
   
   \begin{aligned}
   \Delta\varphi=\dfrac{1}{4\pi\varepsilon_{0}}
   \dfrac{\Delta\vec{p}\cdot(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}}.\end{aligned}

Na :eq:`deltaphi:eq1`, :math:`\Delta\vec{p}` é o
momento de dipolo do elemento de volume :math:`\Delta V^{\prime}`. Este
momento de dipolo é dado pela soma das contribuições de cada um dos
dipolos contidos no elemento de volume :math:`\Delta V^{\prime}`. Para
determinar a quantidade destes dipolos, definimos a polarização
:math:`\vec{P}(\vec{r})` que fornece a densidade de dipolos elétricos do
material dielétrico. A polarização é uma função definida para cada ponto
do dielétrico, deste modo, formalmente, definimos :math:`\vec{P}` pelo
processo limite:

.. math::
   :label: polarizacao
   
   \begin{aligned}
   \vec{P}=\lim_{\Delta V\rightarrow0}\left(\dfrac{\Delta \vec{p}}{\Delta V}\right).\end{aligned}



.. _voldieletrico:

.. figure:: _images/voldieletrico.png
      :scale: 51%
      :align: center

      Volume arbitrário de um material dielétrico, :math:`V^{\prime}`.

Considerando que o elemento de volume que aparece na :eq:`deltaphi:eq1` é pequeno o suficiente para que
:math:`\vec{P}(\vec{r})` não varie aprecialvelmente em seu interior,
então podemos escrever:

.. math::
   :label: deltaphi:eq2
   
   \begin{aligned}
   \Delta\varphi\approx\dfrac{1}{4\pi\varepsilon_{0}}
   \dfrac{\vec{P}(\vec{r}^{\prime})\cdot(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}}\Delta V^{\prime},\end{aligned}

A expressão acima pode ser estendida para o volume total
:math:`V^{\prime}` do dielétrico somando a contribuição de todos os
elementos de volume :math:`\Delta 
V^{\prime}`, assim, a :eq:`deltaphi:eq2` se torna:

.. math::
   :label: deltaphi:eq3
   
   \begin{aligned}
   \varphi(\vec{r})\approx\sum_{i=1}^{N=V^{\prime}/\Delta V^{\prime}}\dfrac{1}{4\pi\varepsilon_{0}}
   \dfrac{\vec{P}(\vec{r}_{i}^{\prime})\cdot(\vec{r}-\vec{r}_{i}^{\prime})}{|\vec{r}-\vec{r}_{i}^{\prime}|^{3}}\Delta V^{\prime},\end{aligned}

onde obtemos uma expressão aproximada devido ao fato dos elementos de
volume são finitos. Uma expressão exata é obtida tomando-se o limite
:math:`\Delta V^{\prime}\rightarrow0`:

.. math::
   :label: potencialpolarant

   \begin{aligned}
   \varphi(\vec{r})=\lim_{\Delta V^{\prime}\rightarrow0}\sum_{i=1}^{N=V^{\prime}/\Delta V^{\prime}}\dfrac{1}{4\pi\varepsilon_{0}}
   \dfrac{\vec{P}(\vec{r}_{i}^{\prime})\cdot(\vec{r}-\vec{r}_{i}^{\prime})}{|\vec{r}-\vec{r}_{i}^{\prime}|^{3}}\Delta V^{\prime},\end{aligned}

assim, segue que:

.. math::
   :label: potencialpolar

   \begin{aligned}
   \varphi(\vec{r})=\dfrac{1}{4\pi\varepsilon_{0}}\int_{V}
   \dfrac{\vec{P}(\vec{r}^{\prime})\cdot(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}} dV^{\prime},\end{aligned}

A :eq:`potencialpolar` nos permite obter o potencial dado que saibamos a forma da função :math:`\vec{P}(\vec{r})`
no volume :math:`V^{\prime}`. Porém, é conveniente reescrever esta expressão de outro modo explicitando as contribuições do volume e da
superfície do dielétrico. Para isso, relembramos a seguinte identidade vetorial:

.. math::

   \begin{aligned}
   \nabla^{\prime}\left[\dfrac{1}{|\vec{r}-\vec{r}^{\prime}|}\right]
   =
   \dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}},\end{aligned}

onde usamos a linha “:math:`~^{\prime}~`” para denotar que a derivada é
realizada nas variáveis com linha.

Esta identidade pode ser usada para reescrever o integrando da :eq:`potencialpolar` da seguinte maneira:

.. math::

   \begin{aligned}
   \dfrac{\vec{P}(\vec{r}^{\prime})\cdot(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}}
   =
   \vec{P}\cdot\nabla^{\prime}\left[\dfrac{1}{|\vec{r}-\vec{r}^{\prime}|}\right],\end{aligned}

e, além disso, temos ainda,

.. math::

   \begin{aligned}
   \nabla\cdot(f\vec{F})
   =
   f\nabla\cdot\vec{F}+\vec{F}\cdot\nabla f,\end{aligned}

que é uma identidade vetorial conhecida envolvendo uma função escalar
:math:`f` e outra função vetorial :math:`\vec{F}`. Fazendo
:math:`\vec{F}=\vec{P}` e :math:`f=1/|\vec{r}-\vec{r}^{\prime}|`,
podemos escrever:

.. math::

   \begin{aligned}
   \nabla^{\prime}\cdot\left[\dfrac{\vec{P}(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}\right]
   =
   \dfrac{\nabla^{\prime}\cdot\vec{P}(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}
   +
   \vec{P}(\vec{r}^{\prime})\cdot\nabla^{\prime}\left(\dfrac{1}{|\vec{r}-\vec{r}^{\prime}|}\right)\end{aligned}

o que nos leva à expressão,

.. math::

   \begin{aligned}
   \vec{P}(\vec{r}^{\prime})\cdot\nabla^{\prime}\left(\dfrac{1}{|\vec{r}-\vec{r}^{\prime}|}\right)
   =
   \nabla^{\prime}\cdot\left[\dfrac{\vec{P}(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}\right]
   -
   \dfrac{\nabla^{\prime}\cdot\vec{P}(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|},\end{aligned}

e substituindo este desenvolvimento na :eq:`potencialpolar`, podemos escrever:

.. math::

   \begin{aligned}
   \varphi(\vec{r})
   =
   \dfrac{1}{4\pi\varepsilon_{0}}
   \int_{V^{\prime}}
   \nabla^{\prime}\cdot\left[\dfrac{\vec{P}}{|\vec{r}-\vec{r}^{\prime}|}\right]~dV^{\prime}
   -
   \dfrac{1}{4\pi\varepsilon_{0}}
   \int_{V^{\prime}}
   \dfrac{\nabla^{\prime}\cdot\vec{P}(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}~dV^{\prime}\end{aligned}

e usando o teorema do divergente na primeira integral, segue que:

.. math::

   \begin{aligned}
   \varphi(\vec{r})
   =
   \dfrac{1}{4\pi\varepsilon_{0}}
   \oint_{S}
   \dfrac{\vec{P}(\vec{r}^{\prime})\cdot\hat{n}}{|\vec{r}-\vec{r}^{\prime}|}~dV^{\prime}
   +
   \dfrac{1}{4\pi\varepsilon_{0}}
   \int_{V^{\prime}}
   \dfrac{[-\nabla^{\prime}\cdot\vec{P}(\vec{r}^{\prime})]}{|\vec{r}-\vec{r}^{\prime}|}~dV^{\prime}.\end{aligned}

Aqui definimos as seguintes quantidades:

.. math::
   :label: rhoP
   
   \begin{aligned}
   \rho_{P}&=-\nabla\cdot\vec{P}\end{aligned},

.. math::
   :label: sigmaP

   \begin{aligned}
   \sigma_{P}&=\vec{P}\cdot\hat{n},\end{aligned}

que são as densidade de carga de polarização volumétrica e superficial.
Com isso, o potencial pode ser escrito na forma:

.. math::
   :label: potencialdiel

   \begin{aligned}
   \varphi(\vec{r})
   =
   \dfrac{1}{4\pi\varepsilon_{0}}
   \oint_{S}
   \dfrac{\sigma_{P}(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}~dV^{\prime}
   +
   \dfrac{1}{4\pi\varepsilon_{0}}
   \int_{V^{\prime}}
   \dfrac{\rho_{P}(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}~dV^{\prime}.\end{aligned}


A :eq:`potencialdiel` nos indica que o potencial
proveniente do material dielétrico é expresso em termos de uma
distribuição de cargas. Note que carga total de um dielétrico pode ser
calculada da seguinte forma:

.. math::

   \begin{aligned}
   Q_{P}=\int_{V}\rho_{P}(\vec{r})~dV
   +
   \oint_{S}\sigma_{P}(\vec{r})~da\end{aligned}

e substituindo-se as definições, temos ainda:

.. math::

   \begin{aligned}
   Q_{P}=-\int_{V}\nabla\cdot\vec{P}(\vec{r})~dV
   +
   \oint_{S}\vec{P}(\vec{r})\cdot\hat{n}~da\end{aligned}

e usando o teorema do divergente, podemos escrever a segunda integral na
forma:

.. math::

   \begin{aligned}
   Q_{P}=-\int_{V}\nabla\cdot\vec{P}(\vec{r})~dV
   +
   \int_{V}\nabla\cdot\vec{P}(\vec{r})~dV
   \qquad\therefore\qquad Q_{P}=0.\end{aligned}

Isto é esperado visto que não temos cargas livres no dielétrico.

Deslocamento Elétrico
---------------------

Vamos supor que temos um dielétrico infinito preenchendo todo o espaço.
Neste caso, não temos contribuição superficial e o potencial elétrico é
escrito de modo geral como:

.. math::
   :label: potencialdielgeral
   
   \begin{aligned}
   \varphi(\vec{r})
   =
   \dfrac{1}{4\pi\varepsilon_{0}}
   \int_{V^{\prime}}
   \dfrac{\rho(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}~dV^{\prime}
   +
   \dfrac{1}{4\pi\varepsilon_{0}}
   \int_{V^{\prime}}
   \dfrac{\rho^{*}(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}~dV^{\prime}.\end{aligned}

onde estamos considerando a possibilidade de haver cargas reais no meio
dielétrico, além da contribuição da polarização.

Note que podemos definir uma densidade de carga generalizada
:math:`\rho^{*}=\rho+\rho_{P}`, assim, a :eq:`potencialdielgeral` pode ser escrita na
forma:

.. math::
   :label: potencialdielgeral2
   
   \begin{aligned}
   \varphi(\vec{r})
   =
   \dfrac{1}{4\pi\varepsilon_{0}}
   \int_{V^{\prime}}
   \dfrac{\rho^{*}(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}~dV^{\prime}.\end{aligned}

A partir da :eq:`potencialdielgeral2`, podemos escrever a lei de Gauss na forma diferencial como foi feito no
caso do vácuo, neste caso porém, obtemos:

.. math::
   :label: gaussdiel
   
   \begin{aligned}
   \nabla\cdot\vec{E}
   =\dfrac{\rho^{*}}{\varepsilon_{0}}\end{aligned}

e substituindo a expressão para a densidade de carga :math:`\rho^{*}`,
segue que:

.. math::

   \begin{aligned}
   \nabla\cdot\vec{E}
   =\dfrac{\rho+\rho_{P}}{\varepsilon_{0}}
   =\dfrac{\rho-\nabla\cdot\vec{P}}{\varepsilon_{0}}\end{aligned}

o que pode ser escrito na forma:

.. math::

   \begin{aligned}
   \nabla\cdot(\varepsilon_{0}\vec{E}+\vec{P})=\rho\end{aligned}

e definindo :math:`\vec{D}`, o vetor deslocamento elétrico, como:

.. math::
   :label: deslocamento

   \begin{aligned}
   \vec{D}=\varepsilon_{0}\vec{E}+\vec{P}\end{aligned}

podemos escrever,

.. math::
   :label: leideGaussdiel

   \begin{aligned}
   \nabla\cdot\vec{D}=\rho.\end{aligned}

A :eq:`leideGaussdiel` é a lei de Gauss no caso
mais geral, envolvendo materiais com a presença de polarização. De fato,
é uma das equações de Maxwell como veremos no final do curso.

Convém notar que no vácuo, :math:`\vec{P}=0` e, assim, recuperamos a lei
de Gauss que havíamos derivado anteriormente. Neste caso, o deslocamento
elétrico está relacionado ao campo elétrico na forma:

.. math::

   \begin{aligned}
   \vec{D}=\varepsilon_{0}\vec{E}.\end{aligned}

De modo análogo, um meio dielétrico linear e isotrópico é caracterizado
por uma permissividade elétrica :math:`\varepsilon`, então podemos
escrever:

.. math::
   :label: permissividade

   \begin{aligned}
   \vec{D}=\varepsilon\vec{E},\end{aligned}

e substituindo na lei de Gauss, dada pela :eq:`leideGaussdiel`, obtemos:

.. math::

   \begin{aligned}
   \nabla\cdot(\varepsilon\vec{E})=\rho\end{aligned}

e, assim, a lei de Gauss pode ser escrita na forma:

.. math::
   :label: leideGaussdiel2
   
   \begin{aligned}
   \nabla\cdot\vec{E}=\dfrac{\rho}{\varepsilon}.\end{aligned}

Na forma integral, a :eq:`leideGaussdiel2` é dada por:

.. math::

   \begin{aligned}
   \oint_{S}\vec{E}\cdot\hat{n}~da=\dfrac{q}{\varepsilon},\end{aligned}

onde :math:`q` é carga livre dentro da superfície gaussiana. Vemos que
agora a informação a respeito da polarização foi incluída na chamada
*permissividade elétrica do meio*, :math:`\varepsilon`.

Alternativamente, a lei de Gauss na forma integral pode ser escrita na
forma:

.. math::

   \begin{aligned}
   \oint_{S}\vec{D}\cdot\hat{n}~da=q.\end{aligned}

Considerando o campo elétrico dado pela :eq:`leideGaussdiel2`, então podemos novamente
escrever :math:`\vec{E}=-\nabla\varphi`, o que nos permite escrever:

.. math::

   \begin{aligned}
   -\nabla\cdot\nabla\varphi=\dfrac{\rho}{\varepsilon}\end{aligned}

ou seja,

.. math::
   :label: poisson

   \begin{aligned}
   \nabla^{2}\varphi=-\dfrac{\rho}{\varepsilon},\end{aligned}

que é a equação de Poisson modificada para incluir a polarização do meio. Quando não há cargas livres no volume, a :eq:`poisson` torna-se a Equação de Laplace:

.. math::
   :label: laplace

   \begin{aligned}
   \nabla^{2}\varphi=0.\end{aligned}

A :eq:`laplace` nos mostra que todo o desenvolvimento que
foi feito para o vácuo e meios condutores se mantém na presença de
dielétricos. Isso ocorre porque estamos considerando um meio dielétrico
infinito, e tacitamente assumindo que o meio é linear e isotrópico, o
que nos permite trocar :math:`\varepsilon_{0}` por :math:`\varepsilon`.

Campo elétrico em um meio dielétrico
------------------------------------

Dadas as considerações que fizemos até o momento, vamos agora considerar
o cálculo do campo elétrico a partir do potencial elétrico. Para isso,
vamos retomar a equação para o potencial eletrostático considerando
agora a contribuição das cargas de polarização:

.. math::
   :label: potencialgeral

   \begin{aligned}
   \varphi(\vec{r})
   =
   \dfrac{1}{4\pi\varepsilon_{0}}
   \int_{V}\dfrac{\rho(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}~dV^{\prime}
   +
   \dfrac{1}{4\pi\varepsilon_{0}}
   \int_{V}\dfrac{\rho_{P}(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}~dV^{\prime}
   +
   \dfrac{1}{4\pi\varepsilon_{0}}
   \oint_{S}\dfrac{\sigma_{P}(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}~dV^{\prime}.\end{aligned}

O campo elétrico pode ser obtido por meio do gradiente
:math:`\vec{E}=-\nabla\varphi` e usando

.. math::

   \begin{aligned}
   \nabla\left[\dfrac{1}{|\vec{r}-\vec{r}^{\prime}|}\right]
   =-
   \dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}},\end{aligned}

podemos escrever o campo elétrico na forma geral:

A :eq:`campoeletrico` permite determinar o campo
devido à ambos: cargas reais e cargas de polarização lembrando que as
densidades de carga de polarização são dadas por:
:math:`\rho_{P}=-\nabla\cdot\vec{P}` e
:math:`\sigma_{P}=\vec{P}\cdot\hat{n}`.

Exemplos
--------

**1.** *Uma esfera dielétrica descarregada de raio* :math:`R` *e
permissividade* :math:`\varepsilon` *tem uma polarização uniforme,
orientada no sentido positivo do eixo-*:math:`\hat{z}`. *Calcule o campo
elétrico no interior da esfera, no eixo-*:math:`\hat{z}`.

.. centered:: Solução


Para calcular o campo elétrico é conveniente calcular primeiro o
potencial :math:`\varphi(\vec{r})` e depois o campo por meio da tomada
do gradiente :math:`\vec{E}=-\nabla\varphi`. A expressão geral para o
potencial elétrico em um meio dielétrico é dada pela :eq:`potencialgeral`. Para aplicá-la ao caso presente
, primeiro notamos que o fato de estar descarregada implica em
:math:`\rho(\vec{r})=0`. Precisamos também avaliar as densidades de
carga de polarização. Considerando que a polarização está dirigida ao
longo do eixo-:math:`\hat{z}`, podemos escrever:

.. math::

   \begin{aligned}
   \vec{P}=P\hat{\mathbf{k}}\end{aligned}

onde :math:`P` é uma constante.

Considerando a densidade volumétrica de carga de polarização, temos
que:

.. math::

   \begin{aligned}
   \rho_{P}&=-\nabla\cdot\vec{P}
   \\
   &=-\nabla\cdot(P\hat{\mathbf{k}})=0,\end{aligned}

e vemos que a contribuição volumétrica é zero.

Resta a contribuição da densidade superficial de carga de polarização,
assim, temos:

.. math::

   \begin{aligned}
   \sigma_{P}&=\vec{P}\cdot\hat{n}\\
   &=P\hat{\mathbf{k}}\cdot\hat{r}=P\cos\theta.\end{aligned}

A carga total de polarização deve ser nula. Podemos verificar isso
integrando a densidade :math:`\sigma_{P}` sobre toda a área da esfera:

.. math::

   \begin{aligned}
   Q_{S_{P}}&=\oint_{S}\vec{P}\cdot\hat{n}~da
   \\
   &=\int_{0}^{\pi}\int_{0}^{2\pi}
   P\cos\theta R^{2}\sin\theta d\theta d\phi
   =
   2\pi R^{2}P\int_{0}^{\pi}\sin\theta\cos\theta d\theta\end{aligned}

e fazendo :math:`u=\sin^{2}\theta`,
:math:`du=2\sin\theta\cos\theta~d\theta`, de modo que:

.. math::

   \begin{aligned}
    Q_{S_{P}}=P\pi R^{2}\left[\sin^{2}\theta\right]_{0}^{\pi}=0,\end{aligned}

como deveria.

Voltando ao problema, devemos substituir :math:`\sigma_{P}` na
integração, lembrando que:

.. math::

   \begin{aligned}
   \vec{r}&=z\hat{\mathbf{k}}
   \\
   \vec{r}^{\prime}&=R\sin\theta^{\prime}\cos\phi^{\prime}\hat{\mathbf{i}}+
   R\sin\theta^{\prime}\sin\phi^{\prime}\hat{\mathbf{j}}+
   R\cos\theta^{\prime}\hat{\mathbf{k}}\end{aligned}

o que nos permite escrever:

.. math::

   \begin{aligned}
   |\vec{r}-\vec{r}^{\prime}|
   &=
   [R^{2}\sin^{2}\theta^{\prime}\cos^{2}\phi^{\prime}
   +
   R^{2}\sin^{2}\theta^{\prime}\sin^{2}\phi^{\prime}
   +
   (z-R\cos\theta^{\prime})^{2}
   ]^{1/2}
   \\&=
   [R^{2}+z^{2}-2zR\cos\theta^{\prime}]^{1/2}.\end{aligned}

Notando que o elemento de área :math:`da^{\prime}
=
R^{2}\sin\theta^{\prime}d\theta^{\prime}d\phi^{\prime}`, segue que:

.. math::

   \begin{aligned}
   \varphi(\vec{r})&=\dfrac{1}{4\pi\varepsilon_{0}}
   \int_{0}^{\pi}\int_{0}^{2\pi}
   \dfrac{P\cos\theta^{\prime}R^{2}\sin\theta^{\prime}
   d\theta^{\prime}d\phi^{\prime}}{[R^{2}+z^{2}-2zR\cos\theta^{\prime}]^{1/2}}\end{aligned}

ou ainda,

.. math::

   \begin{aligned}
   \varphi(\vec{r})
   &=
   \dfrac{PR^{2}}{2\varepsilon_{0}}
   \int_{0}^{\pi}
   \dfrac{\cos\theta^{\prime}\sin\theta^{\prime}
   }{[R^{2}+z^{2}-2zR\cos\theta^{\prime}]^{1/2}}~d\theta^{\prime},\end{aligned}

e fazendo a substituição
:math:`u(\theta)=R^{2}-2zR\cos\theta^{\prime}+z^{2}`, podemos escrever a
integração acima na forma:

.. math::

   \begin{aligned}
   \varphi(\vec{r})
   &=
   \dfrac{PR^{2}}{2\varepsilon_{0}}
   \int_{u(\theta=0)}^{u(\theta=\pi)}
   \dfrac{\left(\dfrac{R^{2}+z^{2}-u}{2zR}\right)\dfrac{du}{2zR}}{u^{1/2}}
   \\&=
   \dfrac{P(R^{2}+z^{2})}{8z^{2}\varepsilon_{0}}
   \int_{u(\theta=0)}^{u(\theta=\pi)}
   u^{-1/2}~du
   -
   \dfrac{P}{8z^{2}\varepsilon_{0}}
   \int_{u(\theta=0)}^{u(\theta=\pi)}
   u^{1/2}~du\end{aligned}

onde :math:`u(\theta=0)=(R-z)^{2}` e :math:`u(\theta=\pi)=(R+z)^{2}`.

As integrais são triviais e podemos escrever diretamente:

.. math::

   \begin{aligned}
   \varphi(\vec{r})
   =
   \dfrac{P(R^{2}+z^{2})}{4z^{2}\varepsilon_{0}}
   [z+R-|R-z|]-\dfrac{P}{12z^{2}\varepsilon_{0}}[(R+z)^{3}-(R-z)^{3}]\end{aligned}

Temos que definir o sinal de :math:`|R-z|`. Como queremos o potencial no
interior da esfera, :math:`R>z` e assim, :math:`|R-z|=R-z`, logo, após
alguma álgebra temos:

.. math::
   :label: repostaex1

   \begin{aligned}
   \varphi(\vec{r})=\dfrac{Pz}{3\varepsilon_{0}}.\end{aligned}

O campo elétrico é determinado por meio do gradiente,

.. math::
   :label: repostaex2

   \begin{aligned}
   \vec{E}=-\dfrac{P}{3\varepsilon_{0}}\hat{\mathbf{k}}.\end{aligned}

**2.** *Calcule o deslocamento e o campo elétrico produzidos por uma
carga* :math:`Q` *imersa num meio dielétrico infinito de permissividade,*
:math:`\varepsilon`.

.. centered:: Solução


Temos, pela lei de Gauss que:

.. math::

   \begin{aligned}
   \oint_{S}\vec{E}\cdot\hat{n}~da=\dfrac{Q}{\varepsilon}\end{aligned}

e, escolhendo uma superfície gaussiana esférica com
:math:`\vec{n}=\vec{r}`, segue que:

.. math::

   \begin{aligned}
   \int_{S}E_{r}\hat{r}\cdot\hat{r}~da=\dfrac{Q}{\varepsilon}\qquad\therefore\qquad E_{r}=\dfrac{Q}{4\pi r^{2}},\end{aligned}

e portanto,

.. math::

   \begin{aligned}
   \vec{E}(\vec{r})=\dfrac{Q}{4\pi\varepsilon r^{2}}\hat{r}.\end{aligned}

Note que o campo elétrico produzido por uma carga no vácuo é dado por:

.. math::

   \begin{aligned}
   \vec{E}_{V}=\dfrac{Q}{4\pi\varepsilon_{0}r^{2}}\hat{r}\end{aligned}

onde :math:`V` denota vácuo.

A razão entre os módulos destas duas expressões é escrito como:

.. math::

   \begin{aligned}
   \dfrac{E}{E_{V}}=\dfrac{Q/4\pi\varepsilon r^{2}}{Q/4\pi\varepsilon_{0}r^{2}}=\dfrac{\varepsilon_{0}}{\varepsilon}
   \qquad\therefore\qquad
   E=\dfrac{E_{V}}{\varepsilon/\varepsilon_{0}}.\end{aligned}

Definimos a chamada constante dielétrica :math:`K` como:

.. math::

   \begin{aligned}
   K=\dfrac{\varepsilon}{\varepsilon_{0}}\end{aligned}

e, assim,

.. math::

   \begin{aligned}
   E=\dfrac{E_{V}}{K}.\end{aligned}

Como :math:`K\geq1`, então :math:`E\leq E_{V}`. Assim, o campo no vácuo
é sempre maior ou no mínimo igual ao campo elétrico no dielétrico. O
efeito do dielétrico é atenuar o campo por um fator :math:`K`.

Podemos entender o resultado por meio da ilustração da :numref:`cargadieletrico2` e vemos que o efeito da carga é induzir
momentos de dipolo em sua vizinhança como indicado. Estes momentos de
dipolo criam um campo elétrico com sentido contrário ao momento. Deste
modo, o campo medido na superfície gaussiana será:

.. math::

   \begin{aligned}
   \vec{E}=\vec{E}_{\text{carga}}+\vec{E}_{\text{induzido}}
   =
   E_{\text{carga}}\hat{r}-E_{\text{induzido}}\hat{r},\end{aligned}

e o campo medido tem seu módulo reduzido por um fator :math:`K`, devido
ao campo induzido devido aos dipolos.

.. _cargadieletrico2:

.. figure:: _images/dieletricocarga.png
      :scale: 51%
      :align: center

      Uma carga :math:`+Q` imersa em um material dielétrico.

Dando prosseguimento, vamos calcular o deslocamento elétrico, :math:`\vec{D}`:

.. math::

   \begin{aligned}
   \oint_{S}\vec{D}\cdot\hat{n}~da=Q\end{aligned}

o que nos leva a

.. math::

   \begin{aligned}
   \vec{D}=\dfrac{Q}{4\pi r^{2}}\hat{r}.\end{aligned}

Susceptibilidade Elétrica
~~~~~~~~~~~~~~~~~~~~~~~~~

Definimos duas quantidades que caracterizam o material dielétrico: a
permissividade do meio, :math:`\varepsilon`, e a constante dielétrica,
:math:`K=\varepsilon/\varepsilon_{0}`.

A permissividade do meio relaciona o deslocamento elétrico e o campo
elétrico e, muitas vezes, é conveniente estabelecer uma relação entre o
campo elétrico e a polarização diretamente. Assim, definimos a chamada
*susceptibilidade elétrica*, :math:`\chi`, na forma:

.. math::

   \begin{aligned}
   \label{susceptibilidade}
   \vec{P}=\chi\vec{E}.\end{aligned}

Considerando a expressão dada pela :eq:`deslocamento`, podemos escrever:

.. math::

   \begin{aligned}
   \vec{D}
   &=\varepsilon\vec{E}=\varepsilon_{0}\vec{E}+\vec{P}
   \\
   &=\varepsilon_{0}\vec{E}+\chi\vec{E}.\end{aligned}

Comparando ambos os membros da equação acima, podemos escrever:

.. math::

   \begin{aligned}
   \varepsilon=\varepsilon_{0}+\chi.\end{aligned}

Fisicamente, a susceptibilidade elétrica mede a facilidade ou
dificuldade que um meio dielétrico oferece à polarização, ou seja, a
indução de dipolos elétricos no meio.

Condições de contorno na interface entre dois meios dielétricos
---------------------------------------------------------------

Nesta seção, vamos considerar o que ocorre com o campo elétrico e com o
deslocamento elétrico na passagem de uma região para outra, ou seja, na
interface entre dois meios dielétricos com permissividades elétricas
diferentes.

A primeira das condições de contorno, é oriunda da lei de Gauss:

.. math::

   \begin{aligned}
   \oint_{S}\vec{D}\cdot\hat{n}~da=Q,\end{aligned}

desde que a validade desta equação independe da permissividade elétrica.

A segunda condição de contorno é oriunda do fato do campo elétrico ser
conservativo, i.e., :math:`\vec{E}=-\nabla\varphi` e, por isso,
:math:`\nabla\times\nabla\varphi=0` o que nos leva a:

.. math::

   \begin{aligned}
   \nabla\times\vec{E}=0,\end{aligned}

que vale também para qualquer meio.

A primeira condição de contorno é obtida aplicando a lei de Gauss à
superfície cilíndrica de comprimento :math:`L` e área :math:`dA`,
conforme ilustrado na :numref:`contorno01`. Note que esta
superfície atravessa a interface entre os dois meios dielétricos,
:math:`\varepsilon_{1}` e :math:`\varepsilon_{2}`.

.. _contorno01:

.. figure:: _images/contorno_01.png
      :scale: 51%
      :align: center

      Uma superfície gaussiana cilíndrica entre dois meios    dielétricos com permissividades :math:`\varepsilon_{1}` e :math:`\varepsilon_{2}`.


Considerando o cilindro ilustrado na :numref:`contorno01`, temos
três superfícies: :math:`A_{1}` e :math:`A_{2}` das bases do cilindro e
a área lateral :math:`A_{l}`. Segue então que:

.. math::

   \begin{aligned}
   \oint_{S}\vec{D}\cdot\hat{n}~da
   &=
   \int_{A_{1}}\vec{D}_{1}\cdot\hat{n}_{1}~da_{1}
   +
   \int_{A_{2}}\vec{D}_{2}\cdot\hat{n}_{2}~da_{2}
   +
   \int_{A_{l}}\vec{D}_{l}\cdot\hat{n}_{l}~da_{l}.\end{aligned}

Tomando o limite em que :math:`L\rightarrow0`, a área lateral vai a zero
e resta apenas as integrais sobre :math:`A_{1}` e :math:`A_{2}`. Neste
limite vemos da :numref:`contorno01` que: :math:`\hat{n}_{1}=-\hat{n}_{2}=\hat{n}`. Além disso, :math:`da_{1}=da_{2}=da`. Dado que o cilindro é muito pequeno podemos escrever:

.. math::

   \begin{aligned}
   \oint_{S}\vec{D}\cdot\hat{n}~da
   &=
   \vec{D}_{1}\cdot\hat{n}~da
   -
   \vec{D}_{2}\cdot\hat{n}~da
   =
   (\vec{D}_{1}-\vec{D}_{2})\cdot\hat{n}~da\end{aligned}

e a integral sobre a superfície fechada do cilindro deve ser igual à
carga encerrada por esta superfície gaussiana. Esta carga pode ser
escrita como:

.. math::

   \begin{aligned}
   Q=\sigma~da\end{aligned}

e assim, podemos escrever a igualdade:

.. math::
   :label: condicao1

   \begin{aligned}
    (\vec{D}_{1}-\vec{D}_{2})\cdot\hat{n}~da=\sigma~da,\end{aligned}

em que :math:`da` é a área da interface. Podemos então escrever:

No caso de não haver uma densidade de carga interface, a condição de
contorno dada pela :eq:`condicao1` se reduz à forma mais simples:

.. math::
   :label: condicao1mod

   \begin{aligned}
   \vec{D}_{1}\cdot\hat{n}-\vec{D}_{2}\cdot\hat{n}=0\end{aligned}

ou

onde usamos a notação :math:`D_{n}=\vec{D}\cdot\hat{n}`.

Vamos agora à segunda condição de contorno, que resulta da condição
:math:`\nabla\times\vec{E}=0`. Consideramos uma superfície retangular de
área :math:`S` como ilustrado na :numref:`contorno02` e sabemos que
integral sobre a superfície retangular de :math:`\nabla\times\vec{E}`
será igual a zero. Assim, escrevemos:


.. _contorno02:

.. figure:: _images/contorno_02.png
      :scale: 51%
      :align: center

      Uma superfície retangular entre dois meios dielétricos com permissividades :math:`\varepsilon_{1}` e :math:`\varepsilon_{2}` e delimitada pelo contorno :math:`ABCD`.


.. math::

   \begin{aligned}
   \int_{S}(\nabla\times\vec{E})\cdot\hat{n}~da=0\end{aligned}

e usando o teorema de Stokes, podemos escrever a equação acima na forma:

.. math::

   \begin{aligned}
   \oint_{C}\vec{E}\cdot d\vec{l}=0\end{aligned}

onde :math:`C` é o caminho :math:`ABCDA` que encerra a superfície
:math:`S`. Esta integral de linha pode ser escrita na forma:

.. math::

   \begin{aligned}
   \oint_{C}\vec{E}\cdot d\vec{l}
   =
   \int_{AB}\vec{E}_{1}\cdot d\vec{l}_{1}
   +
   \int_{BC}\vec{E}\cdot d\vec{l}_{3}
   +
   \int_{CD}\vec{E}_{2}\cdot d\vec{l}_{2}
   +
   \int_{DA}\vec{E}\cdot d\vec{l}_{4}\end{aligned}

e no limite :math:`L\rightarrow0`, as integrais em :math:`BC` e
:math:`DA` são nulas. Assim, segue que:

.. math::
   :label: intermed

   \begin{aligned}
   \oint_{C}\vec{E}\cdot d\vec{l}
   =
   \int_{AB}\vec{E}_{1}\cdot d\vec{l}_{1}
   +
   \int_{CD}\vec{E}_{2}\cdot d\vec{l}_{2}
   =
   0\end{aligned}

e fazendo os segmentos :math:`AB` e :math:`CD` tenderem a zero também,
então os integrandos não variam muito dentro de cada percurso,

.. math::

   \begin{aligned}
   \int_{AB}\vec{E}_{1}\cdot d\vec{l}_{1}\approx\vec{E}_{1}\cdot d\vec{l}_{1}
   \\
   \int_{CD}\vec{E}_{2}\cdot d\vec{l}_{2}\approx
   \vec{E}_{2}\cdot d\vec{l}_{2}\end{aligned}

notamos que :math:`d\vec{l}_{1}=-d\vec{l}_{2}=d\vec{l}`, assim, a :eq:`intermed` se torna:

.. math::
   :label: condicao2tan
   
   \begin{aligned}
   \vec{E}_{1}\cdot d\vec{l}
   -
   \vec{E}_{2}\cdot d\vec{l}=0\end{aligned}

ou ainda,

onde usamos a notação :math:`E_{t}=\vec{E}\cdot d\vec{l}`

Exemplo
-------

**1.** *Dois meios dielétricos com constantes* :math:`K_{1}` *e*
:math:`K_{2}` *estão separados por uma interface plana. Não há carga
externa sobre a interface. Encontre uma relação entre os ângulos*
:math:`\theta_{1}` *e* :math:`\theta_{2}`, *em que estes sejam os ângulos
formados por uma linha arbitrária de deslocamento com a normal à
interface:* :math:`\theta_{1}`, *no meio 1 e* :math:`\theta_{2}`, *no meio
2.*

.. centered:: Solução


As condições de contorno são dadas pelas :eq:`condicao1mod` e :eq:`condicao2tan`.
Assim, vamos, primeiramente, determinar o deslocamento elétrico e o
campo elétrico para os dois meios dielétricos. Sendo assim, podemos
escrever:

.. math::

   \begin{aligned}
   \vec{D}_{1}&=D_{1}\cos\theta_{1}\hat{\mathbf{i}}+D_{1}\sin\theta_{1}\hat{\mathbf{j}},
   \\
   \vec{D}_{2}&=D_{2}\cos\theta_{2}\hat{\mathbf{i}}+D_{2}\sin\theta_{2}\hat{\mathbf{j}}.\end{aligned}

Considerando as permissividades elétricas dos meios 1 e 2, podemos
relacionar o deslocamento :math:`\vec{D}` com o campo elétrico
:math:`\vec{E}`, assim:

.. math::

   \begin{aligned}
   \vec{D}_{1}=K_{1}\varepsilon_{0}\vec{E}_{1},
   \\
   \vec{D}_{2}=K_{2}\varepsilon_{0}\vec{E}_{2}.\end{aligned}

Considerando as expressões para :math:`\vec{D}_{1}` e
:math:`\vec{D}_{2}` acima determinadas, podemos escrever:

.. math::

   \begin{aligned}
   \vec{E}_{1}=\dfrac{D_{1}}{K_{1}\varepsilon_{0}}\cos\theta_{1}\hat{\mathbf{i}}
   +
   \dfrac{D_{1}}{K_{1}\varepsilon_{0}}\sin\theta_{1}\hat{\mathbf{j}},
   \\
   \vec{E}_{2}=\dfrac{D_{2}}{K_{2}\varepsilon_{0}}\cos\theta_{2}\hat{\mathbf{i}}+\dfrac{D_{2}}{K_{2}\varepsilon_{0}}\sin\theta_{2}\hat{\mathbf{j}}.\end{aligned}

Considerando a geometria estabelecida pela orientação do vetor
:math:`\vec{D}`, notamos que a interface está ao longo do
eixo-:math:`\hat{y}` e, por consequência, a normal está ao longo do
eixo-:math:`\hat{x}`. Sendo assim, as condições de contorno dadas pelas :eq:`condicao1mod` e
:eq:`condicao2tan`, nos permitem escrever:

.. math::
   :label: intermed:eq1
   
   \begin{aligned}
    D_{1}\cos\theta_{1}=D_{2}\cos\theta_{2},\end{aligned}

e também,

.. math::
   :label: intermed:eq2
   
   \begin{aligned}
   \dfrac{D_{1}}{K_{1}\varepsilon_{0}}\sin\theta_{1}
   =
   \dfrac{D_{2}}{K_{2}\varepsilon_{0}}\sin\theta_{2},\end{aligned}

e tomando a razão das :eq:`intermed:eq1` e a
:eq:`intermed:eq2`, segue que:

.. math::

   \begin{aligned}
   \dfrac{D_{1}}{K_{1}\varepsilon_{0}}\dfrac{\sin\theta_{1}}{D_{1}\cos\theta_{1}}
   =
   \dfrac{D_{2}}{K_{2}\varepsilon_{0}}\dfrac{\sin\theta_{2}}{D_{2}\cos\theta_{2}}\end{aligned}

de onde obtemos:

.. math::

   \begin{aligned}
   \dfrac{\tan\theta_{1}}{\tan\theta_{2}}=\dfrac{K_{1}}{K_{2}},\end{aligned}

que é a relação procurada.

Capacitância
------------

Vamos considerar um arranjo especial de dois planos condutores com
densidades de carga :math:`+\sigma` e :math:`-\sigma`, conforme
ilustrado na :numref:`capacitor` (a).

.. _capacitor:

.. figure:: _images/capacitor.png
      :scale: 51%
      :align: center

      Duas placas paralelas com densidades de carga  :math:`+\sigma` e :math:`-\sigma` formando um capacitor. (b) Visão frontal do arranjo da :numref:`capacitor`  (a) onde ser    observa as linhas do campo elétrico entre as placas.


Na :numref:`capacitor` (b), mostramos uma visão frontal das
placas, onde se pode observar as linhas do campo elétrico. Note que
existe uma deformação das linhas de campo nas bordas dos planos. Por
simplicidade, vamos assumir que a área :math:`A` das placas é muito
grande de modo que podemos desprezar estas distorções.

Este conjunto de duas placas é que chamamos de capacitor. Como será
discutido adiante, o capacitor permite o armazenamento de cargas. O
capacitor é um dos elementos básicos de um circuito e é sempre formado
por duas placas carregadas por cargas opostas, :math:`+Q` e :math:`-Q`.
A geometria das placas pode variar e os condutores podem ser diferentes.
Por simplicidade, vamos considerar a geometria ilustrada acima com duas
placas planas e paralelas.

O campo elétrico entre duas placas pode ser determinado pela soma dos
campos elétricos produzidos por um plano infinito. Na região entre as
placas este vale:

.. math::
   :label: cap:eq1
   
   \begin{aligned}
   \vec{E}=\dfrac{\sigma}{\varepsilon_{0}}\hat{n},\end{aligned}

onde :math:`\hat{n}` é a normal ao plano de densidade de carga
:math:`+\sigma`.

Também podemos calcular a diferença de potencial entre as placas:

.. math::

   \begin{aligned}
   \varphi_{+}-\varphi_{-}=-\int_{\vec{r}_{-}}^{\vec{r}^{+}}\vec{E}\cdot d\vec{r}=EL\end{aligned}

onde :math:`L` é a distância entre as placas.

Usando o campo elétrico dado pela :eq:`cap:eq1`, a
densidade de carga :math:`\sigma=Q/A`, podemos escrever a diferença de
potencial na forma:

.. math::
   :label: deltaphi:paralel
   
   \begin{aligned}
   \Delta\varphi=\dfrac{\sigma L}{\varepsilon_{0}}=\dfrac{QL}{\varepsilon_{0}A}\end{aligned}

onde :math:`A` é a área das placas.

Inspecionando as fórmulas gerais para o potencial, notamos que o
potencial é proporcional à carga que o produz. Deste modo, podemos
escrever, em geral:

.. math::

   \begin{aligned}
   \Delta\varphi\propto Q\end{aligned}

e a constante de proporcionalidade é a chamada capacitância :math:`C`,
assim,

.. math::
   :label: capacitancia
   
   \begin{aligned}
   Q=C\Delta\varphi.\end{aligned}

A capacitância depende apenas das propriedades do entre as placas
condutoras e da geometria das mesmas. Sendo assim, os capacitores são
classificados por sua capacitância e, quando submetidos a uma diferença
de potencial, armazenam uma carga :math:`Q` dada por:
:math:`Q=C\Delta\varphi`.

No sistema internacional de unidades, a capacitância é medidas em farads
(F) onde

.. math::

   \begin{aligned}
   1~\text{F}=\dfrac{1~\text{C}}{1~\text{V}}.\end{aligned}

No caso do capacitor formando pelas placas ilustradas na Fig.
`1.5 <#figcapacitores>`__\ (a), a capacitância pode ser determinada
diretamente por meio da substituição da 
:eq:`deltaphi:paralel` na :eq:`capacitancia`:

.. math::

   \begin{aligned}
   C=\varepsilon_{0}\dfrac{A}{L}.\end{aligned}

Desta expressão, vemos que é possível aumentar a quantidade de cargas
armazenadas diminuindo a distância :math:`L` entre as placas ou
aumentando a área :math:`A` das mesmas. Além disso, pode-se elevar o
valor da capacitância inserindo um material dielétrico de permissividade
:math:`\varepsilon=K\varepsilon_{0}`. Assim, denotando o novo valor da
capacitância por :math:`C_{\varepsilon}`, temos que:

.. math::

   \begin{aligned}
   C_{\varepsilon}&=K\varepsilon_{0}\dfrac{A}{L}\end{aligned}

e usando a expressão prévia para :math:`C`, vemos que:

.. math::

   \begin{aligned}
   C_{\varepsilon}&=KC,\end{aligned}

ou seja, a capacitância é multiplicada por um fator :math:`K`.


Exemplo
-------

**1.** *Considere duas esferas condutoras concêntricas de raios*
:math:`R_{a}` *e* :math:`R_{b}`, *com cargas* :math:`+Q` e :math:`-Q`,
*respectivamente. Calcule a capacitância deste capacitor esférico. O meio
entre as esferas tem permissividade,* :math:`\varepsilon`.

.. _capesferico:

.. figure:: _images/capesferico.png
      :scale: 51%
      :align: center

      Duas cascas esféricas carregadas com cargas :math:`+Q` e :math:`-Q` formando um capacitor. (b) Visão em corte das cascas mostrando as linhas de campo elétrico.

.. centered:: Solução


Precisamos do campo elétrico entre as esferas para calcular a diferença
de potencial, :math:`\Delta\varphi`. Para isso, podemos aplicar a lei de
Gauss à uma superfície gaussiana esférica de raio :math:`r`, tal que
:math:`R_{a}\leq r\leq R_{b}`. Neste caso podemos escrever:

.. math::

   \begin{aligned}
   \oint_{S}\vec{E}\cdot\hat{n}~da=\dfrac{Q}{\varepsilon}\end{aligned}

e considerando a simetria do problema, vemos que o campo elétrico
apresenta simetria radial, e depende apenas do módulo da distância,
:math:`r`. Com isso, escrevemos:

.. math::

   \begin{aligned}
   \oint_{S}E_{r}\hat{r}\cdot\hat{r}~da=\dfrac{Q}{\varepsilon}\end{aligned}

o que resulta em:

.. math::

   \begin{aligned}
   \vec{E}=\dfrac{Q}{4\pi\varepsilon r^{2}}\hat{r}.\end{aligned}

A diferença de potencial entre as esferas pode ser então determinada
pela integração do campo elétrico na região entre as esferas:

.. math::

   \begin{aligned}
   \varphi(R_{b})-\varphi(R_{a})
   &=
   -\int_{R_{a}}^{R_{b}}
   \dfrac{Q}{4\pi\varepsilon r^{2}}\hat{r}\cdot(dr\hat{r}
   +
   d\theta\hat{\theta}+r\sin\theta\hat{\phi})
   =
   -\dfrac{Q}{4\pi\varepsilon}\int_{R_{a}}^{R_{b}}\dfrac{dr}{r^{2}}
   \\&=
   \dfrac{Q}{4\pi\varepsilon}\left[\dfrac{1}{R_{b}}-\dfrac{1}{R_{a}}\right].\end{aligned}

Considerando que :math:`\varphi(R_{a})>\varphi(R_{b})`, então podemos
escrever, após simplificações:

.. math::

   \begin{aligned}
   C=4\pi\varepsilon\dfrac{R_{b}R_{a}}{R_{b}-R_{a}}.\end{aligned}

Um resultado clássico é a capacitância de uma esfera (assumindo que a
casca esférica externa está no infinito),

.. math::

   \begin{aligned}
   C=4\pi\varepsilon R_{a}\end{aligned}

onde tomamos o limite :math:`R_{b}\rightarrow\infty`.

Energia Eletrostática
=====================

A energia eletrostática de um grupo de cargas é a energia potencial do
sistema medida em relação ao estado em que elas estão infinitamente
separadas umas das outras. Esta energia pode ser obtida facilmente por
meio do trabalho realizado para reunir as cargas, trazendo-as uma de
cada vez. A primeira carga :math:`q_{1}`, pode ser colocada em sua
posição sem nenhum trabalho, assim, :math:`\mathcal{W}_{1}=0`; para
colocar a segunda carga, :math:`q_{2}`, um trabalho externo
:math:`\mathcal{W}_{2}` é necessário e dado por:

.. math::

   \begin{aligned}
   \mathcal{W}_{2}=q_{2}\varphi_{21}\end{aligned}

onde :math:`\varphi_{21}` é o potencial na posição :math:`\vec{r}_{2}`
da carga :math:`q_{2}`, produzido pela carga :math:`q_{1}`, localizada
na posição :math:`\vec{r}_{1}`.

Para trazer uma terceira carga, precisamos realizar um trabalho externo
:math:`\mathcal{W}_{3}`, dado por:

.. math::

   \begin{aligned}
   \mathcal{W}_{3}=q_{3}\varphi_{32}+q_{3}\varphi_{31}\end{aligned}

onde :math:`\varphi_{32}` é o potencial produzido pela carga
:math:`q_{2}` na posição da carga :math:`q_{3}` e :math:`\varphi_{31}` é
o potencial gerado por :math:`q_{1}` na posição da carga :math:`q_{3}`.

Para a *m*-ésima carga, :math:`q_{m}`, o trabalho realizado para
trazê-la do infinito até às proximidades das :math:`m-1` cargas é dado
por:

.. math::

   \begin{aligned}
   \mathcal{W}_{m}=\sum_{j=1}^{m-1}q_{m}\varphi_{mj}\end{aligned}

A energia potencial é igual ao trabalho externo total realizado para
reunir as :math:`N` cargas, assim,

.. math::

   \begin{aligned}
   \mathcal{U}=\sum_{m=1}^{N}\mathcal{W}_{m}\end{aligned}

e substituindo :math:`\mathcal{W}_{m}` da expressão prévia, segue que:

.. math::

   \begin{aligned}
   \mathcal{U}=\sum_{m=1}^{N}\sum_{j=1}^{m-1}q_{m}\varphi_{mj}.\end{aligned}

A expressão acima é complicada pelo fato da segunda soma depender do
índice da primeira soma. Podemos simplificar esta expressão percebendo
que a segunda soma pode ser reescrita na forma:

.. math::

   \begin{aligned}
   \sum_{j=1}^{m-1}q_{m}\varphi_{mj}
   =
   \dfrac{1}{2}\sum_{j=1}^{N}q_{m}\varphi_{mj}\end{aligned}

o que nos permite escrever:

.. math::

   \begin{aligned}
   \mathcal{U}=\dfrac{1}{2}\sum_{m=1}^{N}\sum_{j=1}^{N}q_{m}\varphi_{mj}.\end{aligned}

e definindo o potencial total na posição da carga :math:`q_{m}` pela
soma:

.. math::

   \begin{aligned}
   \varphi_{m}=\sum_{j=1}^{N}\varphi_{mj},\end{aligned}

a energia potencial pode então ser escrita na forma:

.. math::
   :label: energia:potencial
   
   \begin{aligned}
   \mathcal{U}=\dfrac{1}{2}\sum_{m=1}^{N}q_{m}\varphi_{m}.\end{aligned}

A :eq:`energia:potencial` tem validade geral e
no caso de cargas reunidas em um meio dielétrico devemos trocar
:math:`\varepsilon_{0}` por :math:`\varepsilon` no cálculo do potencial
:math:`\varphi_{m}`.

No caso de uma distribuição contínua de cargas, a :eq:`energia:potencial` pode ser escrita na forma
de uma integração:

.. math::
   :label: energia:potencialcont1
   
   \begin{aligned}
   \mathcal{U}=\dfrac{1}{2}\int_{V}\rho(\vec{r})\varphi(\vec{r})~dV,\end{aligned}

onde o potencial é gerado pela própria densidade de cargas,
:math:`\rho`. Além disso, o volume deve conter toda a densidade,
:math:`\rho`.

Forma alternativa de cálculo da energia potencial
-------------------------------------------------

No caso de meios dielétricos, a densidade de carga pode ser relacionada
com o deslocamento elétrico por meio da lei de Gauss:

.. math::

   \begin{aligned}
   \nabla\cdot\vec{D}=\rho,\end{aligned}

assim a :eq:`energia:potencialcont` pode ser escrita na forma:

.. math::
   :label: energia:potencialcont
   
   \begin{aligned}
   \mathcal{U}=\dfrac{1}{2}\int_{V}
   \varphi(\vec{r})\nabla\cdot\vec{D}~dV,\end{aligned}

e podemos reescrever a equação lembrando que
:math:`\varphi\nabla\cdot\vec{D}=\nabla\cdot(\varphi\vec{D})-\nabla\varphi\cdot\vec{D}`,
assim,

.. math::

   \begin{aligned}
   \mathcal{U}=
   \dfrac{1}{2}\int_{V}\nabla\cdot(\varphi\vec{D})~dV
   -
   \dfrac{1}{2}\int_{V}\nabla\varphi\cdot\vec{D}~dV.\end{aligned}

Como :math:`\vec{E}=-\nabla\varphi`, então a segunda integração pode ser
escrita em termos do campo elétrico:

.. math::

   \begin{aligned}
   \mathcal{U}=
   \dfrac{1}{2}\oint_{S}\varphi\vec{D}\cdot\hat{n}~da
   +
   \dfrac{1}{2}\int_{V}\vec{E}\cdot\vec{D}~dV,\end{aligned}

onde também passamos a primeira integral de volume para uma integração
sobre uma superfície fechada usando o teorema do divergente. Vamos
considerar o limite :math:`V\rightarrow\infty`, neste caso, a integral
de superfície é zero visto que :math:`\varphi\propto r^{-1}`, no caso de
uma carga livre, e :math:`D\propto r^{-2}`, assim
:math:`\varphi D\propto r^{-3}`. Distribuições de carga mais complexas
com dipolos, quadrupolos, etc., apresentam dependência com potências
mais elevadas. O elemento de superfície varia com
:math:`da\propto r^{2}`, assim o integrando global, ou seja,
:math:`\varphi\vec{D}\cdot\hat{n}~da` varia com :math:`r^{-1}` e tende a
zero no limite de :math:`V\rightarrow\infty`. Assim, podemos escrever:

.. math::
   :label: energiaoutromet

   \begin{aligned}
   \mathcal{U}=
   \dfrac{1}{2}\int_{V}\vec{E}\cdot\vec{D}~dV.\end{aligned}

Exemplo
-------

**1**. (Reitz, Milford & Christy, cap. 6, Prob. 6-4, pg. 134.) *Dada uma
distribuição de carga esférica, de raio :math:`R`, com densidade de
carga uniforme* :math:`\rho_{0}`, *determine a auto-energia da
distribuição de duas maneiras: (a) por integração direta do potencial
(*:eq:`energia:potencialcont` *)* *e (b) por integração sobre o campo(* :eq:`energiaoutromet` *)*.

.. centered:: Solução


| (a)
| A energia da distribuição é dada pela :eq:`energia:potencialcont`, cuja forma é
  dada por:

  .. math::

     \begin{aligned}
     \mathcal{U}=\dfrac{1}{2}\int_{V}\rho(\vec{r})\varphi(\vec{r})~dV,\end{aligned}

  e considerando que a densidade é uniforme, então podemos escrever:

  .. math::
     :label: intermed:potex1
   
     \begin{aligned}
     \mathcal{U}=\dfrac{\rho_{0}}{2}\int_{V}\varphi(\vec{r})~dV.\end{aligned}

Para continuar, precisamos da expressão do potencial elétrico para esta
distribuição. Vamos determiná-lo por integração direta sobre o volume da
esfera:

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\dfrac{1}{4\pi\varepsilon_{0}}\int_{V}\dfrac{\rho(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}~dV^{\prime}.\end{aligned}

Vamos aproveitar a simetria esférica da distribuição e calcular o
potencial em um ponto ao longo do eixo-:math:`\hat{z}`, assim,
:math:`\vec{r}=r\hat{\mathbf{k}}`. O vetor
:math:`\vec{r}^{\prime}=r^{\prime}\sin\theta^{\prime}\cos\phi^{\prime}\hat{\mathbf{i}}+r^{\prime}\sin\theta^{\prime}\sin\phi^{\prime}\hat{\mathbf{j}}+r^{\prime}
\cos\theta^{\prime}\hat{\mathbf{k}}`. Com isso, o denominador que
aparece na integral do potencial pode ser escrito explicitamente na
forma:

.. math::

   \begin{aligned}
   |\vec{r}-\vec{r}^{\prime}|
   =
   [r^{\prime 2}\sin^{2}\theta^{\prime}\cos^{2}\phi^{\prime}
   +
   r^{\prime 2}\sin^{2}\theta^{\prime}\sin^{2}\phi^{\prime}
   +
   (r-r^{\prime}\cos\theta^{\prime})^{2}
   ]^{1/2}\end{aligned}

e simplificando os termos acima, podemos escrever:

.. math::

   \begin{aligned}
   |\vec{r}-\vec{r}^{\prime}|
   =
   [r^{\prime 2}-2rr^{\prime}\cos\theta^{\prime}+r^{2}]^{1/2}.\end{aligned}

Usando a expressão no denominador e o elemento de volume em coordenadas
esféricas, segue que:

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\dfrac{\rho_{0}}{4\pi\varepsilon_{0}}\int_{0}^{R}\int_{0}^{\pi}\int_{0}^{2\pi}\dfrac{r^{\prime 
   2}dr^{\prime}\sin\theta^{\prime} d\theta^{\prime}d\phi^{\prime}}{[r^{\prime 2}-2rr^{\prime}\cos\theta^{\prime}+r^{2}]^{1/2}}.\end{aligned}

A integral em :math:`\phi^{\prime}` é direta e igual a :math:`2\pi`.
Podemos então reescrever a integral acima na forma:

.. math::
   :label: intermed:potex2
   
   \begin{aligned}
   \varphi(\vec{r})=\dfrac{\rho_{0}}{2\varepsilon_{0}}\int_{0}^{R}I(r^{\prime})r^{\prime 2}~dr^{\prime}\end{aligned}

onde,

.. math::

   \begin{aligned}
   I(r^{\prime})=\int_{0}^{\pi}\dfrac{\sin\theta^{\prime} }{[r^{\prime 2}-2rr^{\prime}\cos\theta^{\prime}+r^{2}]^{1/2}}~d\theta^{\prime}.\end{aligned}

A integral :math:`I(r^{\prime})` pode ser determinada fazendo a seguinte
substituição de variável: :math:`u(\theta)=r^{\prime 
2}-2rr^{\prime}\cos\theta^{\prime}+r^{2}`, tal que
:math:`du=2rr^{\prime}\sin\theta^{\prime}~d\theta^{\prime}` e
:math:`u(\pi)=(r^{\prime}+r)^{2}` e :math:`u(0)=(r^{\prime}-r)^{2}`.
Assim, podemos escrever:

.. math::

   \begin{aligned}
   I(r^{\prime})=\dfrac{1}{2rr^{\prime}}\int_{u(0)}^{u(\pi)}\dfrac{du}{u^{1/2}},\end{aligned}

e efetuando a integração, obtemos:

.. math::

   \begin{aligned}
   I(r^{\prime})&=\dfrac{1}{rr^{\prime}}[\sqrt{(r^{\prime}+r)^{2}}-\sqrt{(r^{\prime}-r)^{2}}]
   \\
   &=\dfrac{1}{rr^{\prime}}[r^{\prime}+r-|r^{\prime}-r|].\end{aligned}

Substituindo :math:`I(r^{\prime})` na :eq:`intermed:potex2`, segue que:

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\dfrac{\rho_{0}}{2\varepsilon_{0}r}\int_{0}^{R}[r^{\prime}+r-|r^{\prime}-r|]r^{\prime}~dr^{\prime}\end{aligned}

ou ainda,

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\dfrac{\rho_{0}}{2\varepsilon_{0}r}\int_{0}^{R}[r^{\prime2}+rr^{\prime}-r^{\prime}|r^{\prime}-r|]~dr^{\prime}\end{aligned}

e efetuando as duas primeiras integrais, temos ainda:

.. math::

   \begin{aligned}
   \varphi(\vec{r})&=\dfrac{\rho_{0}}{2\varepsilon_{0}r}
   \left[\dfrac{R^{3}}{3}+r\dfrac{R^{2}}{2}-\int_{0}^{R}r^{\prime}|r^{\prime}-r|~dr^{
   \prime}\right]
   \\&=
   \dfrac{\rho_{0}}{2\varepsilon_{0}r}
   \left[\dfrac{R^{3}}{3}+r\dfrac{R^{2}}{2}-\int_{0}^{r}r^{\prime}|r^{\prime}-r|~dr^{
   \prime}-\int_{r}^{R}r^{\prime}|r^{\prime}-r|~dr^{
   \prime}\right],\end{aligned}

e notando que :math:`|r^{\prime}-r|=-(r^{\prime}-r)` para
:math:`0\leq r^{\prime}\leq r` e :math:`|r^{\prime}-r|=(r^{\prime}-r)`
para :math:`r\leq r^{\prime}\leq 
R`, então as integrações acima podem ser escritas na forma:

.. math::

   \begin{aligned}
   \varphi(\vec{r})
   =
   \dfrac{\rho_{0}}{2\varepsilon_{0}r}
   \left[\dfrac{R^{3}}{3}+r\dfrac{R^{2}}{2}+\int_{0}^{r}r^{\prime}(r^{\prime}-r)~dr^{
   \prime}-\int_{r}^{R}r^{\prime}(r^{\prime}-r)~dr^{
   \prime}\right],\end{aligned}

e efetuando as integrações, obtemos:

.. math::

   \begin{aligned}
   \varphi(\vec{r})
   =
   \dfrac{\rho_{0}}{2\varepsilon_{0}r}
   \left[\dfrac{R^{3}}{3}+r\dfrac{R^{2}}{2}+\dfrac{r^{3}}{3}-\dfrac{r^{3}}{2}
   -\dfrac{R^{3}}{3}+r\dfrac{R^{2}}{2}+\dfrac{r^{3}}{3}-\dfrac{r^{3}}{2}\right],\end{aligned}

e juntando os termos semelhantes, temos ainda:

.. math::

   \begin{aligned}
   \varphi(\vec{r})
   =
   \dfrac{\rho_{0}}{2\varepsilon_{0}r}
   \left[rR^{2}-\dfrac{r^{3}}{3}\right],\end{aligned}

ou ainda,

.. math::
   :label: varphi:intermed

   \begin{aligned}
   \varphi(\vec{r})
   =
   \dfrac{\rho_{0}}{2\varepsilon_{0}}
   \left[R^{2}-\dfrac{r^{2}}{3}\right].\end{aligned}

Substituindo :eq:`varphi:intermed` em :eq:`intermed:potex1`, segue que:

.. math::

   \begin{aligned}
   \mathcal{U}=\dfrac{\rho_{0}}{2}\dfrac{\rho_{0}}{2\varepsilon_{0}}\int_{V}
   \left[R^{2}-\dfrac{r^{2}}{3}\right]~dV
   =
   \dfrac{\rho^{2}_{0}}{4\varepsilon_{0}}4\pi\int_{0}^{R}\left[R^{2}-\dfrac{r^{2}}{3}\right]r^{2}dr\end{aligned}

e efetuando as duas integrações, segue que:

.. math::

   \begin{aligned}
   \mathcal{U}
   =
   \dfrac{\rho^{2}_{0}}{4\varepsilon_{0}}4\pi\left[\dfrac{R^{5}}{3}-\dfrac{1}{3}\dfrac{R^{5}}{5}\right]
   =
   \dfrac{4\pi\rho^{2}_{0}R^{5}}{12\varepsilon_{0}}\left[1-\dfrac{1}{5}\right]
   =
   \dfrac{4\pi\rho^{2}_{0}R^{5}}{12\varepsilon_{0}}\dfrac{4}{5}\end{aligned}

e, finalmente,

.. math::

   \begin{aligned}
   \mathcal{U}=\dfrac{4\pi\rho^{2}_{0}R^{5}}{15\varepsilon_{0}}.\end{aligned}

| (b)
| Considerando a integração sobre o campo, devemos integrar sobre todo o
  espaço, assim, escrevemos a :eq:`energiaoutromet` para uma parte interna e
  outra parte externa, assim:

  .. math::

     \begin{aligned}
     \mathcal{U}=\dfrac{1}{2}\int_{V}\vec{D}\cdot\vec{E}~dV\end{aligned}

  ou,

  .. math::

     \begin{aligned}
     \mathcal{U}=\dfrac{1}{2}\int_{V}\vec{D}_{\text{ext}}\cdot\vec{E}_{\text{ext}}~dV
     +
     \dfrac{1}{2}\int_{V}\vec{D}_{\text{int}}\cdot\vec{E}_{\text{int}}~dV\end{aligned}

  e considerando que o deslocamento elétrico está relacionado ao campo
  elétrico pela equação: :math:`\vec{D}=\varepsilon_{0}\vec{E}`, assim,
  segue que:

  .. math::
     :label: intermed:energia
   
     \begin{aligned}
     \mathcal{U}=\dfrac{\varepsilon_{0}}{2}\int_{V}E^{2}_{\text{ext}}~dV
     +
     \dfrac{\varepsilon_{0}}{2}\int_{V}E^{2}_{\text{int}}~dV.\end{aligned}

O campo elétrico interno e externo são dados pelas seguintes expressões
obtidas por meio da lei de Gauss:

.. math::

   \begin{aligned}
   E_{\text{int}}=\dfrac{\rho_{0}r}{3\varepsilon_{0}},\qquad r<R
   \\
   E_{\text{ext}}=\dfrac{\rho_{0}R^{3}}{3\varepsilon_{0}r^{2}},\qquad r>R.\end{aligned}

Substituindo-se as expressões na :eq:`intermed:energia`, segue que:

.. math::

   \begin{aligned}
   \mathcal{U}=\dfrac{\varepsilon_{0}}{2}\dfrac{\rho^{2}_{0}}{9\varepsilon^{2}_{0}}\left[\int_{V}r^{2}~dV
   +
   R^{6}\int_{V}\dfrac{1}{r^{4}}~dV\right].\end{aligned}

e considerando que os integrandos são funções de :math:`r`, então
podemos escrever:

.. math::

   \begin{aligned}
   \mathcal{U}=\dfrac{4\pi\varepsilon_{0}}{2}\dfrac{\rho^{2}_{0}}{9\varepsilon^{2}_{0}}\left[\int_{0}^{R}r^{2}r^{2}~dr
   +
   R^{6}\int_{R}^{\infty}\dfrac{1}{r^{4}}r^{2}dr\right]
   =
   \dfrac{4\pi\varepsilon_{0}}{2}\dfrac{\rho^{2}_{0}}{9\varepsilon^{2}_{0}}
   \left[\dfrac{R^{5}}{5}
   +
   R^{6}\dfrac{1}{R}\right],\end{aligned}

o que nos permite escrever,

.. math::

   \begin{aligned}
   \mathcal{U}=
   \dfrac{4\pi\varepsilon_{0}R^{5}}{2}\dfrac{\rho^{2}_{0}}{9\varepsilon^{2}_{0}}
   \dfrac{6}{5}\end{aligned}

ou ainda,

.. math::

   \begin{aligned}
   \mathcal{U}=
   \dfrac{4\pi\rho^{2}_{0}R^{5}}{15\varepsilon_{0}}.\end{aligned}

Note que obtivemos o mesmo resultado usando ambos os métodos, como era esperado.
