Eletrostática
=============

Neste capítulo damos o início ao estudo da teoria eletromagnética, para
isso, consideramos primeiramente o conceito de carga elétrica. Quando
esfregamos um bastão de borracha em um pedaço de lã, percebemos que
estes materiais ficam carregados. Neste processo, ocorre uma
transferência de cargas elétricas de um material para o outro através do
atrito dos materiais. Estas cargas estão associadas à partículas
elementares, especificamente a elétrons. É importante notar que esta
transferência de elétrons não implica na criação ou destruição de
cargas, ou seja, antes e após o processo de transferência a soma das
cargas permanece a mesma. Com efeito, parte dos elétrons da lã são
transferidas para o bastão de borracha deixando este negativamente
carregado (com excesso de elétrons) e a lã positivamente carregada (com
falta de elétrons). 

Existem dois tipos de carga elétrica, que
denominamos positiva e negativa. A carga em si é uma propriedade
intrínseca da matéria e está associada à partículas elementares. A
matéria é constituída por prótons, elétrons e nêutrons. Os prótons são
positivamente carregados e os elétrons possuem carga negativa. Os
nêutrons, por outro lado, não apresentam carga. Assim, desde que estas
partículas são os constituintes dos átomos, a matéria é dotada de uma
grande quantidade de cargas positiva e negativa. A despeito deste fato,
não observamos efeitos de tais cargas desde que a quantidade de cargas
positiva e negativa é aproximadamente igual e assim, nenhum efeito
macroscópico é observado.

Para fixar as nomenclaturas a serem usadas ao longo do curso, quando nos
referimos à carga elétrica do ponto de vista macroscópico, estamos nos
referindo à carga líquida, i.e, à quantidade de carga elétrica
descompensada ou excesso de carga elétrica. Este excesso pode ser tanto
de elétrons (carga negativa) ou de prótons (carga negativa).

Lei de Coulomb
--------------

As experiências com cargas elétricas no final do século 18 podem ser
resumidas em três afirmativas:

(a) Existem duas e somente duas espécies de cargas elétricas, hoje
    denominadas positiva e negativa;

(b) Duas cargas puntuais exercem, entre si, forças que atuam ao longo da
    linha que as une e que são inversamente proporcionais ao quadrado da
    distância entre elas;

(c) Estas forças são também proporcionais ao produto das cargas sendo
    repulsivas para cargas de mesmo sinal e atrativas para cargas de
    sinais opostos.

As afirmações acima são hoje conhecidas como **Lei de Coulomb** em
homenagem à C. A. Coulomb um dos pioneiros no estudo da Eletricidade.
Seguindo a notação vetorial que desenvolvemos no capítulo anterior,
podemos expressar a lei de Coulomb da seguinte maneira:

.. math::
   :label: coulomb

   \begin{aligned}
   \vec{F}_{1}=C\dfrac{q_{1}q_{2}}{r^{2}_{12}}\dfrac{\vec{r}_{12}}{r_{12}},\end{aligned}

com

.. math::

   \begin{aligned}
   \vec{r}_{12}=\vec{r}_{1}-\vec{r}_{2}.\end{aligned}

Na :eq:`coulomb`, :math:`\vec{F}_{1}` é a força sobre a
carga :math:`q_{1}` exercida pela carga :math:`q_{2}`. As cargas
:math:`q_{1}` e :math:`q_{2}` estão localizadas no espaço nas posições
indicadas pelos vetores :math:`\vec{r}_{1}` e :math:`\vec{r}_{2}` cuja
diferença nos fornece a distância entre as cargas. Com efeito,
:math:`\vec{r}_{12}` é o vetor que vai de :math:`q_{2}` a :math:`q_{1}`
cujo módulo é denotado por :math:`r_{12}`. Note que
:math:`\vec{r}_{12}/r_{12}=\hat{r}_{12}` é um vetor de módulo unitário
cujo objetivo é apenas denotar a direção e sentido da força que atua
sobre a carga. Vemos então que a força atua em uma direção definida por
uma linha que conecta as duas cargas, em conformidade com a segunda
afirmação decorrente do experimento.

Para encontrar a força sobre a partícula 2 devido à partícula 1, basta
apenas permutar os índices 1 e 2. É importante entender todos os
elementos da :eq:`coulomb` pois a notação será largamente
utilizada futuramente. Além disso, é importante notar que a :eq:`coulomb` é aplicada a cargas puntuais, o que é aplicável
em sistemas macroscópicos tão logo a distribuição de cargas tenha
dimensão muito menor comparada com a escala de comprimento do sistema. A
constante de proporcionalidade :math:`C` depende do sistema de unidades
usado, no caso presente, vamos utilizar o sistema internacional de
unidades onde a constante é dada por

.. math::
   :label: constante
   
   \begin{aligned}
   C=\dfrac{1}{4\pi\epsilon_{0}}, \qquad\epsilon_{0}=8,854\times10^{-12}~\text{C}^{2}/\text{N.m}^{2}\end{aligned}

onde :math:`\epsilon_{0}` é a chamada permissividade elétrica do vácuo.

A :eq:`coulomb` pode ser generalizada para :math:`N`
partículas, onde por exemplo a força sobre a :math:`i`-ésima carga
devido às demais é dada por:

.. math::
   :label: coulomb2

   \begin{aligned}
   \vec{F}_{i}=q_{i}\sum_{j\neq i}^{N}\dfrac{q_{j}}{4\pi\epsilon_{0}}\dfrac{\vec{r}_{ij}}{r^{3}_{ij}},\end{aligned}

onde :math:`\vec{r}_{ij}=\vec{r}_{i}-\vec{r}_{j}`. Este é simplesmente o
princípio de superposição das forças que nos diz que a força total sobre
uma determinada partícula é a soma vetorial de todas as forças que atuam
sobre ela.

Até aqui, fizemos apenas uma extensão da fórmula de Coulomb para
:math:`N` cargas puntuais mas ainda não dissemos nada a respeito do
valor desta carga. A carga elementar, medida em Coulombs no sistema
internacional de unidades, vale :math:`1,602\times10^{-19}` C. Este é
um valor extremamente pequeno de modo que macroscópicamente não iremos
perceber a granularidade da carga. Mesmo uma quantidade
macroscopicamente pequena de carga contém ainda muitos elétrons de modo
que é possível considerar a extensão da :eq:`coulomb2`
para uma distribuição contínua de cargas, em analogia ao que usualmente
fazemos quando passamos a discutir a dinâmica de corpos rígidos em
Mecânica. Assim, considerando esta situação em que estamos longe do
limite atômico, podemos definir as densidades de carga, conforme segue:

.. math::

   \begin{aligned}
   \label{rho}
   \rho&=\lim_{\Delta V\rightarrow0}\dfrac{\Delta q}{\Delta V}, \qquad(\text{densidade volumétrica de carga}),
   \\
   \sigma&=\lim_{\Delta S\rightarrow0}\dfrac{\Delta q}{\Delta S}, \qquad(\text{densidade superficial de carga})\\
      \lambda&=\lim_{\Delta l\rightarrow0}\dfrac{\Delta q}{\Delta l}, \qquad(\text{densidade linear de carga}).\end{aligned}

Notamos que as densidades de carga são referentes à carga líquida ou
excesso de carga. Agora considere que temos uma carga :math:`q` na
presença de uma distribuição volumétrica :math:`\rho`,  outra densidade
superficial de carga caracterizada por :math:`\sigma` e ainda uma densidade linear de carga :math:`\lambda`. Neste caso a força que
estas distribuições exercem sobre uma carga :math:`q`, localizada na
posição :math:`\vec{r}`, é dada por:

.. math::

   \begin{multline}
   \label{forca:distribuicao}
   \vec{F}_{q}(\vec{r})=\dfrac{q}{4\pi\epsilon_{0}}\int_{V}\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}}\rho(\vec{r}^{\prime})~dV^{\prime}
   +
   \dfrac{q}{4\pi\epsilon_{0}}\int_{S}\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}}\sigma(\vec{r}^{\prime})~da^{\prime}
   \\+
   \dfrac{q}{4\pi\epsilon_{0}}\int_{C}\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}}\lambda(\vec{r}^{\prime})~dl^{\prime}.\end{multline}

O Campo Elétrico
----------------

Quando colocamos uma carga elétrica na presença de outra carga, a
primeira sofre a influência da segunda à distância. Assim, podemos
considerar que a força sofrida pela primeira carga é resultado da
interação da mesma com o **campo elétrico** produzido pela segunda
carga. Para determinar o campo elétrico formalmente, consideramos que a
primeira carga é um carga teste, ou seja, uma carga muito pequena de
modo que não influencia o campo elétrico produzido pela carga que
origina o campo. Neste caso, o campo elétrico :math:`\vec{E}` é definido
por:

.. math::
   :label: campoeletrico
   
   \begin{aligned}
   \label{}
   \vec{E}=\lim_{q\rightarrow0}\dfrac{\vec{F}_{q}}{q}\end{aligned}

onde o limite explicita o fato de que a carga teste deve ser muito
pequena de maneira a não afetar a distribuição de cargas.

As expressões anteriores para a força elétrica nos permitem determinar a
equação para o campo elétrico. Vamos considerar o caso mais geral
possível, onde temos uma distribuição discreta de cargas além de
distribuições contínuas de volume, superfície e linear de cargas. Neste caso a força elétrica sobre
uma carga teste :math:`q` na posição :math:`\vec{r}` é dada por:

.. math::

   \begin{multline}
   \vec{F}_{q}(\vec{r})=\dfrac{q}{4\pi\epsilon_{0}}\sum_{i=1}^{N}q_{i}\dfrac{(\vec{r}-\vec{r}_{i})}{|\vec{r}-\vec{r}_{i}|^{3}}
   +
   \dfrac{q}{4\pi\epsilon_{0}}\int_{V}\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}}\rho(\vec{r}^{\prime})~dV^{\prime}
   \\+
   \dfrac{q}{4\pi\epsilon_{0}}\int_{S}\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}}\sigma(\vec{r}^{\prime})~da^{\prime}
   +
   \dfrac{q}{4\pi\epsilon_{0}}\int_{C}\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}}\lambda(\vec{r}^{\prime})~dl^{\prime}
   \end{multline}

e substituindo esta expressão na :eq:`campoeletrico` obtemos uma equação geral para o
campo elétrico produzido pela distribuição de cargas:

.. math::
   :label: campoeletrico:geral

   \begin{multline}
   \vec{E}(\vec{r})=\dfrac{1}{4\pi\epsilon_{0}}\sum_{i=1}^{N}q_{i}\dfrac{(\vec{r}-\vec{r}_{i})}{|\vec{r}-\vec{r}_{i}|^{3}}
   +
   \dfrac{1}{4\pi\epsilon_{0}}\int_{V}\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}}\rho(\vec{r}^{\prime})~dV^{\prime}
   \\+
   \dfrac{1}{4\pi\epsilon_{0}}\int_{S}\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}}\sigma(\vec{r}^{\prime})~da^{\prime}
   +
   \dfrac{1}{4\pi\epsilon_{0}}\int_{C}\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}}\lambda(\vec{r}^{\prime})~dl^{\prime}
   \end{multline}

Note que a expressão para o campo elétrico implica que este é uma função
vetorial puntual, :math:`\vec{E}=\vec{E}(\vec{r})` e, portanto, é um
campo vetorial. Assim, podemos atribuir a cada ponto do espaço um vetor
indicando o módulo, direção e sentido do campo elétrico. Uma maneira
complementar largamente utilizada para visualizar a forma do campo
elétrico foi formulada por M. Faraday e é baseada no conceito de linhas
de força. Uma linha de força é uma curva imaginária traçada de forma que
sua direção e sentido em qualquer ponto seja o do campo elétrico naquele
ponto. No caso de uma carga pontual isolada, as linhas de força estão na
direção radial com o sentido saindo da partícula no caso de cargas
positivas e entrando na partícula para cargas negativas. Na :numref:`rotulo` temos duas ilustrações das linhas de força para duas
cargas elétricas próximas uma da outra.



.. _rotulo:

.. figure:: _images/linhasdecampo.png
      :scale: 51%
      :align: center

      Linhas de força para duas cargas. (a) cargas de sinais opostos. (b) cargas de mesmo sinal.





O potencial Eletrostático
-------------------------

Considerando o campo elétrico na forma dada pela :eq:`campoeletrico:geral`, vamos calcular
:math:`\nabla\times\vec{E}` que se reduz à calcular o rotacional da
seguinte função vetorial:

.. math::

   \begin{aligned}
   \nabla\times\left(\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right),\end{aligned}

que é uma função escalar multiplicada por um vetor. Sabemos das
identidades vetoriais do livro-texto que
:math:`\nabla\times(\varphi\vec{G})=\nabla\varphi\times\vec{G}+\varphi\nabla\times\vec{G}`.
Assim, usando :math:`\varphi=|\hat{r}-\hat{r}^{\prime}|^{-3}` e
:math:`\vec{G}=\vec{r}-\vec{r}^{\prime}` então podemos escrever a
equação acima da seguinte forma:

.. math::

   \begin{aligned}
   \nabla\times\left(\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right)
   =
   \nabla\left(\dfrac{1}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right)\times(\vec{r}-\vec{r}^{\prime})+\dfrac{1}{|\vec{r}-\vec{r}^{\prime}|^{3}}\nabla\times(\vec{r}-\vec{r}^{\prime})\end{aligned}

e como :math:`\hat{r}^{\prime}` não depende de :math:`\hat{r}`, ou seja,
é uma constante, então sua derivada em relação à :math:`x,y,z` é nula e
usando :math:`\nabla\times\vec{r}=0`, podemos escrever simplesmente:

.. math::

   \begin{aligned}
   \nabla\times\left(\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right)
   =
   \nabla\left(\dfrac{1}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right)\times(\vec{r}-\vec{r}^{\prime})\end{aligned}

e como a função acima depende apenas do módulo de :math:`\hat{r}`,
podemos resolver a derivada usando a identidade vetorial dada pela :eq:`ids:eq1`, veja seção anterior:

.. math::

   \begin{aligned}
   \nabla\left(\dfrac{1}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right)
   =
   \dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}
   \dfrac{d}{dR}\left(\dfrac{1}{R^{3}}\right), \qquad R=|\vec{r}-\vec{r}^{\prime}|\end{aligned}

.. math::

   \begin{aligned}
   \nabla\left(\dfrac{1}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right)
   =
   -3\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}
   \dfrac{1}{|\vec{r}-\vec{r}^{\prime}|^{4}}=-3\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{5}}\end{aligned}

e vemos que é simplesmente uma função escalar multiplicando um vetor
:math:`\vec{r}-\vec{r}^{\prime}`. Com isso, podemos escrever

.. math::

   \begin{aligned}
   \nabla\times\left(\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right)
   =
   -3\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{5}}\times(\vec{r}-\vec{r}^{\prime})\end{aligned}

e como vetores paralelos resultam em produtor vetorial nulo, segue que:

.. math::

   \begin{aligned}
   \nabla\times\left(\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right)=0.\end{aligned}

Com esta relação geral, podemos mostrar que o rotacional do campo
elétrico :math:`\vec{E}`, dado pela :eq:`campoeletrico:geral`, é zero. Sendo assim,
aplicando o rotacional em ambos os membros da :eq:`campoeletrico:geral` obtemos o importante resultado:

.. math::

   \begin{aligned}
   \nabla\times\vec{E}=0.\end{aligned}

Com sabemos que :math:`\nabla\times\nabla\varphi=0`, então podemos
expressar o campo elétrico em termos de um gradiente de uma função
escalar. Assim, definimos o chamado **potencial elétrico**
:math:`\varphi(\vec{r})` por meio da relação

.. math::

   \begin{aligned}
   \label{potencial:eltr}
   \vec{E}=-\nabla\varphi(\vec{r}).\end{aligned}

No caso simples de uma carga puntual, é possível determinar o potencial
eletrostático a partir da equação acima e mostrar que este é dado por:

.. math::

   \begin{aligned}
   \label{potencial:carga}
   \varphi(\vec{r})=\dfrac{q}{4\pi\epsilon_{0}}\dfrac{1}{|\vec{r}-\vec{r}_{q}|}\end{aligned}

onde a carga elétrica está na posição :math:`\vec{r}_{q}` e o potencial
é medido na posição dada por :math:`\vec{r}`.

Partindo da mesma estratégia que adotamos para generalizar o campo
elétrico e a força elétrica para distribuições gerais de carga, podemos
escrever o potencial elétrico incluindo um número arbitrário de cargas
bem com distribuições volumétricas, superficiais e lineares da seguinte maneira:

.. math::
   :label: potencialeletrico:geral

   \begin{multline}
   \varphi(\vec{r})=\dfrac{1}{4\pi\epsilon_{0}}\sum_{i=1}^{N}\dfrac{q_{i}}{|\vec{r}-\vec{r}_{i}|}
   +
   \dfrac{1}{4\pi\epsilon_{0}}\int_{V}\dfrac{\rho(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}~dV^{\prime}
   \\+
   \dfrac{1}{4\pi\epsilon_{0}}\int_{S}\dfrac{\sigma(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}~da^{\prime}
   +
   \dfrac{1}{4\pi\epsilon_{0}}\int_{C}\dfrac{\lambda(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}~dl^{\prime}
   \end{multline}

o que pode ser verificado tomando a gradiente de :eq:`potencialeletrico:geral` para se chegar à expressão do campo elétrico, dada pela :eq:`campoeletrico:geral`.

Aplicando uma integração sobre o campo elétrico, podemos achar uma
expressão explícita para o potencial elétrico a partir do campo
:math:`\hat{E}(\vec{r})`. Assim, fazendo o produto escalar da equação :eq:`potencialeletrico:geral` com :math:`d\vec{r}`,
obtemos:

.. math::

   \begin{aligned}
   \label{potencial:eltr22}
   \hat{E}(\vec{r})\cdot d\vec{r}=-\nabla\varphi(\vec{r})\cdot d\vec{r}\end{aligned}

e vemos que o segundo membro se reduz a :math:`d\varphi` desde que se
trata da relação entre a diferencial total e o gradiente, e integrando,
podemos escrever:

.. math::

   \begin{aligned}
   \label{potencial:eltr2}
   \int_{\text{ref}}^{\varphi(\vec{r})}\nabla\varphi(\vec{r}^{\prime})\cdot d\vec{r}^{\prime}=\int_{\text{ref}}^{\varphi(\vec{r})}d\varphi=-\int_{\text{ref}}^{\vec{r}}\hat{E}(\vec{r}^{\prime})\cdot d\vec{r}^{\prime}\end{aligned}

onde estamos considerando uma integração desde um ponto tomado com o
referência até uma posição :math:`\vec{r}`\  [1]_. Considerando que
neste ponto de referência, usualmente tomado como sendo no infinito
(:math:`\vec{r}\rightarrow\infty`), o potencial é nulo, podemos
escrever:

.. math::
   :label: potencial:eltr3
   
   \begin{aligned}
   \varphi(\vec{r})=-\int_{\text{ref}}^{\vec{r}}\hat{E}(\vec{r}^{\prime})\cdot d\vec{r}^{\prime}\end{aligned}

No caso de uma carga puntual situada da origem, podemos calcular o
potencial devido a esta carga por integração direta do campo elétrico
cuja expressão é dada pela :eq:`campoeletrico:geral` fazendo-se :math:`N=1`,
e :math:`\rho=\sigma=\lambda=0`:

.. math::

   \begin{aligned}
   \vec{E}(\vec{r})=\dfrac{q}{4\pi\epsilon_{0}}\dfrac{\vec{r}-\vec{r}_{1}}{|\vec{r}-\vec{r}_{1}|^{3}}\end{aligned}

e como estamos considerando que a carga está localizada na origem então
:math:`\vec{r}_{1}=0` e assim, podemos escrever:

.. math::
   :label: celetrico
   
   \begin{aligned}
   \vec{E}(\vec{r})=\dfrac{q}{4\pi\epsilon_{0}}\dfrac{\vec{r}}{r^{3}}\end{aligned}

e substituindo este resultado na expressão dada pela :eq:`potencial:eltr3` , segue que:

.. math::

   \begin{aligned}
   \varphi(\vec{r})=-\dfrac{q}{4\pi\epsilon_{0}}\int_{\infty}^{\vec{r}}\dfrac{\vec{r}^{\prime}}{r^{\prime3}}\cdot d\vec{r}^{\prime}\end{aligned}

e considerando o fato de que
:math:`\vec{r}^{\prime}\cdot d\hat{r}^{\prime}=r^{\prime}dr^{\prime}`,
segue que:

.. math::

   \begin{aligned}
   \varphi(\vec{r})=-\dfrac{q}{4\pi\epsilon_{0}}\int_{\infty}^{r}\dfrac{1}{r^{\prime3}}r^{\prime}dr^{\prime}
   =
   -\dfrac{q}{4\pi\epsilon_{0}}\int_{\infty}^{r}\dfrac{dr^{\prime}}{r^{\prime2}}=-\dfrac{q}{4\pi\epsilon_{0}}\left[-\dfrac{1}{r^{\prime}}\right]^{r}_{\infty}\end{aligned}

.. math::

   \begin{aligned}
   \label{potencial:carga:puntual}
   \varphi(\vec{r})
   =
   \dfrac{q}{4\pi\epsilon_{0}r}\end{aligned}

que é potencial de uma carga puntual situada na origem. Esta expressão também pode ser obtida fazendo :math:`N=1`, :math:`\rho=\sigma=\lambda=0` e :math:`\vec{r}_{1}=0` na expressão para o potencial dada pela :eq:`potencialeletrico:geral`.

A força elétrica, assim como a força gravitacional, é uma força
conservativa. Assim, é possível definir uma energia potencial elétrica
da mesma forma que definimos uma energia potencial gravitacional. Com
efeito, a energia potencial é definida como o negativo do trabalho
realizado pela força, assim, escrevemos:

.. math::

   \begin{aligned}
   \Delta U(\vec{r})=-\int_{\text{ref}}^{\vec{r}}\hat{F}\cdot d\vec{r}\end{aligned}

e da :eq:`campoeletrico`, podemos escrever

.. math::

   \begin{aligned}
   \Delta U(\vec{r})=-q\int_{\text{ref}}^{\vec{r}}\hat{E}\cdot d\vec{r}\end{aligned}

e comparando com a equação para o potencial eletrostático
:math:`\varphi`, podemos escrever

.. math::

   \begin{aligned}
   \Delta U(\vec{r})=q\varphi(\vec{r})\qquad\therefore\qquad \varphi=\dfrac{\Delta U}{q}\end{aligned}

e vemos então que o potencial elétrico é simplesmente a energia
potencial elétrica por unidade de carga. O fato do campo elétrico ser
relacionado ao potencial por meio de :math:`\vec{E}=-\nabla\varphi` nos
permite mostrar que a integral de caminho da força elétrica é
independente do caminho entre dois pontos inicial e final. É esta
característica que nos permite definir uma energia potencial associada
associada ao trabalho realizado pela força elétrica.




Exemplos Resolvidos
~~~~~~~~~~~~~~~~~~~

**1. Reitz, Milford & Christy, Problema 2-1, item (a).** *Um disco
circular, de raio* :math:`R` *, tem uma densidade superficial uniforme de
carga* :math:`\sigma`. *Determine o campo elétrico em um ponto sobre o
eixo do disco a um distância* :math:`z` *do plano do disco.*

.. _disco:

.. figure:: _images/disco.png
      :scale: 51%
      :align: center

      Veja exemplo **1**.

.. centered:: Solução

   

Queremos o campo elétrico, então para isso utilizamos a equação geral
para o campo elétrico :eq:`campoeletrico:geral` para uma densidade
superficial de carga elétrica. Deste modo, podemos escrever:

.. math::

   \begin{aligned}
   \vec{E}(\vec{r})=
   \dfrac{1}{4\pi\epsilon_{0}}\int_{S}\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\sigma(\vec{r}^{\prime})~da^{\prime}\end{aligned}

e desde que a densidade de carga é uniforme, então não depende da
posição no disco e podemos tratá-la com constante. Assim, segue que:

.. math::

   \begin{aligned}
   \vec{E}(\vec{r})=
   \dfrac{\sigma}{4\pi\epsilon_{0}}\int_{S}\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}~da^{\prime}\end{aligned}

lembrando que se o disco de raio :math:`R` tem uma carga total
:math:`Q`, então :math:`\sigma=Q/\pi R^{2}`.

O próximo passo é determinar a forma dos vetores :math:`\vec{r}`,
:math:`\vec{r}^{\prime}` e do elemento de área :math:`da^{\prime}`. Para
isso, é dado do enunciado que a posição onde queremos o campo elétrico,
determinada por :math:`\vec{r}` é ao longo do eixo :math:`z`, assim,
podemos escrever :math:`\vec{r}=z\hat{\mathbf{k}}`. Além disso, como
temos um disco, usamos a simetria do mesmo por meio de coordenadas
polares planas em que :math:`x=r\cos\theta` e :math:`y=r\sin\theta`.
Neste caso :math:`da^{\prime}=r^{\prime}dr^{\prime}d\theta^{\prime}`.
Além disso,
:math:`\vec{r}=r^{\prime}\cos\theta^{\prime}\hat{\mathbf{i}}+r^{\prime}\sin\theta^{\prime}\hat{\mathbf{j}}`.
Com estes dados, podemos escrever:

.. math::

   \begin{aligned}
   \vec{r}-\vec{r}^{\prime}=z\hat{\mathbf{k}}-r^{\prime}\cos\theta^{\prime}\hat{\mathbf{i}}-r^{\prime}\sin\theta^{\prime}\hat{\mathbf{j}}\end{aligned}

e,

.. math::

   \begin{aligned}
   |\vec{r}-\vec{r}^{\prime}|=\sqrt{z^{2}+r^{\prime2}\cos^{2}\theta^{\prime}+r^{\prime2}\sin^{2}\theta^{\prime}}
   =
   \sqrt{z^{2}+r^{\prime2}}\qquad\therefore\qquad |\vec{r}-\vec{r}^{\prime}|^{3}=[z^{2}+r^{\prime2}]^{3/2}\end{aligned}

e considerando ainda que :math:`0<\theta^{\prime}<2\pi` e
:math:`0<r^{\prime}<R`, podemos escrever o campo elétrico na forma:

.. math::

   \begin{aligned}
   \vec{E}(\vec{r})=
   \dfrac{\sigma}{4\pi\epsilon_{0}}
   \int_{0}^{2\pi}\int_{0}^{R}
   \dfrac{(z\hat{\mathbf{k}}-r^{\prime}\cos\theta^{\prime}\hat{\mathbf{i}}-r^{\prime}\sin\theta^{\prime}\hat{\mathbf{j}})}{[z^{2}+r^{\prime2}]^{3/2}}~r^{\prime}dr^{\prime}d\theta^{\prime}\end{aligned}

.. math::

   \begin{aligned}
   \vec{E}(\vec{r})=
   \dfrac{\sigma}{4\pi\epsilon_{0}}
   \int_{0}^{R}\int_{0}^{2\pi}
   \dfrac{(z\hat{\mathbf{k}}-r^{\prime}\cos\theta^{\prime}\hat{\mathbf{i}}-r^{\prime}\sin\theta^{\prime}\hat{\mathbf{j}})}{[z^{2}+r^{\prime2}]^{3/2}}~d\theta^{\prime}r^{\prime}dr^{\prime}\end{aligned}

e as integrais em :math:`\theta` do seno e cosseno são nulas. Neste
caso, resta apenas o termo correspondente ao eixo :math:`z`. Com isso,
podemos escrever:

.. math::

   \begin{aligned}
   \vec{E}(\vec{r})=
   \dfrac{\sigma z\hat{\mathbf{k}}}{4\pi\epsilon_{0}}
   \int_{0}^{R}
   \dfrac{1}{[z^{2}+r^{\prime2}]^{3/2}}~r^{\prime}dr^{\prime}\int_{0}^{2\pi}d\theta^{\prime}\end{aligned}

e resolvendo a integral angular, ficamos com

.. math::

   \begin{aligned}
   \vec{E}(\vec{r})=
   \dfrac{2\pi\sigma z\hat{\mathbf{k}}}{4\pi\epsilon_{0}}
   \int_{0}^{R}
   \dfrac{r^{\prime}dr^{\prime}}{[z^{2}+r^{\prime2}]^{3/2}}\end{aligned}

e resta apenas a integral em :math:`r^{\prime}`. Esta integral pode ser
facilmente resolvida fazendo a substituição :math:`u=z^{2}+r^{\prime2}`,
de modo que :math:`du=2r^{\prime}dr^{\prime}`. Com isso, obtemos:

.. math::

   \begin{aligned}
   \vec{E}(\vec{r})=
   \dfrac{2\pi\sigma z\hat{\mathbf{k}}}{4\pi\epsilon_{0}}\dfrac{1}{2}
   \int_{z^{2}}^{z^{2}+R^{2}}
   \dfrac{du}{u^{3/2}}
   =
   -\dfrac{2\pi\sigma z\hat{\mathbf{k}}}{4\pi\epsilon_{0}}\dfrac{1}{2}2
   \left[\dfrac{1}{\sqrt{z^{2}+R^{2}}}-\dfrac{1}{\sqrt{z^{2}}}\right]\end{aligned}

.. math::

   \begin{aligned}
   \vec{E}(\vec{r})
   =
   \dfrac{\sigma z\hat{\mathbf{k}}}{2\epsilon_{0}}
   \left[\dfrac{1}{z}-\dfrac{1}{\sqrt{z^{2}+R^{2}}}\right]\end{aligned}

.. math::

   \begin{aligned}
   \vec{E}(\vec{r})
   =
   \dfrac{\sigma }{2\epsilon_{0}}
   \left[1-\dfrac{z}{\sqrt{z^{2}+R^{2}}}\right]\hat{\mathbf{k}}.\end{aligned}

Esta é a expressão procurada. Mas vamos considerar o caso em que estamos
medindo o campo elétrico em uma distância muito grande comparada ao raio
do disco. Neste caso, :math:`z\gg R`, de modo que podemos escrever:

.. math::

   \begin{aligned}
   \dfrac{z}{\sqrt{z^{2}+R^{2}}}=\dfrac{1}{\sqrt{1+\dfrac{R^{2}}{z^{2}}}}=\left[1+\dfrac{R^{2}}{z^{2}}\right]^{-1/2}\approx1-\dfrac{R^{2}}{2z^{2}}\end{aligned}

o que nos permite escrever

.. math::

   \begin{aligned}
   \vec{E}(\vec{r})
   \approx
   \dfrac{\sigma }{2\epsilon_{0}}
   \left[1-\left(1-\dfrac{R^{2}}{2z^{2}}\right)\right]\hat{\mathbf{k}}=\dfrac{\sigma}{2\epsilon_{0}}
   \dfrac{R^{2}}{2z^{2}}\hat{\mathbf{k}}=\end{aligned}

e usando a densidade de carga na forma :math:`\sigma=Q/\pi R^{2}`, segue
que:

.. math::

   \begin{aligned}
   \vec{E}(\vec{r})
   \approx
   \dfrac{Q}{4\pi R^{2}\epsilon_{0}}
   \dfrac{R^{2}}{z^{2}}\hat{\mathbf{k}}\end{aligned}

| 

  .. math::

     \begin{aligned}
     \vec{E}(z\gg R)
     \approx
     \dfrac{Q}{4\pi\epsilon_{0}z^{2}}\hat{\mathbf{k}}\end{aligned}

que é o campo gerado por uma carga pontual.

| **2. Reitz, Milford & Christy, Problema 2-6.** *Uma casca esférica fina, condutora, de raio* :math:`R`, *está uniformemente carregada com uma carga total* :math:`Q`. *Por integração direta encontre o potencial em um ponto arbitrário (a) no interior da casca (b) fora da casca.*


.. _sphere:

.. figure:: _images/sphere.png
      :scale: 47%
      :align: center

      Veja exemplo **2**.


.. centered:: Solução

O potencial eletrostático é dado pela Eq.
:eq:`potencialeletrico:geral`, adaptada para
o caso presente:

.. math::

   \begin{aligned}
   \varphi(\vec{r})=
   \dfrac{1}{4\pi\epsilon_{0}}\oint_{S}\dfrac{\sigma(\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|}~da^{\prime}\end{aligned}

onde indicamos que é uma integração sobre uma superfície fechada. Vamos
agora definir cada um dos termos que aparecem na expressão acima.
Novamente, usamos a densidade superficial de carga uniforme o que nos
permite escrever:

.. math::

   \begin{aligned}
   \varphi(\vec{r})=
   \dfrac{\sigma}{4\pi\epsilon_{0}}\oint_{S}\dfrac{da^{\prime}}{|\vec{r}-\vec{r}^{\prime}|}.\end{aligned}

Aqui é conveniente fazer uso das coordenadas polares esféricas, para
explorar a simetria da casca. Para isso, sem perda de generalidade
assumimos que o ponto onde vamos calcular o campo elétrico é dado por:
:math:`\vec{r}=r\hat{\mathbf{k}}`. O vetor :math:`\vec{r}^{\prime}` que
percorre a casca esférica é dado por:

.. math::

   \begin{aligned}
   \vec{r}^{\prime}=R\cos\phi^{\prime}\sin\theta^{\prime}\hat{\mathbf{i}}+R\sin\phi^{\prime}\sin\theta^{\prime}\hat{\mathbf{j}}+R\cos\theta^{\prime}\hat{\mathbf{k}}\end{aligned}

onde :math:`R` é o raio da casca esférica e os ângulos azimutal
:math:`\phi` e polar :math:`\theta` variam de no intervalo
:math:`0<\phi<2\pi` e :math:`0<\theta<\pi`. O elemento de superfície é
dado por
:math:`da^{\prime}=R^{2}\sin\theta^{\prime}d\theta^{\prime}d\phi^{\prime}`.
O módulo :math:`|\vec{r}-\vec{r}^{\prime}|` é dado por:

.. math::

   \begin{aligned}
   \vec{r}-\vec{r}^{\prime}
   =
   r\hat{\mathbf{k}}-R\cos\phi^{\prime}\sin\theta^{\prime}\hat{\mathbf{i}}-R\sin\phi^{\prime}\sin\theta^{\prime}\hat{\mathbf{j}}-R\cos\theta^{\prime}\hat{\mathbf{k}}\end{aligned}

.. math::

   \begin{aligned}
   |\vec{r}-\vec{r}^{\prime}|
   =
   \sqrt{(r-R\cos\theta^{\prime})^{2}+R^{2}\cos^{2}\phi^{\prime}\sin^{2}\theta^{\prime}+R^{2}\sin^{2}\phi^{\prime}\sin^{2}\theta^{\prime}}
   =
   \sqrt{r^{2}-2rR\cos\theta^{\prime}+R^{2}}.\end{aligned}

Substituindo os elementos na integração, podemos escrever:

.. math::

   \begin{aligned}
   \varphi(r\hat{\mathbf{k}})=
   \dfrac{\sigma}{4\pi\epsilon_{0}}\int_{0}^{2\pi}\int_{0}^{\pi}\dfrac{R^{2}\sin\theta^{\prime}d\theta^{\prime}d\phi^{\prime}}{\sqrt{r^{2}-2rR\cos\theta^{\prime}+R^{2}}}\end{aligned}

e vemos que a integração em :math:`\phi^{\prime}` pode ser realizada
imediatamente visto que o integrando não depende de
:math:`\phi^{\prime}`. Assim, ficamos com:

.. math::

   \begin{aligned}
   \varphi(r\hat{\mathbf{k}})=
   \dfrac{2\pi R^{2}\sigma}{4\pi\epsilon_{0}}\int_{0}^{\pi}\dfrac{\sin\theta^{\prime}d\theta^{\prime}}{\sqrt{r^{2}-2rR\cos\theta^{\prime}+R^{2}}}\end{aligned}

e novamente podemos efetuar a integração por substituição direta.
Fazendo :math:`u=r^{2}-2rR\cos\theta^{\prime}+R^{2}`,
:math:`du=2rR\sin\theta^{\prime}d\theta^{\prime}`. Com isso, podemos
escrever:

.. math::

   \begin{aligned}
   \varphi(r\hat{\mathbf{k}})=
   \dfrac{2\pi R^{2}\sigma}{4\pi\epsilon_{0}}\dfrac{1}{2rR}\int_{(r-R)^{2}}^{(r+R)^{2}}\dfrac{du}{\sqrt{u}}
   =
   \dfrac{2\pi R^{2}\sigma}{4\pi\epsilon_{0}}\dfrac{2}{2rR}[\sqrt{(r+R)^{2}}-\sqrt{(r-R)^{2}}]\end{aligned}

.. math::

   \begin{aligned}
   \varphi(r\hat{\mathbf{k}})=
   \dfrac{\sigma R}{2\epsilon_{0}r}[r+R-|r-R|]\end{aligned}

e aqui temos duas possibilidades:

.. math::

   \begin{aligned}
   \varphi(r\hat{\mathbf{k}})=
   \dfrac{\sigma R}{2\epsilon_{0}r}[r+R-r+R]=\dfrac{QR}{4\pi R^{2}}\dfrac{2R}{2\epsilon_{0}r}=\dfrac{Q}{4\pi\epsilon_{0}r}, \qquad r>R\end{aligned}

que é o campo elétrico fora da casca.

Vamos considerar o campo elétrico no interior da casca esférica, i.e.,
:math:`r<R`. Neste caso, obtemos:

.. math::

   \begin{aligned}
   \varphi(r\hat{\mathbf{k}})=
   \dfrac{\sigma R}{2\epsilon_{0}r}[r+R+r-R|]=\dfrac{\sigma R}{2\epsilon_{0}r}2r=\dfrac{\sigma R}{\epsilon_{0}}=\dfrac{Q}{4\pi\epsilon_{0}R}, \qquad r<R.\end{aligned}

O ponto aqui é verificar que o campo elétrico fora da casca, é referente
ao de uma carga puntual e dentro da casca é zero, pois
:math:`\vec{E}=-\nabla\varphi` e o potencial é constante. Este
comportamento é idêntico ao observado para o caso de um campo
gravitacional.

Condutores e Isolantes
----------------------

Antes de prosseguir com o estudo da eletrostática, é importante fazer
uma observação a respeito dos tipos diferentes de materiais em relação à
sua resposta a um campo elétrico. Neste sentido, podemos identificar
dois tipos de materiais que denominamos como condutores e isolantes
(dielétricos). Os condutores são materiais caracterizados por terem
elétrons fracamente ligados aos átomos da rede e, com isso, podem se
deslocar quase livremente pelo material. Como resultado, a presença do
campo elétrico produz uma corrente elétrica pelo material. Os isolantes,
por outro lado, têm seus elétrons fortemente presos aos átomos e o campo
elétrico introduz apenas uma pequena redistribuição dos elétrons em tornos
dos íons. Assim, a resposta elétrica de materiais isolantes e condutores
são completamente diferentes. No momento, vamos nos concentrar nos
materiais condutores que são mais simples de serem tratados e,
futuramente, consideraremos os materiais isolantes. A razão para os
condutores serem mais simples está relacionado à mobilidade das cargas.
Uma vez que um condutor é submetido a um campo elétrico constante no
tempo, as cargas elétricas passam a se mover no condutor até que uma
situação de equilíbrio se estabeleça, onde as forças resultantes sobre
todas as cargas são iguais a zero. No equilíbrio, o interior do condutor
apresenta um campo elétrico nulo, pois do contrário, haveriam forças e
as cargas se moveriam, o que é claramente uma situação de não-equilíbrio.
No caso de uma carga ser transferida para um condutor, ou seja, um
condutor com uma carga líquida em seu interior, estas cargas irão
interagir entre si de maneira que a situação de equilíbrio será
estabelecida com as cargas localizadas na superfície deste
condutor.

A Lei de Gauss
--------------

Já mostramos que um carga puntual localizada na origem, apresenta um
campo elétrico expresso pela :eq:`celetrico` . O campo elétrico produzido por
esta carga apresenta uma orientação radial, ou seja, em cada ponto do
espaço podíamos atribuir um vetor com orientação radial cujo sentido era
saindo ou entrando na carga elétrica dependendo se esta era positiva ou
negativa. Uma maneira alternativa de ilustrar o campo elétrico é por
meio das linhas de campo que são trajetórias no espaço tangente ao vetor
campo elétrico. É importante notar que essas linhas nunca se cruzam pois
em cada ponto do espaço temos apenas um, e somente um, valor para o
campo elétrico. Além de ilustrar a orientação do campo elétrico no
espaço, também podemos expressar a intensidade do campo elétrico por
meio da densidade do número de linhas de campo. Com efeito, o número de
linhas de campo que atravessam uma área :math:`S` perpendicular às
mesmas é proporcional à intensidade do campo elétrico. Para ilustrar,
imagine que temos uma superfície esférica envolvendo uma carga elétrica
positiva localizada na origem do sistema de coordenadas. Neste caso, se
temos :math:`N` linhas atravessando esta superfície, o campo elétrico é
proporcional à :math:`N/A` onde :math:`A=4\pi R^{2}` é a área da
superfície esférica. Como o número de linhas é fixo, então
:math:`E\propto N/4\pi R^{2}` e é proporcional ao inverso do quadrado da
distância da carga, o que está de acordo com a :eq:`celetrico`. Além disso, como o número de
linhas de campo é proporcional à carga elétrica, então,
:math:`N\propto q` o que nos permite escrever
:math:`E\propto q/4\pi R^{2}`. De fato a :eq:`celetrico` nos fornece
:math:`E=q/4\pi\epsilon_{0}R^{2}` o que nos mostra que o raciocínio
baseado nas linhas campo é consistente.

Antes de discutir propriamente a Lei de Gauss, precisamos da definição
do fluxo de um campo vetorial. Para um dado campo qualquer
:math:`\vec{B}`, o fluxo deste campo é definido da seguinte forma:

.. math::

   \begin{aligned}
   \label{fluxo:aberto}
   \Phi_{\vec{B}}=\int_{S}\vec{B}\cdot\hat{n}~da\end{aligned}

onde :math:`S` é uma superfície aberta com normal :math:`\hat{n}`. Note
que o fluxo nada mais é que a projeção do vetor :math:`\vec{B}` na
direção de :math:`\hat{n}` o que nos diz que só temos fluxo quando as
linhas de campo de :math:`\vec{B}` atravessam a superfície :math:`S`. O
fluxo é máximo quando essas linhas incidem perpendicularmente à
superfície. Podemos também considerar o fluxo do campo vetorial
:math:`\vec{B}` encerrado por uma superfície fechada :math:`S`, onde
escrevemos:

.. math::
   :label: fluxo:fechado

   \begin{aligned}
   \Phi_{\vec{B}}=\oint_{S}\vec{B}\cdot\hat{n}~da.\end{aligned}

Vamos agora considerar novamente a nossa carga puntual centrada na
origem do sistema de coordenadas. Considerando uma superfície esférica e
fechada em torno da carga elétrica, então o fluxo do campo elétrico
através desta superfície é determinado por meio da combinação das Eqs.
:eq:`celetrico` e :eq:`fluxo:fechado`.

.. math::

   \begin{aligned}
   \Phi_{\vec{E}}=\oint_{S}\vec{E}\cdot\hat{n}~da=\oint_{S}E\hat{r}\cdot\hat{r}~da\end{aligned}

e considerando que o módulo do campo elétrico é constante na superfície,
segue que:

.. math::

   \begin{aligned}
   \Phi_{\vec{E}}=E\oint_{S}\hat{r}\cdot\hat{r}~da=E4\pi r^{2}\end{aligned}

e substituindo módulo do campo elétrico da :eq:`celetrico`, obtemos finalmente:

.. math::

   \begin{aligned}
   \Phi_{\vec{E}}=\dfrac{q}{4\pi\epsilon_{0}r^{2}}4\pi r^{2}\end{aligned}

.. math::
   :label: PhiE

   \begin{aligned}
   \Phi_{\vec{E}}=\dfrac{q}{\epsilon_{0}}.\end{aligned}


Vemos então que o fluxo elétrico depende apenas da carga elétrica encerrada pela superfície. Além disso, isso evidencia que o fluxo independe do raio da superfície bem como a sua forma. O fato de que qualquer superfície fechada encerra o mesmo fluxo, nos permite a liberdade de escolher a superfície mais simples que carrega a simetria da distribuição de
cargas. De fato, tudo o que foi discutido vale para qualquer distribuição de cargas que esteja encerrada pela superfície Gaussiana desde um número discreto de cargas à distribuições lineares, superficiais ou volumétricas de carga. Portanto, a igualdade expressa pela :eq:`PhiE`, calculada por uma única carga pontual, vale para qualquer carga :math:`q` que esteja no interior da superfície Gaussiana e podemos expressar isso pela chamada **Lei de Gauss**:

.. admonition:: Lei de Gauss (forma integral)

   .. math::
      :label: leideGauss

      \begin{aligned}
      \oint_{S}\vec{E}\cdot\hat{n}~da=\dfrac{q}{\epsilon_{0}}.\end{aligned}

A :eq:`leideGauss` é um dos pilares fundamentais da
teoria eletromagnética e é um das equações de Maxwell do
eletromagnetismo. A equação acima está escrita na versão de uma equação
integral, mas podemos escrevê-la em uma forma diferencial por meio do
teorema do divergente. Com efeito, podemos escrever a carga elétrica no
interior da superfície :math:`S` como uma integral sobre uma densidade
volumétrica :math:`\rho`,

.. math::

   \begin{aligned}
   q=\int_{V}\rho(\vec{r}^{\prime})~dV\end{aligned}

e assim, a lei de Gauss dada pela :eq:`leideGauss`
pode ser expressa na forma:

.. math::

   \begin{aligned}
   \oint_{S}\vec{E}\cdot\hat{n}~da=\dfrac{1}{\epsilon_{0}}\int_{V}\rho(\vec{r}^{\prime})~dV\end{aligned}

e por meio do teorema do divergente podemos trocar a integração sobre
:math:`S` em uma integração sobre o volume :math:`V`:

.. math::

   \begin{aligned}
   \int_{V}\nabla\cdot\vec{E}~dV=\dfrac{1}{\epsilon_{0}}\int_{V}\rho(\vec{r}^{\prime})~dV,\end{aligned}

.. math::

   \begin{aligned}
   \int_{V}\left[\nabla\cdot\vec{E}-\dfrac{\rho}{\epsilon_{0}}\right]~dV=0,\end{aligned}

o que nos permite escrever:

.. admonition:: Lei de Gauss (forma diferencial)

   .. math::
      :label: leideGauss:diff
      
      \begin{aligned}
      \nabla\cdot\vec{E}=\dfrac{\rho}{\epsilon_{0}}.\end{aligned}

Verificação da Lei de Gauss
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Podemos derivar a lei de Gauss a partir da equação para o campo elétrico
devido à uma distribuição volumétrica de carga. Para isso, partimos da
expressão geral:

.. math::

   \begin{aligned}
   \vec{E}(\vec{r})=\dfrac{1}{4\pi\epsilon_{0}}\int_{V}\rho(\vec{r}^{\prime})\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{3}}~dV\end{aligned}

e tomando o divergente de ambos os lados, segue que:

.. math::
   :label: gauss:intermed
   
   \begin{aligned}
   \nabla\cdot\vec{E}(\vec{r})=\dfrac{1}{4\pi\epsilon_{0}}\int_{V}\rho(\vec{r}^{\prime})\nabla\cdot\left[\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right]~dV\end{aligned}

e vemos que precisamos calcular o divergente do produto de um vetor
:math:`\vec{r}-\vec{r}^{\prime}` por uma função escalar
:math:`|\vec{r}-\vec{r}^{\prime}|^{-3}`. Neste caso, podemos usar a
identidade
:math:`\nabla (\varphi\vec{F})=\nabla\varphi\cdot\vec{F}+\varphi\nabla\cdot\vec{F}`.
Com isso, podemos escrever:

.. math::

   \begin{aligned}
   \nabla\cdot\left[\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right]
   =
   \nabla(|\vec{r}-\vec{r}^{\prime}|^{-3})\cdot(\vec{r}-\vec{r}^{\prime})+|\vec{r}-\vec{r}^{\prime}|^{-3}\nabla\cdot(\vec{r}-\vec{r}^{\prime}).\end{aligned}

Note que :math:`\nabla\cdot(\vec{r}-\vec{r}^{\prime})=3`. Além disso, o
gradiente da função :math:`\nabla(|\vec{r}-\vec{r}^{\prime}|^{-3})` já
foi determinado e vale

.. math::

   \begin{aligned}
   \nabla\left(\dfrac{1}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right)
   =
   -3\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{5}}\end{aligned}

o que nos permite escrever

.. math::

   \begin{aligned}
   \nabla\cdot\left[\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right]
   &=
   -3\dfrac{(\vec{r}-\vec{r}^{\prime})}{|\vec{r}-\vec{r}^{\prime}|^{5}}\cdot(\vec{r}-\vec{r}^{\prime})
   +
   3\dfrac{1}{|\vec{r}-\vec{r}^{\prime}|^{3}}
   \\
   &=
   -3\dfrac{|\vec{r}-\vec{r}^{\prime}|^{2}}{|\vec{r}-\vec{r}^{\prime}|^{5}}
   +
   3\dfrac{1}{|\vec{r}-\vec{r}^{\prime}|^{3}}=0, \qquad\vec{r}\neq\vec{r}^{\prime}\end{aligned}

onde destacamos que os cálculos acima valem apenas na condição
:math:`\vec{r}\neq\vec{r}^{\prime}`.

Para resolver a equação acima incluindo o ponto
:math:`\vec{r}=\vec{r}^{\prime}`, usamos o teorema do divergente para
transformar a integral de volume em uma integral de superfície:

.. math::

   \begin{aligned}
   \int_{V}\nabla\cdot\left[\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right]~dV
   =
   \oint_{S}\left[\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right]\cdot\hat{n}~da.\end{aligned}

como a esfera é centrada no ponto :math:`\vec{r}^{\prime}` então a
normal é dada por

.. math::

   \begin{aligned}
   \hat{n}=\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|}\end{aligned}

o que nos permite escrever a integral de superfície da seguinte forma:

.. math::

   \begin{aligned}
   \oint_{S}\left[\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right]\cdot\hat{n}~da
   =
   \oint_{S}\dfrac{da}{|\vec{r}-\vec{r}^{\prime}|^{2}}\end{aligned}

e como :math:`|\vec{r}-\vec{r}^{\prime}|=R` é o raio da esfera, que é
uma quantidade fixa, podemos escrever:

.. math::

   \begin{aligned}
   \oint_{S}\left[\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right]\cdot\hat{n}~da
   =
   \dfrac{1}{R^{2}}\oint_{S}da=\dfrac{1}{R^{2}}4\pi R^{2}=4\pi,\end{aligned}

que é uma expressão que não depende do raio da esfera e, portanto,
inclui também o ponto :math:`\vec{r}=\vec{r}^{\prime}`, que é o ponto
que nos interessa. Como o divergente é nulo para
:math:`\vec{r}\neq\vec{r}^{\prime}`, o resultado :math:`4\pi` é oriundo
da contribuição da origem onde se situa a carga.

Para lidar com singularidades como essa, lançamos mão da chamada *função
delta de Dirac* que tem as seguinte propriedades:

.. math::

   \begin{aligned}
   \int \delta(x-x_{0})~dx=1,
   \\
   \int f(x)\delta(x-x_{0})~dx=f(x_{0})\end{aligned}

que são válidas somente se o intervalo de integração contenha o valor
:math:`x_{0}`.

Usando a função delta, podemos reescrever a equação acima para o
divergente da seguinte maneira:

.. math::

   \begin{aligned}
   \int_{V}\nabla\cdot\left[\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right]~dV
   =
   \int_{V}4\pi\delta(\vec{r}-\vec{r}^{\prime})~dV
   =
   4\pi\int_{V}\delta(\vec{r}-\vec{r}^{\prime})~dV=4\pi\times1=4\pi\end{aligned}

o que nos leva à seguinte propriedade que nos será extremamente útil:

.. math::

   \begin{aligned}
   \int_{V}\left\{\nabla\cdot\left[\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right]-4\pi\delta(\vec{r}-\vec{r}^{\prime})\right\}~dV
   =
   0\end{aligned}

donde

.. math::

   \begin{aligned}
   \label{divergente:util}
   \nabla\cdot\left[\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right]=4\pi\delta(\vec{r}-\vec{r}^{\prime}).\end{aligned}

Agora que resolvemos o divergente da função acima, estamos aptos a
calcular explicitamente o divergente do campo elétrico devido à uma
carga puntual. Assim, voltamos à demonstração da lei de Gauss a partir
da :eq:`gauss:intermed`,

.. math::

   \begin{aligned}
   \nabla\cdot\vec{E}(\vec{r})
   =\dfrac{1}{4\pi\epsilon_{0}}\int_{V}\rho(\vec{r}^{\prime})\nabla\cdot\left[\dfrac{\vec{r}-\vec{r}^{\prime}}{|\vec{r}-\vec{r}^{\prime}|^{3}}\right]~dV\end{aligned}

.. math::

   \begin{aligned}
   \nabla\cdot\vec{E}(\vec{r})
   =\dfrac{1}{4\pi\epsilon_{0}}\int_{V}\rho(\vec{r}^{\prime})4\pi\delta(\vec{r}-\vec{r}^{\prime})~dV
   =
   \dfrac{\rho(\vec{r})}{\epsilon_{0}}\end{aligned}

ou de modo geral

.. math::

   \begin{aligned}
   \nabla\cdot\vec{E}=\dfrac{\rho}{\epsilon_{0}}\end{aligned}

e integrando obtermos chegamos a lei de Gauss na forma integral.

.. _exemplos-resolvidos-1:

Exemplos Resolvidos
~~~~~~~~~~~~~~~~~~~

Vamos aplicar a lei de Gauss à alguns casos simples para ilustrar o seu
uso e sua simplicidade no cálculo do campo elétrico devido a algumas
distribuições de carga.

**1. Casca esférica de raio** :math:`R` **e densidade superficial de carga**
:math:`\sigma`. *Já discutimos o cálculo do potencial elétrico
produzido por uma casca esférica por meio de integração direta. Agora
vamos considerar o campo elétrico por meio da Lei de Gauss.*

.. centered:: Solução

Vamos considerar primeiramente o caso em que o ponto de interesse no
cálculo do campo elétrico é um ponto :math:`\vec{r}=r\hat{r}`, tal que
:math:`r<R` (ou seja, no interior da casca esférica). A forma geral da
Lei de Gauss é dada por:

.. math::

   \begin{aligned}
   \oint_{S}\vec{E}\cdot\hat{n}~da=\dfrac{q}{\epsilon_{0}}\end{aligned}

onde :math:`q` é carga no interior da superfície gaussiana. No caso da
casca, também consideramos uma superfície gaussiana esférica de raio
:math:`r`. Assim, o vetor normal é :math:`\hat{r}` para fora da casca.
No interior da casca esférica não temos nenhuma carga livre e assim,
podemos escrever:

.. math::

   \begin{aligned}
   \oint_{S}E_{r}\hat{r}\cdot\hat{r}~da=\dfrac{0}{\epsilon_{0}}=0\end{aligned}

o que nos permite escrever

.. math::

   \begin{aligned}
   E_{r}\oint_{S}~da=0\qquad\therefore\qquad E_{r}=0.\end{aligned}

Assim, podemos escrever que

.. math::

   \begin{aligned}
   \vec{E}=0,\qquad r<R.\end{aligned}

Agora vamos considerar o caso em que temos um ponto fora da casca
esférica, ou seja, :math:`r>R`. Com isso, podemos escrever:

.. math::

   \begin{aligned}
   \oint_{S}\vec{E}\cdot\hat{n}~da=\dfrac{q}{\epsilon_{0}}\end{aligned}

e considerando que a carga da casca esférica é :math:`Q`, então a
superfície gaussiana esférica de raio :math:`r>R` envolve toda a carga.
Com isso, podemos escrever:

.. math::

   \begin{aligned}
   \oint_{S}E_{r}\hat{r}\cdot\hat{r}~da=\dfrac{Q}{\epsilon_{0}},\qquad E_{r}\oint da=\dfrac{Q}{\epsilon_{0}}\end{aligned}

com isso, chegamos a

.. math::

   \begin{aligned}
   E_{r}4\pi r^{2}=\dfrac{Q}{\epsilon_{0}}\end{aligned}

o que nos permite escrever

.. math::

   \begin{aligned}
   E_{r}=\dfrac{Q}{4\pi\epsilon_{0}r^{2}}\end{aligned}

ou ainda na forma vetorial

.. math::

   \begin{aligned}
   \vec{E}(r)=\dfrac{Q}{4\pi\epsilon_{0}r^{2}}\hat{r},\qquad r>R.\end{aligned}

Uma vez que temos a expressão para os campos elétricos, podemos
determinar os valores dos potenciais elétricos. Para isso, relembramos a
expressão geral para o potencial elétrico que é dada por:

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\varphi(\text{ref})-\int_{\text{ref}}^{\vec{r}}\vec{E}\cdot d\vec{r}\end{aligned}

e vamos determinar o campo elétrico fora da casca esférica de modo que
:math:`\varphi(\vec{r}\rightarrow\infty)=0`.

.. math::

   \begin{aligned}
   \varphi(\vec{r})=-\int_{\infty}^{\vec{r}}\vec{E}\cdot d\vec{r}\end{aligned}

.. math::

   \begin{aligned}
   \varphi(\vec{r})=-\int_{\infty}^{r}\dfrac{Q}{4\pi\epsilon_{0}r^{2}}\hat{r}\cdot \hat{r}dr\end{aligned}

.. math::

   \begin{aligned}
   \varphi(\vec{r})=-\dfrac{Q}{4\pi\epsilon_{0}}\int_{\infty}^{r}\dfrac{dr}{r^{2}}=\dfrac{Q}{4\pi\epsilon_{0}r}\end{aligned}

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\dfrac{Q}{4\pi\epsilon_{0}r}, \qquad r>R\end{aligned}

Considerando que o campo elétrico na superfície da esfera é simplesmente
obtido fazendo :math:`r=R` na expressão acima, podemos escrever:

.. math::

   \begin{aligned}
   \varphi(R)=\dfrac{Q}{4\pi\epsilon_{0}R}, \qquad r=R.\end{aligned}

Agora estamos aptos a calcular o potencial elétrico no interior da casca
esférica. Assim, podemos escrever:

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\varphi(R)-\int_{R}^{\vec{r}}\vec{E}\cdot d\vec{r}\end{aligned}

e como o campo elétrico no interior da casca esférica é zero, então

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\varphi(R)\end{aligned}

e substituindo o valor potencial elétrico na superfície da esfera, segue
que:

.. math::

   \begin{aligned}
   \varphi(\vec{r})=\dfrac{Q}{4\pi\epsilon_{0}R},\qquad r<R.\end{aligned}

**2. Fio retilíneo infinito com densidade linear de cargas**
:math:`\lambda`. *Vamos considerar uma superfície gaussiana de forma
cilíndrica em torno do fio [veja* :numref:`cylinder` *]. Assim, como o campo elétrico tem simetria
radial, podemos escrever* :math:`\vec{E}=E\hat{\rho}`, *onde o versor*
:math:`\hat{\rho}` *é definido no plano* :math:`xy` *e o fio está dirigido
ao longo do eixo* :math:`z`.

.. centered:: Solução

.. _cylinder:

.. figure:: _images/gauss_cilindro.png
      :scale: 70%
      :align: center

      Veja exemplo **2**. Superfície gaussiana cilíndrica envolvendo o fio infinito carregado com densidade de carga :math:`\lambda`.

Assim, podemos escrever

.. math::

   \begin{aligned}
   \oint_{S}\vec{E}\cdot\hat{n}~da=\dfrac{q}{\epsilon_{0}}\end{aligned}

e particularizando para o caso do fio, segue que:

.. math::

   \begin{aligned}
   \int_{S_{1}}\vec{E}\cdot\hat{n}_{1}~da+\int_{S_{2}}\vec{E}\cdot\hat{n}_{2}~da+\int_{S_{3}}\vec{E}\cdot\hat{n}_{3}~da
   =
   \dfrac{1}{\epsilon_{0}}\int_{\text{fio}}\lambda~dl\end{aligned}

onde as superfícies :math:`S_{1}` e :math:`S_{2}` são as tampas do
cilindro com normais :math:`\hat{\mathbf{k}}` e
:math:`-\hat{\mathbf{k}}`. As contribuições destas superfícies são nulas
porque o campo elétrico é radial, e portanto, perpendicular ao eixo
:math:`z`. Resta ainda a superfície :math:`S_{3}` que é a área lateral
do cilindro com normal :math:`\hat{n}_{3}=\hat{\rho}`. Com isso, podemos
escrever:

.. math::

   \begin{aligned}
   \int_{\text{lateral}}E_{\rho}\hat{\rho}\cdot\hat{\rho}~da
   =
   \dfrac{1}{\epsilon_{0}}\int_{\text{fio}}\lambda~dl\end{aligned}

o que pode ser escrito na forma

.. math::

   \begin{aligned}
   \int_{0}^{2\pi}\int_{-\infty}^{+\infty}E_{\rho}\hat{\rho}\cdot\hat{\rho}~rd\phi dz
   =
   \dfrac{1}{\epsilon_{0}}\int_{-\infty}^{\infty}\lambda~dz\end{aligned}

.. math::

   \begin{aligned}
   2\pi r E_{\rho}\int_{-L/2}^{+L/2}~ dz
   =
   \dfrac{\lambda}{\epsilon_{0}}\int_{-L/2}^{L/2}~dz\end{aligned}

onde consideramos uma superfície Gaussiana de comprimento :math:`L`.
Simplificando, obtemos o resultado final:

.. math::

   \begin{aligned}
   \vec{E}
   =
   \dfrac{\lambda}{2\pi \epsilon_{0}r}\hat{\rho}.\end{aligned}

**3. Campo elétrico de um plano infinito de cargas negativas.** *Aqui
novamente consideramos uma superfície gaussiana cilíndrica. Neste caso,
o campo elétrico é perpendicular ao plano infinito de modo que este é
normal às tampas do cilindro e assim, a contribuição da superfície
lateral é nula pois o campo elétrico é perpendicular a esta superfície.
Com isso, podemos escrever:*


.. centered:: Solução

.. _plane:

.. figure:: _images/plane.png
      :scale: 50%
      :align: center

      Veja exemplo **3**. 




.. math::

   \begin{aligned}
   \int_{S_{1}}\vec{E}\cdot\hat{n}_{1}~da+\int_{S_{2}}\vec{E}\cdot\hat{n}_{2}~da+\int_{S_{3}}\vec{E}\cdot\hat{n}_{3}~da
   =
   \dfrac{\sigma}{\epsilon_{0}}\int_{\text{plano}}~da\end{aligned}

.. math::

   \begin{aligned}
   \int_{S_{1}}E_{z}(\hat{\mathbf{k}})\cdot(-\hat{\mathbf{k}})~da+\int_{S_{2}}E_{z}(-\hat{\mathbf{k}})\cdot\hat{\mathbf{k}}~da
   =
   \dfrac{\sigma}{\epsilon_{0}}\int_{\text{plano}}~da\end{aligned}

.. math::

   \begin{aligned}
   -E_{z}\pi r^{2}-E_{z}\pi r^{2}
   =
   \dfrac{\sigma}{\epsilon_{0}}\pi r^{2}\qquad\therefore\qquad E_{z}=-\dfrac{\sigma}{2\epsilon_{0}}\end{aligned}

que pode ser escrito na forma:

.. math::

   \begin{aligned}
   \label{E:planodecargas}
   \vec{E}=\dfrac{\sigma}{2\epsilon_{0}}\hat{n}\end{aligned}

onde absorvemos o sinal da densidade de cargas no sinal da normal ao
plano. Note que o campo elétrico gerado pelo plano infinito é
independente da distância do ponto ao plano. Note que perto do plano, o
campo elétrico é oriundo das cargas do plano, próximas ao ponto
considerado. As contribuições de regiões no plano distantes do ponto tem
componentes do campo aproximadamente paralelas e assim, as componentes
perpendiculares destes pontos são desprezíveis. No caso em que o ponto
está longe do plano, as contribuições das cargas longe do ponto
considerado têm componentes praticamente perpendiculares a ele o que
ajuda a compensar a redução da intensidade do campo elétrico devido às
regiões próximas à projeção do ponto no plano. No final das contas, a
intensidade do campo elétrico permanece constante.

.. [1]
   Note que neste último passo trocamos a variável de integração para
   variáveis com linha para distinguí-lo do limite superior que é fixo
