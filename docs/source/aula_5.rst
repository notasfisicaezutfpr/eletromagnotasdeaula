Corrente Elétrica
=================

:math:`\phantom{\text{pargraph}}` Nos capítulo anteriores, nos concentramos em sistemas eletrostáticos, caractereizados pelo fato das cargas estarem em repouso. Desejamos agora considerar cargas em movimento. Isso implica tratarmos com condutores, onde os elétrons estão livres para se moverem sob a ação de campos elétricos. Esta definição não inclui apenas metais e ligas mas também semicondutores, eletrólitos, gases ionizados, etc. As cargas em movimento constituem uma *corrente elétrica* e o processo por meio da qual a carga é transportada é chamado de *condução*.

:math:`\phantom{\text{pargraph}}` Formalmente a corrente elétrica, :math:`I`, é definida como a razão segundo a qual a carga é transportada através de uma dada superfície em um sistema condutor:

.. math::
   :label: corrente:def

   \begin{aligned}
   I=\dfrac{dq}{dt},\end{aligned}

onde :math:`q=q(t)` é a carga transportada em um tempo :math:`t` através de uma superfície perpendicular ao movimento das cargas. A corrente elétrica é medida em coulombs/segundo que, no sistema internacional de unidades, é o ampère (A):

.. math::

   \begin{aligned}
   1~\text{A}=\dfrac{1~\text{C}}{1~\text{s}}.\end{aligned}

:math:`\phantom{\text{pargraph}}` Usualmente estabelecemos uma corrente elétrica em um meio condutor conectando-o a uma bateria. No entanto, qualquer dispositivo que garanta uma diferença de potencial fixa ao longo do condutor permite que uma corrente elétrica seja estabelecida.

:math:`\phantom{\text{pargraph}}` Dizemos, de modo genérico que este dispositivo é uma fonte de força eletromotriz (f. e. m. ) pois é um mecanismo que produz uma separação de cargas ou que move as cargas de um lado para outro, daí o nome eletromotriz. A fonte de fem atua realizando um trabalho sobre as cargas aumentando sua energia potencial por meio da realização de um trabalho
sobre estas cargas. Com efeito, se aplicamos um campo elétrico ao longo de um fio condutor, digamos da direita para a esquerda, os elétrons de carga negativa são atraídos para a extremidade direita do fio, o que irá gerar um campo elétrico de sentido contrário ao aplicado. No equilíbrio, o campo interno anula o campo externo e a corrente elétrica ao longo do fio vai a zero. O que a fonte faz é devolver os elétrons para a extremidade direita do fio por meio de uma diferença de potencial fixa mantida em suas extremidades.

.. _femfig:

.. figure:: _images/fem.png
      :scale: 51%
      :align: center

      Corrente elétrica através de um condutor devido à um campo elétrico externo. O campo :math:`\vec{E}_{\text{ext}}` produz um fluxo de cargas da esquerda para direita.




Densidade de corrente
---------------------

:math:`\phantom{\text{pargraph}}` O fluxo de corrente elétrica pode estar ocorrendo através de um fio cuja espessura é desprezível, porém, muitas vezes precisamos considerar que o fluxo das cargas elétricas está distribuído em um volume :math:`V` e, deste modo, importa a orientação da velocidade dos portadores. A definição escalar da corrente como utilizamos acima está relacionada à um fluxo perpendicular a uma área atravessada pelos portadores. Isso é ilustrado na :numref:`condutor` onde uma seção de um fio é submetida a um campo elétrico :math:`\vec{E}` orientado paralelamente ao seu comprimento. Para determinar a corrente que flui neste condutor, consideramos que o mesmo tem uma área :math:`A`, perpendicular ao campo elétrico. Considerando que temos uma densidade de portadores de carga :math:`n`, então o número de portadores que atravessam a área :math:`A`, num intervalo de tempo :math:`dt`, são aqueles contidos no elemento de volume dado por :math:`Adl`, onde :math:`dl` é a distância percorrida pelas cargas ao longo do condutor. Este volume está destacado na :numref:`condutor`.


.. _condutor:

.. figure:: _images/condutor.png
      :scale: 45%
      :align: center

      Corrente elétrica através de um condutor devido à um campo elétrico externo. Este campo produz um fluxo de cargas da esquerda para direita.



:math:`\phantom{\text{pargraph}}` Com estas considerações, a carga :math:`dQ` contida no volume :math:`Adl`, é dada por:

.. math::
   :label: carganovol

   \begin{aligned}
   dQ=e n Adl,\end{aligned}

onde :math:`e` é carga elementar. Considerando que os portadores de
carga se deslocam dentro do condutor com uma velocidade :math:`v`,
então, no intervalo de tempo :math:`dt`, temos que :math:`dl=vdt`,
assim, substituindo na :eq:`carganovol` temos:

.. math::
   :label: cargavol

   \begin{aligned}
   dQ=en A v dt.\end{aligned}

:math:`\phantom{\text{pargraph}}` Usando a definição da corrente elétrica, dada pela Eq., podemos escrever que :math:`dQ=Idt`
e, comparando com a :eq:`cargavol`, obtemos:

.. math::
   :label: corrvol

   \begin{aligned}
   I=envA.\end{aligned}

:math:`\phantom{\text{pargraph}}` A corrente :math:`I` está distribuída sobre a área :math:`A`, deste modo, é conveniente trabalhar com uma quantidade que forneça a informação sobre o fluxo de cargas pontualmente, assim, definimos a
chamada densidade de corrente, :math:`J`, como sendo a razão da corrente pela área atravessada por ela. Assim, escrevemos:

.. math::
   :label: densidade:correq1

   \begin{aligned}
   J=\dfrac{I}{A}.\end{aligned}

:math:`\phantom{\text{pargraph}}` Na definição dada pela :eq:`densidade:correq1` a área :math:`A` que aparece no denominador é a área perpendicular à corrente elétrica. Note que este é o caso ilustrado na :numref:`condutor` e, assim, a densidade de corrente para este condutor é determinada diretamente por meio da substituição da :eq:`corrvol` na :eq:`densidade:correq1`:

.. math::
   :label: jota

   \begin{aligned}
   J=env,\end{aligned}

ou, como ambos :math:`\vec{J}` e :math:`\vec{v}` são grandezas vetoriais, podemos escrever:

.. math::
   :label: jotavec

   \begin{aligned}
   \vec{J}=en\vec{v}.\end{aligned}


:math:`\phantom{\text{pargraph}}` As grandezas que aparecem no lado direito são definidas em cada ponto do condutor e, assim, a densidade de corrente nos permite ter uma noção de como o fluxo de cargas pode variar sobre a área do condutor. A corrente :math:`I`, por outro lado, envolve a área atravessada pela corrente e é, portanto, uma grandeza global que caracteriza o fluxo de cargas.

:math:`\phantom{\text{pargraph}}` Até aqui analisamos o fluxo de cargas para o caso particular ilustrado na :numref:`condutor`. Este é caso mais simples visto que o campo elétrico que gera o movimento das cargas está orientado ao longo do comprimento do condutor. Se mudamos a direção do campo elétrico, o fluxo de cargas mudará a orientação, caracterizada pela direção da velocidade
dos portadores. Deste modo, a definição da densidade de corrente feita
acima precisa ser generalizada para o caso geral em que as cargas se
deslocam ao longo de uma direção arbitrária em relação à área :math:`A`.

:math:`\phantom{\text{pargraph}}` O caso geral do fluxo de carga é ilustrado na :numref:`fluxo2`, onde as linhas de corrente verticais ilustram o movimento das cargas elétricas. A direção e sentido do vetor :math:`\vec{J}` é determinado pelo vetor velocidade dos portadores. Vemos na :numref:`fluxo2` que o fluxo depende da orientação relativa entre os vetores :math:`\hat{n}` e :math:`\vec{J}`,
caracterizada pelo ângulo :math:`\theta`. Quando os vetores :math:`\hat{n}` e :math:`\vec{J}` são paralelos (:math:`\theta=0`), isso implica que o fluxo atravessa perpendicularmente a superfície :math:`S`. Para ângulos intermediários :math:`\theta_{1}` e :math:`\theta_{2}(>\theta_{1})`, o fluxo é reduzido e vai a zero quando
os vetores :math:`\hat{n}` e :math:`\vec{J}` são perpendiculares. De fato, para :math:`\theta=\pi/2`, as linhas de corrente são paralelas à superfície :math:`S`, assim, nenhuma linha de corrente atravessa a superfície :math:`S`.

.. _fluxo2:

.. figure:: _images/fluxo.png
      :scale: 40%
      :align: center

      O fluxo de cargas elétricas caracterizado pelo vetor densidade de corrente :math:`\vec{J}`. A orientação relativa do fluxo das cargas com respeito à superfície :math:`S` é determinado pelo ângulo, :math:`\theta`, entre o versor :math:`\hat{n}`, normal à :math:`S`, e o vetor densidade de corrente, :math:`\vec{J}`. O fluxo varia desde um valor máximo quando :math:`\theta=0` até zero quando :math:`\theta=\pi/2`.


:math:`\phantom{\text{pargraph}}` A orientação pode variar pontualmente sobre toda área :math:`S`, e o fluxo é sempre proporcional à orientação relativa entre os vetores :math:`\hat{n}` e :math:`\vec{J}`. A corrente elétrica é então dada pela soma sobre o fluxo em cada ponto sobre a superfície e, desta forma, a corrente elétrica pode ser determinada mediante integração:

.. math::
   :label: corrente:int

   \begin{aligned}
   I=\int_{S}\vec{J}\cdot\hat{n}~da.\end{aligned}


Conservação da corrente elétrica : a equação da continuidade
-------------------------------------------------------------

:math:`\phantom{\text{pargraph}}` No contexto da eletrostática, mencionamos que a carga é conservada. Naturalmente, este fato permanece válido no contexto de cargas em movimento e é expresso por meio da chamada equação da continuidade. Esta equação relaciona a densidade de corrente, :math:`\vec{J}`, e a densidade de carga,  :math:`\rho`. Vamos aplicar a definição da densidade de corrente dada pela :eq:`corrente:int` para uma  superfície fechada, :math:`S`, como ilustrado na :numref:`volumecorrente`. Esta superfície delimita um volume :math:`V` contendo uma densidade volumétrica de carga que varia com o tempo uma vez que a corrente está emergindo através da superfície, :math:`S`.

.. _volumecorrente:

.. figure:: _images/volumecorrente.png
   :scale: 51%
   :align: center

   Corrente emergindo de um volume :math:`V` delimitado por uma superfície, :math:`S`.




:math:`\phantom{\text{pargraph}}` A corrente :math:`I`, que emerge da superfície :math:`S` é dada pela :eq:`corrente:int`:

.. math::

   \begin{aligned}
   I=\oint_{S}\vec{J}\cdot\hat{n}~da.\end{aligned}

:math:`\phantom{\text{pargraph}}` Note que a superfície :math:`S` é fechada e, assim, podemos escrever
:math:`I` em termos de uma integral de volume por meio do teorema do
divergente:

.. math::
   :label: corrente:vol

   \begin{aligned}
   I=\int_{V}\nabla\cdot\vec{J}~dV.\end{aligned}

:math:`\phantom{\text{pargraph}}` A corrente elétrica também pode ser expressa em termos da taxa que a densidade volumétrica de carga varia no tempo, assim, podemos escrever:

.. math::

   \begin{aligned}
   I=\int_{V}\dfrac{\partial\rho}{\partial t}~dV.\end{aligned}

:math:`\phantom{\text{pargraph}}` Para haver conservação de carga é necessário que a carga que está fluindo através da superfície :math:`S` seja a mesma carga que foi reduzida dentro do volume :math:`V`. Assim, devemos ter:

.. math::

   \begin{aligned}
   \int_{V}\nabla\cdot\vec{J}~dV
   =-\int_{V}\dfrac{\partial\rho}{\partial t}~dV\end{aligned}

:math:`\phantom{\text{pargraph}}` Para que a equação acima seja satisfeita, devemos ter:

.. math::

   \begin{aligned}
   \nabla\cdot\vec{J}=-\dfrac{\partial\rho}{\partial t}\end{aligned}

ou ainda,

.. math::
   :label: eqcontinuidade

   \begin{aligned}
   \nabla\cdot\vec{J}+\dfrac{\partial\rho}{\partial t}=0.\end{aligned}

:math:`\phantom{\text{pargraph}}` A :eq:`eqcontinuidade` é a chamada equação da continuidade e é uma expressão da conservação da carga.

Resistência Elétrica e Lei de Ohm
---------------------------------

:math:`\phantom{\text{pargraph}}` Desde o início do estudo da condução elétrica em materiais, era sabido que alguns materiais conduziam corrente elétrica melhor do que outros. Em particular, os materiais chamados condutores apresentam uma relação de proporcionalidade entre diferença de potencial e corrente:

.. math::

   \begin{aligned}
   \Delta\varphi\propto I,\end{aligned}

com isso, definimos a chamada resistência elétrica, :math:`R`, de modo
que:

.. math::
   :label: resistencia

   \begin{aligned}
   \Delta \varphi=RI.\end{aligned}

:math:`\phantom{\text{pargraph}}` A :eq:`resistencia` é a chamada *lei de Ohm* e estabelece uma relação linear entre a corrente elétrica e a diferença de
potencial estabelecida ao longo do condutor.

:math:`\phantom{\text{pargraph}}` A unidade de medida da resistência elétrica no sistema internacional é o ohm (:math:`\Omega`):

.. math::

   \begin{aligned}
   1~\text{ohm}=\dfrac{1~\text{volt}}{1~\text{ampère}}.\end{aligned}

:math:`\phantom{\text{pargraph}}` A resistência elétrica está relacionada com a dissipação da energia elétrica na forma de calor em um condutor. Quando os elétrons estão se movendo no interior do condutor, ocorre espalhamentos destes elétrons pelos íons da rede cristalina e pelos defeitos do material. Estes
espalhamentos constituem forças dissipativas convertendo parte da energia cinética dos elétrons em calor. De fato, é possível demonstrar experimentalmente que os condutores apresentam uma resistência que varia
com a temperatura (:math:`T`), em geral, há uma redução linear com
:math:`T` para o caso de metais.

:math:`\phantom{\text{pargraph}}` Vemos da :eq:`resistencia` que :math:`R` é uma propriedade macroscópica, da mesma forma que a corrente elétrica. Deste
modo, :math:`R` depende da geometria do condutor. Considerando que o comprimento :math:`d` é definido ao longo da direção por onde passa a corrente elétrica e :math:`A` é a área perpendicular ao fluxo da corrente, então podemos mostrar que a resistência elétrica é dada por:

.. math::
   :label: resistencia:eq2

   \begin{aligned}
   R=\rho\dfrac{d}{A},\end{aligned}

onde a constante de proporcionalidade :math:`\rho` é a chamada
resistividade elétrica do condutor. Esta quantidade é definida
pontualmente dentro do condutor e depende apenas das propriedades
microscópicas do material.

:math:`\phantom{\text{pargraph}}` Considere que o campo elétrico esteja direcionado ao longo do condutor e que a área :math:`A` tenha direção perpendicular ao fluxo da corrente elétrica. Neste caso, a diferença de potencial pode ser relacionada ao campo elétrico por: :math:`\Delta\varphi=Ed`. A corrente elétrica pode ser expressa em termos da densidade de corrente por :math:`I=JA`. Substituindo-se estes dados e a expressão da resistência elétrica, dada pela :eq:`resistencia:eq2`, na :eq:`resistencia`, obtemos:

.. math::

   \begin{aligned}
   \Delta\varphi=RI\end{aligned}

.. math::

   \begin{aligned}
   Ed=\rho\dfrac{d}{A}JA\qquad\therefore\qquad E=\rho J\end{aligned}

e, na forma vetorial, podemos escrever ainda:

.. math::
   :label: leideOhm:micro

   \begin{aligned}
   \vec{E}=\rho\vec{J}.\end{aligned}

:math:`\phantom{\text{pargraph}}` A :eq:`leideOhm:micro`  é a versão microscópica da lei de Ohm, ou seja, é a relação dada pela :eq:`resistencia:eq2`, porém, válida para cada ponto no
condutor.

:math:`\phantom{\text{pargraph}}` Também é comum definir a condutividade elétrica :math:`g`, dada por:

.. math::

   \begin{aligned}
   g=\dfrac{1}{\rho}\end{aligned}

e, assim, a lei de Ohm microscópica pode ser escrita na forma:

.. math::
   :label: leideOhm:micro2

   \begin{aligned}
   \vec{J}=g\vec{E}.\end{aligned}



Teoria microscópica da condução em metais
+++++++++++++++++++++++++++++++++++++++++


:math:`\phantom{\text{pargraph}}` Vamos considerar a origem da condutância :math:`g` que definimos acima. Para isso, considere a dinâmica de um elétron sob a ação de um campo elétrico. A dinâmica é determinada por meio da 2ª lei de Newton, assim, se o elétron tem massa :math:`m` e carga :math:`q`, podemos escrever:


.. math::
  :label: leiNewton

    \begin{aligned}
    m\dfrac{d\vec{v}}{dt}=q\vec{E},
    \end{aligned}

e, considerando que o campo elétrico é constante, então podemos resolver a equação diferencial dada pela :eq:`leiNewton` facilmente:

.. math::
  :label: vt

    \begin{aligned}
    \vec{v}(t)=\vec{v}_{0}+\dfrac{q}{m}\vec{E}t.
    \end{aligned}


:math:`\phantom{\text{pargraph}}` A :eq:`vt` nos indica que a velocidade do elétron irá crescer linearmente com o tempo, assim, em princípio teríamos uma velocidade infinita para :math:`t\rightarrow\infty`. É claro que este tipo de comportamento é inconsistente e, de fato, o que realmente ocorre é que os elétrons apresentam uma velocidade constante no tempo. O problema é que na :eq:`vt` não foi introduzido outro efeito extremamente importante que afeta a dinâmica do elétron: as colisões com os íons e demais elétrons que compõem o metal.

.. _drude:

.. figure:: _images/drude.png
    :scale: 50%
    :align: center

    Trajetória de um elétron dentro de um metal. Embora o deslocamento :math:`\vec{r}` esteja na mesma direção do campo elétrico, :math:`\vec{E}`, o elétrons apresenta uma dinâmica similar à uma molécula em um gás.


:math:`\phantom{\text{pargraph}}` De fato, conforme ilustrado na :numref:`drude`, o elétron apresenta uma trajetória aleatória somada ao deslocamento linear na direção do campo elétrico. A cada colisão a velocidade do elétron muda de direção e, com isso, parte da energia fornecida pelo campo elétrico é perdida neste processo de colisão. São precisamente estes processos que dão origem à resistência elétrica oferecida pelo metal à ao processo de condução. Assim, a :eq:`vt` só é válida entre dois eventos de colisão, quando o elétron se desloca livremente sob a ação do campo.


:math:`\phantom{\text{pargraph}}` Considere um conjunto de :math:`N` partículas em um intervalo de tempo :math:`dt`. Quantas destas partículas irão sofrer colisão neste intervalo de tempo? Considerando que as colisões são proporcionais ao tempo, isso significa que o número de colisões (ou, equivalentemente, o número de partículas que colidem) será dado por:

.. math::
  :label: colisoes1

    \begin{aligned}
    \#~\text{de partículas que colidem no intervalo}~dt=\dfrac{N}{\tau}~dt,
    \end{aligned}

onde :math:`\tau` é a constante de proporcionalidade entre o número de partículas e o tempo. Vamos definir o número de partículas que não sofreram colisões no intervalo de tempo entre :math:`0` e :math:`t` como :math:`N(t)` e, assim, o número de partículas que não sofrem colisões no tempo :math:`t+dt` é dada por:

.. math::

    \begin{aligned}
    N(t+dt)=N(t)-N(t)\dfrac{dt}{\tau},
    \end{aligned}

o que pode ser expresso na forma,

.. math::

    \begin{aligned}
    \dfrac{N(t+dt)-N(t)}{dt}=-\dfrac{N(t)}{\tau}\qquad\therefore\qquad \dfrac{dN}{dt}=-\dfrac{N}{\tau},
    \end{aligned}

que é uma equação diferencial de primeira ordem para :math:`N(t)`. Sua solução é bem simples de modo que apenas apresentamos a solução final:

.. math::
  :label: Ntequation

    \begin{aligned}
    N(t)=N_{0}e^{-t/\tau},
    \end{aligned}

onde :math:`N_{0}` é o número de partículas que não sofreram colisão no tempo inicial :math:`t=0`.

:math:`\phantom{\text{pargraph}}` A probabilidade :math:`P(t)` de não ocorrer nenhuma colisão entre os instantes de tempo :math:`0` e :math:`t` é dado pela razão :math:`N(t)/N_{0}`, assim, usando a :eq:`Ntequation`, podemos escrever:

.. math::
  :label: probnaocol

    \begin{aligned}
    P(t)=e^{-t/\tau}.
    \end{aligned}


:math:`\phantom{\text{pargraph}}` Agora consideremos a probabilidade de não ocorrer colisão entre o intervalo de tempo de :math:`0` a :math:`t` e então termos uma colisão no instante posterior :math:`dt`. Neste caso, temos um produto de probabilidades: a probabilidade de não-colisão de :math:`0` a :math:`t` é dado pela :eq:`probnaocol` enquanto a probabilidade de ocorrer a colisão no instane :math:`dt` é simplesmente dado por :math:`dt/\tau`, usando a :eq:`colisoes1`. Assim, escrevemos esta probabilidade, que definimos como :math:`\mathcal{P}(t)~dt`.

.. math::
  :label: probcoldt

    \begin{aligned}
    \mathcal{P}(t)~dt=P(t)\dfrac{dt}{\tau}=e^{-t/\tau}\dfrac{dt}{\tau}
    \end{aligned}


:math:`\phantom{\text{pargraph}}` O tempo médio entre colisões então pode ser determinado usando a probabilidade expressa pela :eq:`probcoldt`, assim, temos que:

.. math::
  :label: tempomedio

    \begin{aligned}
    \langle t\rangle=\int_{0}^{\infty}t\mathcal{P}(t)~dt=\int_{0}^{\infty}t\dfrac{e^{-t/\tau}}{\tau}~dt=\tau.
    \end{aligned}

a integral é bem simples de ser resolvida e, deste modo, deixamos a cargo do leitor a sua solução.

:math:`\phantom{\text{pargraph}}` Assim, vemos que a dinâmica determinada pela equação :eq:`leiNewton` só ocorrerá, na média, dentro do intervalo de tempo dado por :math:`\tau`, o tempo médio entre colisões.

:math:`\phantom{\text{pargraph}}` Tomando a média da :eq:`vt`, segue que:

.. math::
  :label: vt

    \begin{aligned}
    \langle\vec{v}(t)\rangle=\langle\vec{v}_{0}\rangle+\dfrac{q}{m}\vec{E}\langle t\rangle,
    \end{aligned}

e, como após cada evento de colisão, a carga emerge em uma direção diferente, então a média da velocidade inicial é igual a zero. Assim, escrevemos:

.. math::
  :label: mediavel

    \begin{aligned}
    \vec{v}\equiv\langle\vec{v}(t)\rangle=\dfrac{q}{m}\vec{E}\tau,
    \end{aligned}

esta é a velocidade que aparece na expressão que derivamos acima para a densidade de corrente. De fato, quando calculamos a corrente elétrica através de um condutor, estamos assumindo tacitamente que todos os portadores têm a mesma velocidade, que é igual a velocidade média. Substituindo :eq:`mediavel` na :eq:`jotavec`, segue que:


.. math::
   :label: jotavec1

   \begin{aligned}
   \vec{J}=qn\dfrac{q}{m}\vec{E}\tau=\dfrac{q^{2}n\tau}{m}\vec{E}.\end{aligned}

:math:`\phantom{\text{pargraph}}` E, definindo a condutividade :math:`g` na forma:

.. math::
  :label: condutividadedef

    \begin{aligned}
    g=\dfrac{nq^{2}\tau}{m},
    \end{aligned}

podemos reescrever :eq:`jotavec1` na forma:

.. math::

    \begin{aligned}
    \vec{J}=g\vec{E},
    \end{aligned}

que é a lei de Ohm microscópica que apresentamos acima, na :eq:`leideOhm:micro2`.
.

:math:`\phantom{\text{pargraph}}` Vemos, portanto, que a condutividade está relacionada com o tempo médio entre colisões, que uma propriedade intrínseca de cada metal.








































Condições de Contorno para correntes estacionárias
++++++++++++++++++++++++++++++++++++++++++++++++++


:math:`\phantom{\text{pargraph}}` Na :numref:`correntecontorno` temos uma corrente elétrica atravessando dois meios condutores caracterizados por condutividades :math:`g_{1}` e :math:`g_{2}`. Sendo a corrente elétrica estacionária, não há acúmulo de cargas dentro do volume cilíndrico que se encontra na interface entre os dois meios. Assim, neste volume, podemos escrever:

.. math::

   \begin{aligned}
   \oint_{S}\vec{J}\cdot\hat{n}~da=0.\end{aligned}


.. _correntecontorno:

.. figure:: _images/correntecontorno.png
    :scale: 40%
    :align: center

    Interface entre dois meios condutores de condutividades :math:`g_{1}` e :math:`g_{2}`.

:math:`\phantom{\text{pargraph}}` Considerando o volume cilíndrico ilustrado na :numref:`correntecontorno` podemos escrever a integral de :math:`\vec{J}` da seguinte forma:

.. math::

   \begin{aligned}
   \oint_{S}\vec{J}\cdot\hat{n}~da
   =
   \int_{S_{1}}\vec{J}_{1}\cdot\hat{n}_{1}~da
   +
   \int_{S_{2}}\vec{J}_{2}\cdot\hat{n}_{2}~da
   +
   \int_{S_{l}}\vec{J}_{l}\cdot\hat{n}_{l}~da=0\end{aligned}

onde os índices indicam a integração sobre as tampas do cilindro e a
última integral é feita sobre a área lateral.

:math:`\phantom{\text{pargraph}}` Vamos tomar o limite em que o comprimento :math:`L` do cilindro tende a zero, assim, a área lateral não contribui e resta apenas as contribuições das áreas das tampas do cilindro. No limite
:math:`L\rightarrow 0`, :math:`\hat{n}_{1}=-\hat{n}_{2}=\hat{n}`. Assim,

.. math::

   \begin{aligned}
   \int_{S_{1}}\vec{J}_{1}\cdot\hat{n}~da
   -
   \int_{S_{2}}\vec{J}_{2}\cdot\hat{n}~da=0\end{aligned}

e considerando que a área das tampas é muito pequena no limite em que o
volume encerrado pelo cilindro tende a zero, podemos escrever:

.. math::

   \begin{aligned}
   \vec{J}_{1}\cdot\hat{n}=\vec{J}_{2}\cdot\hat{n}.\end{aligned}

:math:`\phantom{\text{pargraph}}` A condição acima pode ser expressa em termos do campo elétrico por meio da lei de Ohm. Considerando a condutividade dos meios, podemos escrever:

.. math::
   :label: condcontorno

   \begin{aligned}
   g_{1}E_{1n}=g_{2}E_{2n}.\end{aligned}

onde utilizamos :math:`E_{jn}=\vec{E}_{j}\cdot\hat{n}` com
:math:`j=1, 2`.

:math:`\phantom{\text{pargraph}}` No caso estacionário, onde as correntes são independentes do tempo, ainda temos :math:`\nabla\times\vec{E}=0`. Deste modo, ainda podemos escrever:

.. math::

   \begin{aligned}
   E_{1t}=E_{2t},\end{aligned}

que é a mesma condição de contorno para a componente tangente à
interface do campo elétrico para os meios dielétricos.

Exemplo Resolvido
-----------------

**1.** *Dois cilindros metálicos coaxiais de comprimento* :math:`L` *e raios* :math:`R_{a}` *e* :math:`R_{b}`, *sendo* :math:`R_{b}>R_{a}`, *estão submetidos a uma  diferença de potencial fixa,* :math:`\varphi_{0}`. *Este potencial dá origem a uma corrente elétrica que flui do cilindro menor para o maior (veja* Fig. *Considere que entre eles existe um meio caracterizado por uma resistividade* :math:`\rho` *e permissividade elétrica* :math:`\varepsilon`. *(a) Calcule a resistência elétrica entre os condutores. (b) Calcule a capacitância. (c) Determine o valor do produto* :math:`RC` *e interprete o resultado.*


.. _exemplofig1:

.. figure:: _images/exemplofig1.png
   :scale: 20%
   :align: center

   Dois cilindros coaxiais de raios :math:`R_{a}` e  :math:`R_{b}`, sendo :math:`R_{b}>R_{a}`. As linhas de corrente ilustram o fluxo de cargas. Ao lado é mostrada uma vista superior identificando os valores dos raios dos cilindros.

.. centered:: **Solução**

(a)

:math:`\phantom{\text{pargraph}}` Como discutimos anteriormente, o comprimento e a área da seção reta que aparecem na equação para a resistência dependem da direção do fluxo da corrente elétrica. Com efeito, considerando o sistema da :numref:`exemplofig1` vemos que o fluxo não está alinhado na direção do comprimento do cilindro, mas perpendicular ao seu comprimento. Assim, para determinar a resistência a partir da :eq:`resistencia:eq2`, vamos adotar o esquema ilustrado na :numref:`exemplofig1`. Vemos que o elemento de resistência é definido por um cilindro de espessura :math:`dr`, raio :math:`R_{a}\leq r\leq R_{b}`, e altura :math:`L`. Considerando os   parâmetros que aparecem na :eq:`resistencia:eq2`, identificamos que o   comprimento :math:`d=dr` pois é o comprimento :math:`dr` que é paralelo ao fluxo da corrente elétrica. Assim, a área que é atravessada pelas cargas é justamente a área lateral do cilindro cujo valor é igual ao perímetro da circunferência de raio :math:`r` multiplicada pela altura :math:`L`. Esta área é o paralelepípedo ilustrado do lado direito da :numref:`exemplofig2`.

.. _exemplofig2:

.. figure:: _images/exemplofig2.png
   :align: center
   :scale: 20%

   Para determinar a resistência do sistema  consideramos um elemento de volume dado por um cilindro de espessura :math:`dr`, raio :math:`r` e altura :math:`L`. Assim, com respeito à equação para a resistência identificamos o comprimento :math:`d=dr` e :math:`A` é a área lateral do cilindro que, aberta, é igual ao paralelepípedo ilustrado na figura ao lado.


:math:`\phantom{\text{pargraph}}` Com estas informações, podemos escrever a resistência para esse elemento de volume do cilindro na forma:

.. math::
   :label: probx1:eq1

   \begin{aligned}
   dR=\rho\dfrac{dr}{2\pi r L}\end{aligned}

:math:`\phantom{\text{pargraph}}` A resistência total é determinada integrando-se a :eq:`probx1:eq1` na região entre os cilindros. Assim,
segue que:

.. math::

   \begin{aligned}
   R=\dfrac{\rho}{2\pi L}\int_{R_{a}}^{R_{b}}\dfrac{dr}{r}\end{aligned}

ou ainda,

.. math::
   :label: probx1:eq2

   \begin{aligned}
   R=\dfrac{\rho}{2\pi L}\ln\dfrac{R_{b}}{R_{a}}.\end{aligned}

(b)

:math:`\phantom{\text{pargraph}}` Vamos agora determinar a capacitância para o sistema de dois   cilindros. Para isso, lembramos que a capacitância é definida pela razão do potencial pela carga elétrica, assim,

.. math::
   :label: eqparaC

     \begin{aligned}
     C=\dfrac{Q}{\Delta\varphi}.\end{aligned}

Precisamos, primeiramente, calcular a diferença de potencial entre os dois cilindros. Para isso, partimos da equação:

.. math::

   \begin{aligned}
   \varphi(R_{b})-\varphi(R_{a})=-\int_{C}\vec{E}\cdot d\vec{l}\end{aligned}

e o campo elétrico pode ser determinado por meio da lei de Gauss. Sabemos que sua forma é igual ao campo devido a um fio de comprimento :math:`L`, assim, temos que:

.. math::

   \begin{aligned}
   \vec{E}=\dfrac{\lambda}{2\pi\varepsilon r}\hat{r},\end{aligned}

onde, obviamente, estamos considerando o :math:`L\gg R_{a},R_{b}`, sem perda de generalidade. O fio no caso é o cilindro de raio :math:`R_{a}`. Se ele tem um comprimento :math:`L` então a densidade de carga é dada por :math:`\lambda=Q/L`, assim, escrevemos a equação acima na forma:

.. math::

   \begin{aligned}
   \vec{E}=\dfrac{Q}{2\pi\varepsilon r L}\hat{r}\end{aligned}

e considerando que o elemento de comprimento é
:math:`d\vec{l}=dr\hat{r}`, então a diferença de potencial é dada

.. math::

   \begin{aligned}
   \varphi(R_{b})-\varphi(R_{a})=-\int_{R_{a}}^{R_{b}}\dfrac{Q}{2\pi\varepsilon r L}\hat{r}\cdot\hat{r}~dr\end{aligned}

o que nos permite obter:

.. math::

   \begin{aligned}
   \varphi(R_{b})-\varphi(R_{a})=-\dfrac{Q}{2\pi\varepsilon L}\int_{R_{a}}^{R_{b}}\dfrac{dr}{r}\end{aligned}

e, efetuando a integração, segue que:

.. math::

   \begin{aligned}
   \varphi(R_{b})-\varphi(R_{a})=-\dfrac{Q}{2\pi\varepsilon L}\ln\dfrac{R_{b}}{R_{a}}\end{aligned}

e como as cargas fluem do cilindro interno para o externo, então
:math:`\varphi(R_{a})>\varphi(R_{b})` e portanto :math:`\Delta 
\varphi=\varphi(R_{a})-\varphi(R_{b})`, tal que :math:`\Delta\varphi>0`.
Assim, escrevemos:

.. math::

   \begin{aligned}
   \Delta\varphi=\dfrac{Q}{2\pi\varepsilon L}\ln\dfrac{R_{b}}{R_{a}}\end{aligned}

Substituindo :math:`\Delta\varphi` na :eq:`eqparaC` obtemos a capacitância:

.. math::
   :label: res:itemb

   \begin{aligned}
   C=\dfrac{2\pi\varepsilon L}{\ln\dfrac{R_{b}}{R_{a}}}.\end{aligned}


(c)

:math:`\phantom{\text{pargraph}}` Neste item é pedido o valor do produto :math:`RC`. Assim, multiplicando :eq:`probx1:eq2` e :eq:`res:itemb` obtemos:

.. math::
   :label: probx1:eq3

     \begin{aligned}
     RC=\dfrac{\rho}{2\pi L}\ln\dfrac{R_{b}}{R_{a}}\dfrac{2\pi\varepsilon L}{\ln\dfrac{R_{b}}{R_{a}}},\end{aligned}

e, simplificando, segue que:

.. math::
   :label: probx1:eq4

     \begin{aligned}
     RC=\rho\varepsilon.\end{aligned}

:math:`\phantom{\text{pargraph}}` Sabemos a que resistência elétrica é medida em ohms e a capacitância em farads. Assim, temos o produto de uma grandeza definida em :math:`V/A` multiplicada por outra dada por :math:`C/V`. Isso nos dá :math:`C/A` e, considerando que o ampére é :math:`C/s`, temos finalmente que o produto :math:`RC` tem dimensão de segundos, ou seja, é um tempo. Além disso, vemos da :eq:`probx1:eq4` que este tempo vale :math:`\rho\varepsilon`, ou seja, depende apenas das característica do meio entre os cilindros. Este tempo característico é muitas vezes chamado de tempo de relaxação e é o tempo que o sistema leva para entrar
em equilíbrio após uma perturbação dependente do tempo. Podemos pensar na transferência de carga entre as duas superfícies do sistema. Quando estabelecemos o potencial entre os dois cilindros, uma corrente constante passou a fluir entre os dois cilindros, e esta corrente só fica constante se uma bateria é acoplada ao sistema para garantir que a diferença de potencial seja constante. Imagine que desligamos a bateria, como resultado, os elétrons que estão depositados no cilindro externo irão se deslocar em relação ao cilindro interno produzindo uma corrente no sentido contrário. No processo haverá dissipação de energia e podemos pensar nisso como um capacitor que está sendo descarregado através de um resistor. A medida que as cargas atravessam o meio, calor é produzido. A energia potencial eletrostática é convertida em calor e, para conservar energia, este potencial tem que ser igual à potência que produz a corrente, assim,

.. math::

   \begin{aligned}
   \Delta\varphi=RI\end{aligned}

além disso, o potencial, como sabemos pode ser escrito como
:math:`\Delta\varphi=Q/C`, assim, escrevemos:

.. math::

   \begin{aligned}
   \dfrac{Q}{C}=RI\qquad\therefore\qquad Q=RCI\end{aligned}

e derivando em relação ao tempo a equação acima, obtemos

.. math::

   \begin{aligned}
   \dfrac{dQ}{dt}=RC\dfrac{dI}{dt}\end{aligned}

e identificando a corrente elétrica no primeiro membro, i.e.,
:math:`I=-dQ/dt`, segue que:

.. math::
   :label: corr:RC

   \begin{aligned}
   \dfrac{dI}{dt}+\dfrac{I}{RC}=0.\end{aligned}

:math:`\phantom{\text{pargraph}}` A equação acima determina como a corrente varia no tempo após a diferença de potencial dos cilindros ser desligada. Podemos resolvê-la facilmente lembrando que a função exponencial é a única função que, derivada, resulta nela mesma. Assim, :math:`I` deve ter a forma :math:`e^{\lambda t}` onde :math:`\lambda` é uma constante a ser determinada. Substituindo-se :math:`I=e^{\lambda t}` na :eq:`corr:RC` segue que:

.. math::

   \begin{aligned}
   \dfrac{d(e^{\lambda t})}{dt}+\dfrac{e^{\lambda t}}{RC}=0\end{aligned}

ou ainda,

.. math::

   \begin{aligned}
   \lambda e^{\lambda t}+\dfrac{e^{\lambda t}}{RC}=0,\qquad\therefore\qquad \lambda=-\dfrac{1}{RC},\end{aligned}

e a solução geral para este equação é da forma :math:`I(t)=Be^{-t/RC}`
onde :math:`B` é uma constante indeterminada pois é uma solução geral.
Podemos fixá-la considerando que no tempo :math:`t=0`, :math:`I=I_{0}`,
ou seja a corrente tem um valor inicial :math:`I_{0}` no momento que a bateria é
desconectada do sistema. Com isso, temos que :math:`B=I_{0}`, e assim,
chegamos a:

.. math::

   \begin{aligned}
   I=I_{0}e^{-t/RC}\end{aligned}

e usando :math:`RC=\rho\varepsilon`, obtemos ainda:

.. math::

   \begin{aligned}
   I=I_{0}e^{-t/\rho\varepsilon}.\end{aligned}

:math:`\phantom{\text{pargraph}}` Num tempo :math:`t\gg RC` a corrente é zero, mas o decaimento depende do material entre os cilindros. Trocando o material entre os cilindros, as curvas para corrente elétrica em função do tempo serão diferentes. Este é o significado físico do produto :math:`RC` que determinamos acima.
