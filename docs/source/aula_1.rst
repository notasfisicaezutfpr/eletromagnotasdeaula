Revisão de Análise Vetorial
===========================

*Leitura Recomendada: M. Spiegel, Ánálise Vetorial com introdução à
análise tensorial*

:math:`\phantom{\text{paragr}}` Neste capítulo faremos uma revisão do formalismo vetorial que será
extensivamente utilizado ao longo do curso. Com efeito, grande parte das
grandezas físicas que caracterizam as propriedades eletromagnéticas da
matéria são vetoriais. Uma grandeza vetorial é completamente
caracterizada especificando o seu módulo, direção e sentido. Desta
forma, o formalismo vetorial oferece um método compacto para se
trabalhar com todas estas informações.

:math:`\phantom{\text{paragr}}` Geometricamente, representamos um vetor por uma seta em um sistema de
coordenadas tridimensional especificado por três coordenadas, por
exemplo as coordenadas :math:`x,y,z` no sistema de coordenadas
retangulares. O vetor é decomposto em componentes em relação a este
sistema de modo que um dado vetor :math:`\vec{V}` apresenta componentes
:math:`V_{x}, V_{y}, V_{z}` onde cada componente está relacionada ao
módulo :math:`V` da seguinte forma: :math:`V_{x}=V\cos\alpha`,
:math:`V_{y}=V\cos\beta` e :math:`V_{z}=V\cos\gamma`, onde
:math:`\alpha`, :math:`\beta` e :math:`\gamma` são os ângulos entre o
vetor e os eixos coordenados. O módulo do vetor é determinado por meio
do teorema de Pitágoras, :math:`V=\sqrt{V^{2}_{x}+V^{2}_{y}+V^{2}_{z}}`
e representa o seu comprimento.

:math:`\phantom{\text{paragr}}` Aqui vale observar que na maioria dos livros o vetor é representado não
como uma seta em cima da variável, como fazemos aqui, mas grafando as
variáveis vetoriais em negrito. Assim, um dado vetor :math:`\vec{V}`
pode ser escrito como :math:`\mathbf{V}`.

:math:`\phantom{\text{paragr}}` Embora a representação gráfica do vetor por meio de uma seta seja
interessante para visualizar a quantidade vetorial, nas operações
básicas com vetores não lançamos mão de tal artifício visto que é
possível representar um vetor em termos de suas componentes de modo
algébrico. No caso de um sistema de coordenadas retangulares, são
definidos três versores (vetores com módulo unitário) paralelos aos
eixos :math:`x`, :math:`y` e :math:`z` denotados, respectivamente, por
:math:`\hat{\mathbf{i}}`, :math:`\hat{\mathbf{j}}` e
:math:`\hat{\mathbf{k}}`. Qualquer vetor pode ser escrito como uma soma
sobre estes três vetores da seguinte forma:

.. math::
   :label: eq1

   \begin{align}
   \vec{V}=V_{x}\hat{\mathbf{i}}+V_{y}\hat{\mathbf{j}}+ V_{z}\hat{\mathbf{k}}.\end{align}


Álgebra Vetorial
----------------

Operações básicas com vetores
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As operações matemáticas de adição, subtração e multiplicação comumente realizadas com grandezas escalares podem ser estendidas para a álgebra vetorial tomando-se obviamente o devido cuidado com as generalizações.

A soma de dois vetores é realizada por meio da soma das componentes correspondentes destes vetores. Assim, sendo :math:`\vec{C}` a soma de dois vetores :math:`\vec{A}` e :math:`\vec{B}`, então podemos escrever,

.. math::

   \begin{aligned}
   \vec{C}=\vec{A}+\vec{B},\end{aligned}

com :math:`C_{x}=A_{x}+B_{x}`, :math:`C_{y}=A_{y}+B_{y}` e
:math:`C_{z}=A_{z}+B_{z}`. Isto é resultado do fato de que os versores
básicos são linearmente independentes, i.e., a multiplicação de um
destes versores por um escalar nunca gera um dos outros versores. Assim,
o resultado é que a soma deve ser realizada componente por componente.

A subtração de vetores é realizada por meio do vetor oposto, i.e., dado um vetor :math:`\vec{A}` então o vetor oposto é obtido multiplicando :math:`\vec{A}` por -1 o que inverte todas as suas componentes e, com isso, o vetor :math:`-\vec{A}` apresenta mesmo módulo e direção mas sentido oposto a :math:`\vec{A}`. Assim, a subtração :math:`\vec{A}-\vec{B}` é feita somando-se o vetor :math:`\vec{A}` com o vetor oposto de :math:`\vec{B}`,

.. math::

   \begin{aligned}
   \vec{A}-\vec{B}=\vec{A}+(-\vec{B}).\end{aligned}


Tanto a adição como a subtração de vetores é comutativa e associativa,
ou seja, dados três vetores quaisquer :math:`\vec{A}`, :math:`\vec{B}` e
:math:`\vec{C}`, as seguintes igualdades são válidas:

.. math::

   \begin{aligned}
   (\vec{A}+\vec{B})+\vec{C}=\vec{A}+(\vec{B}+\vec{C})=\vec{A}+\vec{B}+\vec{C},\qquad \text{associatividade},
   \\
   \vec{A}+\vec{B}=\vec{B}+\vec{A},\qquad \text{comutatividade}.\end{aligned}

Uma vez que discutimos as propriedades básicas da adição e subtração,
vamos considerar agora a operação de multiplicação. Existem várias
possibilidades de multiplicar vetores. Assim, vamos começar com o tipo
de produto mais simples que consiste em multiplicar um vetor por uma
grandeza escalar, o que resulta em um novo vetor. Seja um vetor
:math:`\vec{B}` que é resultado da multiplicação do escalar :math:`c`
pelo vetor :math:`\vec{A}`. Com isso, podemos escrever
:math:`\vec{B}=c\vec{A}` de modo que,

.. math::

   \begin{aligned}
   B_{x}=cA_{x},\quad B_{y}=cA_{y},\quad B_{z}=cA_{z}.\end{aligned}

Quando consideramos a multiplicação de dois vetores, existem duas
possibilidades que são chamados de produtos escalar e vetorial. Vamos
considerar primeiramente o produto escalar, que como o próprio nome
indica, resulta em uma variável escalar. Em alguns livros é comum que
este tipo de produto seja chamado de produto interno ou produto ponto.
Com efeito, sejam dois vetores :math:`\vec{A}` e :math:`\vec{B}`, então
o produto escalar expresso por :math:`\vec{A}\cdot\vec{B}`, é definido
por:

.. math::
   :label: eq2

   \begin{aligned}
   \vec{A}\cdot\vec{B}=AB\cos\theta\end{aligned}

onde :math:`\theta` é o ângulo entre os dois vetores. Note que quando
:math:`\theta=\pi/2`, quando os vetores são perpendiculares, o produto
escalar é zero,

.. math::

   \begin{aligned}
   \vec{A}\cdot\vec{B}=0, \qquad\theta=\pi/2.\end{aligned}

Com base na definição do produto escalar, vemos que o produto dos
versores básicos :math:`\hat{\mathbf{i}}`, :math:`\hat{\mathbf{j}}` e
:math:`\hat{\mathbf{k}}` é trivialmente dado por,

.. math::

   \begin{aligned}
   \hat{\mathbf{i}}\cdot\hat{\mathbf{i}}&=1, \quad\hat{\mathbf{j}}\cdot\hat{\mathbf{i}}=0, \quad\hat{\mathbf{k}}\cdot\hat{\mathbf{i}}=0,
   \\
   \hat{\mathbf{i}}\cdot\hat{\mathbf{j}}&=0, \quad\hat{\mathbf{j}}\cdot\hat{\mathbf{j}}=1, \quad\hat{\mathbf{k}}\cdot\hat{\mathbf{j}}=0,
   \\
   \hat{\mathbf{i}}\cdot\hat{\mathbf{k}}&=0, \quad\hat{\mathbf{j}}\cdot\hat{\mathbf{k}}=0, \quad\hat{\mathbf{k}}\cdot\hat{\mathbf{k}}=1.\end{aligned}

Como o produto escalar é distributivo, então dados
:math:`\vec{A}=A_{x}\hat{\mathbf{i}}+A_{y}\hat{\mathbf{j}}+A_{z}\hat{\mathbf{k}}`
e
:math:`\vec{B}=B_{x}\hat{\mathbf{i}}+B_{y}\hat{\mathbf{j}}+B_{z}\hat{\mathbf{k}}`,
podemos escrever:

.. math::

   \begin{aligned}
   \vec{A}\cdot\vec{B}&=(A_{x}\hat{\mathbf{i}}+A_{y}\hat{\mathbf{j}}+A_{z}\hat{\mathbf{k}})\cdot(B_{x}\hat{\mathbf{i}}+B_{y}\hat{\mathbf{j}}+B_{z}\hat{\mathbf{k}})
   \\                 &=A_{x}B_{x}+A_{y}B_{y}+A_{z}B_{z}.\end{aligned}

Por fim, ressaltamos que o produto escalar é comutativo, o que implica
que:

.. math::

   \begin{aligned}
   \vec{A}\cdot\vec{B}=\vec{B}\cdot\vec{A}.\end{aligned}

O produto vetorial de dois vetores :math:`\vec{A}` e :math:`\vec{B}`
resulta em um terceiro vetor conforme indica o nome do produto vetorial.
Por vezes, este produto também é chamado de produto externo ou produto
cruz. Assim, dados dois vetores :math:`\vec{A}` e :math:`\vec{B}` o
produto vetorial expresso por :math:`\vec{A}\times\vec{B}`, resulta em
um terceiro vetor :math:`\vec{C}` de acordo com a seguinte definição:

.. math::
   :label: eq3

   \begin{aligned}
   \label{eq3}
   \vec{C}=\vec{A}\times\vec{B}=AB\sin\phi\hat{\mathbf{c}}\end{aligned}

onde :math:`\hat{\mathbf{c}}` é um versor (vetor unitário) na direção do
vetor :math:`\vec{C}` cuja direção e sentido é determinado pela seguinte
regra: suponha que o vetor :math:`\vec{A}` gira até :math:`\vec{B}` pelo
menor ângulo possível. Um parafuso de rosca direita girado desta forma
avançará numa direção perpendicular tanto a :math:`\vec{A}` quanto a
:math:`\vec{B}`; o sentido deste avanço é o sentido do vetor
:math:`\vec{C}`  [1]_. Note que os dois vetores :math:`\vec{A}` e
:math:`\vec{B}` produzem um produto vetorial nulo se estes dois vetores
sao paralelos, i.e., se o ângulo :math:`\phi` entre estes vetores é
zero. Além disso, o produto vetorial, em contraste com o produto
escalar, não é comutativo, de modo que a ordem em que é feito o produto
deve ser sempre observada. Com efeito, baseando-se na regra para a
determinação do sentido e direção do produto vetorial, podemos escrever:

.. math::

   \begin{aligned}
   \vec{A}\times\vec{B}=-\vec{B}\times\vec{A}.\end{aligned}

Note que se :math:`\vec{B}=\vec{A}`, então a igualdade acima resulta em

.. math::

   \begin{align}
   \vec{A}\times\vec{A}=0\end{align}


o que é consistente com a definição dada pela :eq:`eq3` e
resulta em um valor nulo para vetores paralelos.

Com base na definição dada pela equação  :eq:`eq3`, podemos
calcular o produto vetorial entre os versores básicos do sistema
retangular de coordenadas:

.. math::

   \begin{aligned}
   \hat{\mathbf{i}}\times\hat{\mathbf{i}}&=0, \quad\hat{\mathbf{j}}\times\hat{\mathbf{i}}=-\hat{\mathbf{k}}, \quad\hat{\mathbf{k}}\times\hat{\mathbf{i}}=\hat{\mathbf{j}},
   \\
   \hat{\mathbf{i}}\times\hat{\mathbf{j}}&=\hat{\mathbf{k}}, \quad\hat{\mathbf{j}}\times\hat{\mathbf{j}}=0, \quad\hat{\mathbf{k}}\times\hat{\mathbf{j}}=-\hat{\mathbf{i}},
   \\
   \hat{\mathbf{i}}\times\hat{\mathbf{k}}&=-\hat{\mathbf{j}}, \quad\hat{\mathbf{j}}\times\hat{\mathbf{k}}=\hat{\mathbf{i}}, \quad\hat{\mathbf{k}}\times\hat{\mathbf{k}}=0.\end{aligned}

Com os resultados acima podemos calcular o produto vetorial de quaisquer
dois vetores :math:`\vec{A}` e :math:`\vec{B}` em termos de suas
componentes, assim, segue que:

.. math::

   \begin{aligned}
   \vec{A}\times\vec{B}=(A_{x}\hat{\mathbf{i}}+A_{y}\hat{\mathbf{j}}+A_{z}\hat{\mathbf{k}})\times(B_{x}\hat{\mathbf{i}}+B_{y}\hat{\mathbf{j}}+B_{z}\hat{\mathbf{k}}),\end{aligned}

e considerando que o produto vetorial é distributivo, podemos escrever:

.. math::

   \begin{aligned}
   \vec{A}\times\vec{B}=
   B_{x}A_{x}\hat{\mathbf{i}}\times\hat{\mathbf{i}}+B_{y}A_{x}\hat{\mathbf{i}}\times\hat{\mathbf{j}}+B_{z}A_{x}\hat{\mathbf{i}}\times\hat{\mathbf{k}}
   \\
   +B_{x}A_{y}\hat{\mathbf{j}}\times\hat{\mathbf{i}}+B_{y}A_{y}\hat{\mathbf{j}}\times\hat{\mathbf{j}}+B_{z}A_{y}\hat{\mathbf{j}}\times\hat{\mathbf{k}}
   \\
   +B_{x}A_{z}\hat{\mathbf{k}}\times\hat{\mathbf{i}}+B_{y}A_{z}\hat{\mathbf{k}}\times\hat{\mathbf{j}}+B_{z}A_{z}\hat{\mathbf{k}}\times\hat{\mathbf{k}}\end{aligned}

ou seja,

.. math::

   \begin{aligned}
   \vec{A}\times\vec{B}=
   B_{y}A_{x}\hat{\mathbf{k}}-B_{z}A_{x}\hat{\mathbf{j}}
   -B_{x}A_{y}\hat{\mathbf{k}}+B_{z}A_{y}\hat{\mathbf{i}}
   +B_{x}A_{z}\hat{\mathbf{j}}-B_{y}A_{z}\hat{\mathbf{i}}
   \\
   =
   (B_{z}A_{y}-B_{y}A_{z})\hat{\mathbf{i}}
   +(B_{x}A_{z}-B_{z}A_{x})\hat{\mathbf{j}}
   +(B_{y}A_{x}-B_{x}A_{y})\hat{\mathbf{k}}\end{aligned}

o que pode ser escrito compactamente na forma:

.. math::

   \begin{aligned}
   \vec{A}\times\vec{B}=
   \left|\begin{array}{ccc}
     \hat{\mathbf{i}} & \hat{\mathbf{j}} & \hat{\mathbf{k}} \\
     A_{x} & A_{y} & A_{z} \\
     B_{x} & B_{y} & B_{z}
   \end{array}\right|.\end{aligned}

As operações de soma, subtração e os produtos escalar e vetorial podem
ser combinados de várias formas. Usando as propriedades destas operações
é relativamente simples obter os resultados destas operações combinadas.
Damos particular relevância a dois tipos de produtos, chamados produtos
triplos, o primeiro deles resulta em um escalar e é dado por
:math:`D=\vec{A}\cdot(\vec{B}\times\vec{C})`, cuja operação pode ser
realizada por meio de um determinante:

.. math::

   \begin{aligned}
   \vec{A}\times\vec{B}=
   \left|
   \begin{array}{ccc}
     A_{x} & A_{y} & A_{z}\\
     B_{x} & B_{y} & B_{z}\\
     C_{x} & C_{y} & C_{z}
   \end{array}
   \right|.\end{aligned}

Este produto é inalterado por meio de uma permutação cíclica dos
elementos do produto, assim, podemos escrever:

.. math::

   \begin{aligned}
   \vec{A}\cdot(\vec{B}\times\vec{C})=\vec{C}\cdot(\vec{A}\times\vec{B})=\vec{B}\cdot(\vec{C}\times\vec{A})\end{aligned}

se estes vetores correspondem às arestas de um paralelepípedo, então o
produto triplo resulta no volume do mesmo.

Outro produto que aparece comumente na teoria eletromagnética é o
produto triplo vetorial que resulta em um vetor :math:`\vec{D}` dado por

.. math::

   \begin{aligned}
   \vec{D}=\vec{A}\times(\vec{B}\times\vec{C})=\vec{B}(\vec{A}\cdot\vec{C})-\vec{C}(\vec{A}\cdot\vec{B}).\end{aligned}

Cálculo Diferencial Vetorial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A diferenciação de funções vetoriais segue as mesmas regras da
diferenciação de uma função escalar, tomando-se cuidado com a inversão
da ordem de produtos vetoriais quando é o caso. Com efeito, dados
:math:`\vec{A}`, :math:`\vec{B}` e :math:`\vec{C}` funções vetoriais de
uma variável :math:`u`, e :math:`\phi` uma função escalar de :math:`u`,
então segue que:

.. math::

   \begin{aligned}
   \dfrac{d}{du}(\vec{A}+\vec{B})&=\dfrac{d\vec{A}}{du}+\dfrac{d\vec{B}}{du}
   \\
   \dfrac{d}{du}(\vec{A}\cdot\vec{B})&=\dfrac{d\vec{A}}{du}\cdot\vec{B}+\vec{A}\cdot\dfrac{d\vec{B}}{du}
   \\
   \dfrac{d}{du}(\vec{A}\times\vec{B})&=\dfrac{d\vec{A}}{du}\times\vec{B}+\vec{A}\times\dfrac{d\vec{B}}{du}
   \\
   \dfrac{d}{du}(\phi\vec{A})&=\phi\dfrac{d\vec{A}}{du}+\vec{A}\dfrac{d\phi}{du}
   \\
   \dfrac{d}{du}[\vec{A}\cdot(\vec{B}\times\vec{C})]&=\dfrac{d\vec{A}}{du}\cdot(\vec{B}\times\vec{C})+\vec{A}\cdot\left(\dfrac{d\vec{B}}{du}\times\vec{C}\right)
   +
   \vec{A}\cdot\left(\vec{B}\times\dfrac{d\vec{C}}{du}\right)\end{aligned}

e outras combinações seguem do mesmo modo.

Além da derivada em relação à variáveis escalares como ilustrado nas
identidades acima, também são definidos os chamados operadores
diferenciais vetoriais, onde através dos quais definimos três
quantidades úteis chamadas *gradiente*, *divergente* e *rotacional*. A
seguir, discutiremos cada uma destas grandezas mais detalhadamente.

Gradiente
^^^^^^^^^

O gradiente é uma quantidade vetorial que determina a direção e sentido
da máxima variação de uma função escalar. Para entender e determinar o
método do cálculo do gradiente de uma dada função é conveniente revisar
o conceito de derivada direcional de uma função de várias variáveis.
Para isso, vamos considerar uma função :math:`\varphi(x,y,z)` cuja
derivada direcional é representada por :math:`d\varphi/ds` onde
:math:`ds` é o módulo do vetor elemento de comprimento
:math:`d\vec{s}=dx\hat{\mathbf{i}}+dy\hat{\mathbf{j}}+dz\hat{\mathbf{k}}`.
Explicitamente, podemos escrever a derivada direcional de acordo como o
limite:

.. math::

   \begin{aligned}
   \dfrac{d\varphi}{ds}&=\lim_{\Delta s\rightarrow0}\left[\dfrac{\varphi(x+\Delta x, y+\Delta y+z+\Delta z)-\varphi(x,y,z)}{\Delta s}\right]
   \\
   &=\dfrac{\partial\varphi}{\partial x}\dfrac{dx}{ds}+\dfrac{\partial\varphi}{\partial y}\dfrac{dy}{ds}+\dfrac{\partial\varphi}{\partial z}\dfrac{dz}{ds},\end{aligned}

que é simplesmente a regra da cadeia para determinar a derivada total de
:math:`\varphi` em relação a :math:`s` que, por sua vez, é função de
:math:`x,y,z`.

Para ilustrar a idéia de uma derivada direcional, considere o caso
particular em que :math:`\varphi(x,y)=x^{2}+y^{2}` que corresponde a um
parabolóide de revolução do espaço tridimensional. Neste caso,
:math:`\varphi` representa a altura :math:`z` em função das variáveis
:math:`x` e :math:`y`. A derivada direcional vai depender da direção e
do sentido no plano :math:`x,y`. Neste caso, podemos especificar a
direção e sentido por meio da derivada de :math:`y` em relação à
:math:`x`, :math:`dy/dx`. Assim, para um dado ponto
:math:`(x_{0},y_{0})`, vamos calcular a derivada direcional para uma
direção fixada por :math:`dy/dx=-x_{0}/y_{0}`:

.. math::

   \begin{aligned}
   \left.\dfrac{d\varphi}{ds}\right|_{x_{0},y_{0}}=\dfrac{\partial\varphi}{\partial x}\dfrac{dx}{ds}+\dfrac{\partial\varphi}{\partial y}\dfrac{dy}{ds}
   =
   \left[2x_{0}-2y_{0}\dfrac{x_{0}}{y_{0}}\right]\left.\dfrac{dx}{ds}\right|_{x_{0},y_{0}}=0.\end{aligned}

e vemos que nesta direção a função :math:`\varphi(x,y)` permanece
constante. Esta direção determina uma trajetória circular no plano
:math:`x,y`. Com efeito, uma circuferência é determinada pela equação

.. math::

   \begin{aligned}
   x^{2}+y^{2}=R^{2}\end{aligned}

e tomando a derivada em relação a :math:`x`, obtemos:

.. math::

   \begin{aligned}
   2x+2y\dfrac{dy}{dx}=0\qquad\therefore\qquad\dfrac{dy}{dx}=-\dfrac{x}{y}.\end{aligned}

Assim, se “caminhamos” no plano :math:`x,y` ao longo de uma trajetória
circular não vemos variação na função :math:`\varphi`. Com efeito, esta
terá um valor dado pelo raio ao quadrado da circunferência que
descrevemos no plano.

Vamos calcular a derivada direcional para uma outra direção. Vamos
fixá-la fazendo :math:`dy/dx=y_{0}/x_{0}`, neste caso, temos que:

.. math::

   \begin{aligned}
   \left.\dfrac{d\varphi}{ds}\right|_{x_{0},y_{0}}=\dfrac{\partial\varphi}{\partial x}\dfrac{dx}{ds}+\dfrac{\partial\varphi}{\partial y}\dfrac{dy}{ds}
   =
   \left[2x_{0}+\dfrac{2y^{2}_{0}}{x_{0}}\right]\left.\dfrac{dx}{ds}\right|_{x_{0},y_{0}}
   =
   2\left[\dfrac{x^{2}_{0}+y^{2}_{0}}{x_{0}}\right]\left.\dfrac{dx}{ds}\right|_{x_{0},y_{0}}\end{aligned}

e agora precisamos determinar a derivada :math:`dx/ds`. Para isso,
lembramos que :math:`ds=\sqrt{dx^{2}+dy^{2}}` o que nos permite
escrever:

.. math::

   \begin{aligned}
   ds=\sqrt{dx^{2}+dy^{2}}=\sqrt{1+\left(\dfrac{dy}{dx}\right)^{2}}dx\qquad\therefore\qquad \dfrac{dx}{ds}=\dfrac{1}{\sqrt{1+\left(\dfrac{dy}{dx}\right)^{2}}}\end{aligned}

e no ponto :math:`x_{0},y_{0}` a derivada acima se reduz a

.. math::

   \begin{aligned}
   \left.\dfrac{dx}{ds}\right|_{x_{0},y_{0}}=\dfrac{1}{\sqrt{1+\dfrac{y^{2}_{0}}{x^{2}_{0}}}}=\dfrac{x_{0}}{\sqrt{x^{2}_{0}+y^{2}_{0}}}\end{aligned}

o que nos permite escrever

.. math::

   \begin{aligned}
   \left.\dfrac{d\varphi}{ds}\right|_{x_{0},y_{0}}
   =
   2\left[\dfrac{x^{2}_{0}+y^{2}_{0}}{x_{0}}\right]\dfrac{x_{0}}{\sqrt{x^{2}_{0}+y^{2}_{0}}}=2\sqrt{x^{2}_{0}+y^{2}_{0}}\end{aligned}

e vemos que nesta direção a derivada direcional é diferente de zero.

A direção especificada por :math:`dy/dx=y_{0}/x_{0}` é a direção radial,
i.e., perpendicular à trajetória que calculamos anteriormente. Para ver
isso, basta notar que a direção radial pode ser modelada por equação de
uma reta,

.. math::

   \begin{aligned}
   y(x)=ax+b\end{aligned}

onde :math:`a` é o coeficiente angular da reta e :math:`b` é o
coeficiente linear. No caso de um círculo centrado na origem, a direção
radial é dada por uma reta saindo da origem de modo que :math:`b=0`

.. math::

   \begin{aligned}
   y=\dfrac{y_{0}}{x_{0}}x\end{aligned}

e :math:`a=y_{0}/x_{0}` é o coeficiente angular da reta, dada por
:math:`\tan\theta`, onde :math:`\theta` é o ângulo que a reta faz com o
eixo :math:`x`. Tomando a derivada da equação acima, obtemos:

.. math::

   \begin{aligned}
   \dfrac{dy}{dx}=\dfrac{y_{0}}{x_{0}}\end{aligned}

que é a direção que havíamos especificado.

É possível perceber que à medida que variamos o coeficiente angular da
reta a direção da mesma varia e, assim, quando o coeficiente angular
:math:`\alpha=-x_{0}/y_{0}` vamos obter uma reta tangente à trajetória
circular que havíamos calculado primeiramente. É possível mostrar que se
especificamos uma direção fazendo :math:`dy/dx=\alpha`, então a derivada
direcional terá a seguinte equação:

.. math::

   \begin{aligned}
   \left.\dfrac{d\varphi}{ds}\right|_{x_{0},y_{0}}
   =
   2(x_{0}+2\alpha y_{0})(1+\alpha^{2})^{-1/2}.\end{aligned}

e analisando a derivada da expressão em relação à :math:`\alpha`,
podemos determinar em que direção a derivada direcional é máxima e
mínima. É possível mostrar que a direção onde a derivada é mínima
corresponde à :math:`dy/dx=-x_{0}/y_{0}` (tangente à trajetória
circular) e é máxima na direção radial para a qual
:math:`dy/dx=y_{0}/x_{0}`.

Podemos definir o gradiente de uma função genérica :math:`\varphi`,
denotado por :math:`\textbf{grad}~\varphi` (ou
:math:`\vec{\nabla}\varphi`) em termos da derivada direcional de acordo
com a seguinte equação:

.. math::
   :label: gradiente

   \begin{aligned}
   \dfrac{d\varphi}{ds}=|\textbf{grad}~\varphi|\cos\theta\end{aligned}

onde :math:`\theta` é o ângulo entre o vetor :math:`d\vec{s}` e o vetor
:math:`\textbf{grad}~\varphi`. Assim, quando :math:`\theta=0` a derivada
direcional é máxima e, por conseguinte, o gradiente aponta nesta direção
e o sentido aponta para onde a variação da função :math:`\varphi` tem
sua máxima variação. Comparando com a :eq:`eq2`, podemos
escrever a :eq:`gradiente` da seguinte forma:

.. math::

   \begin{aligned}
   \dfrac{d\varphi}{ds}=\textbf{grad}\cdot\dfrac{d\vec{s}}{ds}\end{aligned}

e comparando ambos os lados podemos escrever ainda

.. math::

   \begin{aligned}
   \label{dvarphi}
   d\varphi=\textbf{grad}\cdot d\vec{s}\end{aligned}

a diferencial total de :math:`\varphi` pode ser escrita a forma

.. math::

   \begin{aligned}
   d\varphi=\dfrac{\partial\varphi}{\partial x}dx+\dfrac{\partial\varphi}{\partial y}dy+\dfrac{\partial\varphi}{\partial z}dz\end{aligned}

que também pode ser escrita como

.. math::
   :label: eq4

   \begin{aligned}
   d\varphi=\left(\dfrac{\partial\varphi}{\partial x}\hat{\mathbf{i}}+\dfrac{\partial\varphi}{\partial y}\hat{\mathbf{j}}+\dfrac{\partial\varphi}{\partial z}\hat{\mathbf{k}}\right)\cdot(dx\hat{\mathbf{i}}+dy\hat{\mathbf{j}}+dz\hat{\mathbf{k}})\end{aligned}

usando a definição do produto escalar. O segundo membro da :eq:`eq4` pode ser expresso na forma:

.. math::
   :label: eq5

   \begin{aligned}
   d\varphi=[(\textbf{grad})_{x}\hat{\mathbf{i}}+(\textbf{grad})_{y}\hat{\mathbf{j}}+(\textbf{grad})_{z}\hat{\mathbf{k}}]\cdot(dx\hat{\mathbf{i}}+dy\hat{\mathbf{j}}+dz\hat{\mathbf{k}})\end{aligned}

e comparando :eq:`eq4` e :eq:`eq5`, vemos que:

.. math::
   :label: gradienteeq 
   
   \begin{aligned}
   \textbf{grad}~\varphi=\dfrac{\partial\varphi}{\partial x}\hat{\mathbf{i}}+\dfrac{\partial\varphi}{\partial y}\hat{\mathbf{j}}+\dfrac{\partial\varphi}{\partial z}\hat{\mathbf{k}}.\end{aligned}

Esta é a expressão para o gradiente de uma função escalar e é um vetor
que aponta para a direção e sentido onde ocorre a máxima variação de
:math:`\varphi`. Antes de prosseguir, notamos que a expressão acima pode
ser escrita como:

.. math::

   \begin{aligned}
   \label{gradiente:eq2}
   \textbf{grad}~\varphi=\nabla\varphi=\left(\dfrac{\partial}{\partial x}\hat{\mathbf{i}}+\dfrac{\partial}{\partial y}\hat{\mathbf{j}}+\dfrac{\partial}{\partial z}\hat{\mathbf{k}}\right)\varphi\end{aligned}

de onde identificamos o operador diferencial vetorial
:math:`\nabla` (chamado “nabla”)

.. math::

   \begin{aligned}
   \mathbf{\nabla}=\dfrac{\partial}{\partial x}\hat{\mathbf{i}}+\dfrac{\partial}{\partial y}\hat{\mathbf{j}}+\dfrac{\partial}{\partial z}\hat{\mathbf{k}}\end{aligned}

que irá aparecer em outras definições.

Divergente
^^^^^^^^^^

A próxima quantidade relevante é o chamado divergente que é determinado
a partir de uma função vetorial :math:`\vec{F}(x,y,z)`. Com efeito, o
divergente é definido como o limite de uma integral de superfície
fechada :math:`S` que encerra um volume :math:`V` que tende a zero.
Matematicamente, escrevemos:

.. math::
   :label: divergente:def

   \begin{aligned}
   \textbf{div}\vec{F}\equiv\nabla\cdot\vec{F}=\lim_{V\rightarrow0}\left(\dfrac{1}{V}\oint_{S}\vec{F}\cdot\hat{n}~da\right).\end{aligned}

A definição formal dada pela :eq:`divergente:def`
não nos diz muito a respeito do significado físico representado por
:math:`\textbf{div}\vec{F}`, e para obtermos uma expressão mais simples
para o divergente, vamos calcular a integral dada pela Eq.
:eq:`divergente:def` para um volume genérico
:math:`V`. Desde que aplicamos um limite ao término da integração, a
definição é válida para qualquer forma do volume, e por simplicidade,
vamos considerar um paralelepípedo de volume
:math:`V=\Delta x\Delta y\Delta z` com um dos vértices localizados em
:math:`(x_{0},y_{0},z_{0})`. Neste caso, a integral de superfície é
dividida em 6 integrações sobre cada face do volume :math:`V`. O versor
:math:`\hat{n}` é um vetor normal à superfície correspondente, i.e., é
um versor perpendicular ao plano da superfície. No caso de uma
superfície fechada, o versor normal aponta para fora da mesma e para
superfícies abertas e finitas, o sentido do versor é definido pela regra
da mão direita usando o contorno que delimita a superfície. No caso
específico do paralelepípedo, temos 6 versores normais dados por:

.. math::

   \begin{aligned}
   \hat{n}_{1}=\hat{\mathbf{i}},\quad\hat{n}_{2}=-\hat{\mathbf{i}},\quad\hat{n}_{3}=\hat{\mathbf{j}},\quad\hat{n}_{4}=-\hat{\mathbf{j}}\quad\hat{n}_{5}=\hat{\mathbf{k}},\quad\hat{n}_{6}=-\hat{\mathbf{k}}.\\\end{aligned}

Com estes vetores, podemos escrever a integração no divergente da
seguinte forma:

.. math::

   \begin{aligned}
   \oint_{S}\vec{F}\cdot\hat{n}~da=\int_{S_{1}}\vec{F}\cdot\hat{n}_{1}~da+\int_{S_{2}}\vec{F}\cdot\hat{n}_{2}~da+\int_{S_{3}}\vec{F}\cdot\hat{n}_{3}~da
   \\
   +\int_{S_{4}}\vec{F}\cdot\hat{n}_{4}~da+\int_{S_{5}}\vec{F}\cdot\hat{n}_{5}~da+\int_{S_{6}}\vec{F}\cdot\hat{n}_{6}~da\end{aligned}

.. math::

   \begin{aligned}
   \oint_{S}\vec{F}\cdot\hat{n}~da=
   \int\int\vec{F}(x_{0}+\Delta x,y,z)\cdot\hat{\mathbf{i}}~dydz
   -
   \int\int\vec{F}(x_{0},y,z)\cdot\hat{\mathbf{i}}~dydz
   \\+
   \int\int\vec{F}(x,y_{0}+\Delta y,z)\cdot\hat{\mathbf{j}}~dxdz
   -
   \int\int\vec{F}(x,y_{0},z)\cdot\hat{\mathbf{j}}~dxdz
   \\
   +\int\int\vec{F}(x,y,z_{0}+\Delta z)\cdot\hat{\mathbf{k}}~dxdy
   -
   \int\int\vec{F}(x,y,z_{0})\cdot\hat{\mathbf{k}}~dxdy\end{aligned}

e efetuando os produtos escalares, segue que:

.. math::

   \begin{aligned}
   \oint_{S}\vec{F}\cdot\hat{n}~da=
   \int\int F_{x}(x_{0}+\Delta x,y,z)~dydz
   -
   \int\int F_{x}(x_{0},y,z)~dydz
   \\+
   \int\int F_{y}(x,y_{0}+\Delta y,z)~dxdz
   -
   \int\int F_{y}(x,y_{0},z)~dxdz
   \\
   +\int\int F_{z}(x,y,z_{0}+\Delta z)~dxdy
   -
   \int\int F_{z}(x,y,z_{0})~dxdy.\end{aligned}

Para simplificar a expressão acima, expandimos as componentes nas faces
posteriores em série de Taylor até primeira ordem:

.. math::

   \begin{aligned}
   F_{x}(x_{0}+\Delta x,y,z)=F_{x}(x_{0},y,z)+\left.\dfrac{\partial F_{x}}{\partial x}\right|_{x_{0},y,z}\Delta x,
   \\
   F_{y}(x,y_{0}+\Delta y,z)=F_{y}(x,y_{0},z)+\left.\dfrac{\partial F_{y}}{\partial y}\right|_{x,y_{0},z}\Delta y,
   \\
   F_{z}(x,y,z_{0}+\Delta z)=F_{y}(x,y,z_{0})+\left.\dfrac{\partial F_{z}}{\partial z}\right|_{x,y,z_{0}}\Delta z.\end{aligned}

O próximo passo é substituir estas expressões na integração acima,
podemos escrever:

.. math::

   \begin{aligned}
   \oint_{S}\vec{F}\cdot\hat{n}~da=
   \int\int F_{x}(x_{0},y,z)~dydz+\int\int\left.\dfrac{\partial F_{x}}{\partial x}\right|_{x_{0},y,z}\Delta x~dydz
   -
   \int\int F_{x}(x_{0},y,z)~dydz
   \\+
   \int\int F_{y}(x,y_{0},z)~dxdz+\int\int\left.\dfrac{\partial F_{y}}{\partial y}\right|_{x,y_{0},z}\Delta y~dxdz
   -
   \int\int F_{y}(x,y_{0},z)~dxdz
   \\
   +\int\int F_{y}(x,y,z_{0})~dxdy+\int\int \left.\dfrac{\partial F_{z}}{\partial z}\right|_{x,y,z_{0}}\Delta z~dxdy
   -
   \int\int F_{z}(x,y,z_{0})~dxdy.\end{aligned}

.. math::

   \begin{aligned}
   \oint_{S}\vec{F}\cdot\hat{n}~da
   =
   \Delta x\int\int\left.\dfrac{\partial F_{x}}{\partial x}\right|_{x_{0},y,z}~dydz
   +
   \Delta y\int\int\left.\dfrac{\partial F_{y}}{\partial y}\right|_{x,y_{0},z}~dxdz
   \\
   +\Delta z
   \int\int \left.\dfrac{\partial F_{z}}{\partial z}\right|_{x,y,z_{0}}~dxdy.\end{aligned}

Como será aplicado o limite :math:`\Delta V\rightarrow0`, as derivadas
não variam muito e podemos tirá-las da integral, o que nos permite
escrever

.. math::

   \begin{multline*}
   \oint_{S}\vec{F}\cdot\hat{n}~da
   =
   \Delta x\Delta y\Delta z
   \left.\dfrac{\partial F_{x}}{\partial x}\right|_{x_{0},y,z}
   +
   \Delta x\Delta y\Delta z
   \left.\dfrac{\partial F_{y}}{\partial y}\right|_{x,y_{0},z}
   \\+
   \Delta x\Delta y\Delta z
   \left.\dfrac{\partial F_{z}}{\partial z}\right|_{x,y,z_{0}}\end{multline*}

e substituindo na definição do divergente, podemos escrever finalmente

.. math::

   \begin{multline}
   \label{divergente:def22}
   \textbf{div}\vec{F}\equiv\nabla\cdot\vec{F}\\=\lim_{V\rightarrow0}\dfrac{1}{\Delta x \Delta y \Delta z}\Delta x\Delta y\Delta z\left(
   \left.\dfrac{\partial F_{x}}{\partial x}\right|_{x_{0},y,z}
   +
   \left.\dfrac{\partial F_{y}}{\partial y}\right|_{x,y_{0},z}
   +
   \left.\dfrac{\partial F_{z}}{\partial z}\right|_{x,y,z_{0}}\right).\end{multline}

.. math::

   \begin{aligned}
   \label{divergente}
   \textbf{div}\vec{F}\equiv\nabla\cdot\vec{F}=\dfrac{\partial F_{x}}{\partial x}+\dfrac{\partial F_{y}}{\partial y}+\dfrac{\partial F_{z}}{\partial z}.\end{aligned}

que é a expressão largamente utilizada para determinar o divergente.
Note que podemos pensar em um produto escalar do operador nabla com o
vetor :math:`\vec{F}`.

Para finalizar, vamos considerar o significado físico do divergente.
Este é determinado em termos de um volume :math:`V` infinitesimal e está
relacionado com o fluxo do campo vetorial dentro deste volume. Quando o
divergente é diferente de zero, isto implica em um fluxo saindo ou
entrando no volume dependendo se o divergente apresenta um valor
positivo ou negativo. Caso o divergente seja nulo, não há um fluxo
líquido dentro do volume. Para ilustrar este fato, considere que o vetor
:math:`\vec{F}=\vec{E}`, ou seja, representa o campo elétrico. Neste
caso, iremos mostrar futuramente que o fluxo elétrico é dado por:

.. math::

   \begin{aligned}
   \nabla\cdot\vec{E}=\dfrac{\rho}{\epsilon_{0}}\end{aligned}

onde :math:`\rho` é densidade de carga elétrica. Isto nos mostra que
existe um fluxo elétrico líquido apenas em um volume que envolva uma
densidade de carga elétrica. Normalmente, dizemos que o divergente não
nulo representa um fonte ou um sumidouro de fluxo elétrico ou qualquer
outro campo vetorial.

Rotacional
^^^^^^^^^^

Vamos considerar agora o último operador diferencial vetorial que
chamamos de rotacional. Este também é definido em termos de uma
integração sobre um superfície fechada, porém, agora o integrando é
constituído por um produto vetorial. Com efeito, considerando uma
superfície fechada :math:`S` encerrando um volume :math:`V` o rotacional
de uma função vetorial :math:`\vec{F}`, denotado por
:math:`\textbf{rot}\vec{F}` (ou ainda :math:`\nabla\times\vec{F}`), é
definido por:

.. math::

   \begin{aligned}
   \label{rotacional:def}
   \textbf{rot}\vec{F}=\lim_{V\rightarrow0}\left(\dfrac{1}{V}\oint_{S}\hat{n}\times\vec{F}~da\right).\end{aligned}

Novamente, consideramos o volume :math:`V=\Delta x\Delta y\Delta z` na
forma de um paralelepípedo com um dos vértices localizado no ponto
:math:`(x_{0},y_{0},z_{0})`. Aqui, vamos determinar a componente do
rotacional que aponta ao longo da direção :math:`x`. Para isso,
precisamos considerar quais são as componentes do produto vetorial que
aparece na integral de superfície com componentes ao longo do eixo
:math:`x`. Para isso, primeiramente escrevemos o produto vetorial na
forma:

.. math::

   \begin{aligned}
   \hat{n}\times\hat{F}=\hat{n}\times(F_{x}\hat{\mathbf{i}}+F_{y}\hat{\mathbf{j}}+F_{z}\hat{\mathbf{k}})\end{aligned}

e somente os produtos envolvendo as normais ao longo de :math:`y` e
:math:`z` resultarão em um vetor ao longo do eixo :math:`x`. Assim,
usando a mesma notação anterior, vemos que as normais correspondentes
são
:math:`\hat{n}_{3}=\hat{\mathbf{j}},~\hat{n}_{4}=-\hat{\mathbf{j}},~\hat{n}_{5}=\hat{\mathbf{k}},~\hat{n}_{6}=-\hat{\mathbf{k}}`.
Note que :math:`\hat{n}_{3}` e :math:`\hat{n}_{4}` são multiplicados por
:math:`F_{z}\hat{\mathbf{k}}` e :math:`\hat{n}_{5}` e
:math:`\hat{n}_{6}` são multiplicados por :math:`F_{y}\hat{\mathbf{j}}`.
Assim, escrevemos a integral de superfície da seguinte forma:

.. math::

   \begin{gathered}
   \left.\textbf{rot}\vec{F}\right|_{x}=\lim_{V\rightarrow0}\dfrac{1}{\Delta x\Delta y\Delta z}\left.\left(
   \int_{S_{3}}\hat{n}_{3}\times\vec{F}(x,y_{0}+\Delta y,z)~dxdz
   +
   \int_{S_{4}}\hat{n}_{4}\times\vec{F}(x,y_{0},z)~dxdz
   \right.\right.\\\left.\left.+
   \int_{S_{5}}\hat{n}_{5}\times\vec{F}(x,y,z_{0}+\Delta z)~dxdy
   +
   \int_{S_{6}}\hat{n}_{6}\times\vec{F}(x,y,z_{0})~dxdy\right)\right|_{x}.\end{gathered}

.. math::

   \begin{gathered}
   \left.\textbf{rot}\vec{F}\right|_{x}\\=\lim_{V\rightarrow0}\dfrac{1}{\Delta x\Delta y\Delta z}\left.\left(
   \int\int(\hat{\mathbf{j}}\times\hat{\mathbf{k}})F_{z}(x,y_{0}+\Delta y,z)~dxdz
   +
   \int\int(-\hat{\mathbf{j}}\times\hat{\mathbf{k}})F_{z}(x,y_{0},z)~dxdz
   \right.\right.\\\left.\left.+
   \int\int(\hat{\mathbf{k}}\times\hat{\mathbf{j}})F_{y}(x,y,z_{0}+\Delta z)~dxdy
   +
   \int\int(-\hat{\mathbf{k}}\times\hat{\mathbf{j}})F_{y}(x,y,z_{0})~dxdy\right)\right|_{x}.\end{gathered}

.. math::

   \begin{gathered}
   \left.\textbf{rot}\vec{F}\right|_{x}\\=\lim_{V\rightarrow0}\dfrac{1}{\Delta x\Delta y\Delta z}\left.\left(
   \int\int\hat{\mathbf{i}}F_{z}(x,y_{0}+\Delta y,z)~dxdz
   +
   \int\int(-\hat{\mathbf{i}})F_{z}(x,y_{0},z)~dxdz
   \right.\right.\\\left.\left.+
   \int\int(-\hat{\mathbf{i}})F_{y}(x,y,z_{0}+\Delta z)~dxdy
   +
   \int\int\hat{\mathbf{i}}F_{y}(x,y,z_{0})~dxdy\right)\right|_{x}.\end{gathered}

o que pode ser escrito de modo mais compacto na forma:

.. math::

   \begin{gathered}
   \left.\textbf{rot}\vec{F}\right|_{x}\\=\lim_{V\rightarrow0}\dfrac{1}{\Delta x\Delta y\Delta z}\left(
   \int\int F_{z}(x,y_{0}+\Delta y,z)~dxdz
   -
   \int\int F_{z}(x,y_{0},z)~dxdz
   \right.\\\left.-
   \int\int F_{y}(x,y,z_{0}+\Delta z)~dxdy
   +
   \int\int F_{y}(x,y,z_{0})~dxdy\right).\end{gathered}

Aqui novamente, fazemos uso da expansão em série de Taylor em torno do
ponto :math:`x_{0},y_{0},z_{0}` de modo que:

.. math::

   \begin{aligned}
   F_{z}(x,y_{0}+\Delta y,z)= F_{z}(x,y_{0},z)+\dfrac{\partial F_{z}}{\partial y}\Delta y
    \\
   F_{y}(x,y,z_{0}+\Delta z)= F_{y}(x,y,z_{0})+\dfrac{\partial F_{y}}{\partial z}\Delta z\end{aligned}

e com isso a expressão acima pode ser escrita na forma

.. math::

   \begin{gathered}
   \left.\textbf{rot}\vec{F}\right|_{x}\\=\lim_{V\rightarrow0}\dfrac{1}{\Delta x\Delta y\Delta z}\left(
   \int\int F_{z}(x,y_{0},z)~dxdz+\dfrac{\partial F_{z}}{\partial y}\Delta x\Delta y\Delta z
   -
   \int\int F_{z}(x,y_{0},z)~dxdz
   \right.\\\left.-
   \int\int F_{y}(x,y,z_{0})~dxdy-\dfrac{\partial F_{y}}{\partial z}\Delta x\Delta y\Delta z
   +
   \int\int F_{y}(x,y,z_{0})~dxdy\right).\end{gathered}

ou seja,

.. math::

   \begin{aligned}
   \label{eq6}
   \left.\textbf{rot}\vec{F}\right|_{x}=\dfrac{\partial F_{z}}{\partial y}-\dfrac{\partial F_{y}}{\partial z}.\end{aligned}

De maneira análoga é possível demonstrar as demais componentes do
rotacional ao longo de :math:`y` e :math:`z`:

.. math::

   \begin{aligned}
   \label{eq7}
   \left.\textbf{rot}\vec{F}\right|_{y}=\dfrac{\partial F_{x}}{\partial z}-\dfrac{\partial F_{z}}{\partial x},\end{aligned}

.. math::

   \begin{aligned}
   \label{eq8}
   \left.\textbf{rot}\vec{F}\right|_{z}=\dfrac{\partial F_{y}}{\partial x}-\dfrac{\partial F_{x}}{\partial y}.\end{aligned}

Todas as componentes podem ser determinadas simultâneamente por meio do
seguinte determinante:

.. math::

   \begin{aligned}
   \label{det:rot}
   \nabla\times\vec{F}=\left|
   \begin{array}{ccc}
   \hat{\mathbf{i}}&\hat{\mathbf{j}}&\hat{\mathbf{k}}\\
   \dfrac{\partial}{\partial x}&\dfrac{\partial}{\partial y}&\dfrac{\partial}{\partial z}\\
   F_{x}&F_{y}&F_{z}.
   \end{array}
   \right|\end{aligned}

Teoremas úteis
~~~~~~~~~~~~~~

As operações de gradiente, divergente e rotacional eventualmente
aparecem dentro de integrais de linha, superfície e volume. Muitas vezes
é mais conveniente resolver determinado tipo de integral e, desta forma,
alguns teoremas que relacionam diferentes tipos de integrais são
interessantes. Destacamos aqui o chamado Teorema do Divergente e o
Teorema de Stokes.

Teorema do Divergente
^^^^^^^^^^^^^^^^^^^^^

O teorema do divergente estabelece uma igualdade entre uma integral de
superfície e uma integral de volume. Considerando um volume :math:`V`
delimitado por uma superfície fechada :math:`S`, então para uma dada
função vetorial :math:`\vec{F}(x,y,z)` qualquer podemos escrever

.. math::

   \begin{aligned}
   \label{divergente:teorema}
   \int_{V}\nabla\cdot\vec{F}~dV=\oint_{S}\vec{F}\cdot\hat{n}~da.\end{aligned}

A igualdade acima pode ser verificada considerando-se que um volume
:math:`V` pode ser subdividido em :math:`N` subvolumes
:math:`\Delta V_{i}`. Desta forma, a integral sobre a superfície
:math:`S` pode ser considerada como uma soma sobre :math:`N` integrais
de superfície sobre cada subvolume :math:`\Delta V_{i}`. Se cada
subvolume é considerando como sendo um paralelepípedo de volume
:math:`\Delta x_{i}\Delta y_{i}\Delta z_{i}`, notamos que as integrais
sobre as superfícies contidas dentro do volume serão nulas porque cada
face do paralelepípedo tem duas normais (para fora e para dentro) em
sentido opostos e estas integrais se anulam mutuamente. As únicas
integrais que efetivamente contribuem para o resultado final são as
integrais da superfície externa que não são compensadas. Com isso,
podemos escrever:

.. math::

   \begin{aligned}
   \oint_{S}\vec{F}\cdot\hat{n}~da=\lim_{N\rightarrow\infty}\sum_{i=1}^{N}\oint_{S_{i}}\vec{F}\cdot\hat{n}~da\end{aligned}

de modo que quando :math:`N\rightarrow\infty` os elementos de volume
:math:`\Delta V_{i}` tendem a zero. Assim, podemos escrever a igualdade
acima, da seguinte forma:

.. math::

   \begin{aligned}
   \oint_{S}\vec{F}\cdot\hat{n}~da=\sum_{i=1}\left(\lim_{\Delta V_{i}\rightarrow0}\dfrac{1}{\Delta V_{i}}\oint_{S_{i}}\vec{F}\cdot\hat{n}~da\right)\Delta V_{i}\end{aligned}

onde consideramos que o volume :math:`\Delta V_{i}=V/N` e quando
:math:`N\rightarrow\infty`, :math:`\Delta V_{i}\rightarrow0`. O que vai
dentro do parênteses é justamente o divergente de :math:`\vec{F}` e soma
em :math:`i` multiplicada por :math:`\delta V_{i}` resulta em uma
integral de volume. Com isso, podemos escrever:

.. math::

   \begin{aligned}
   \oint_{S}\vec{F}\cdot\hat{n}~da=\sum_{i=1}\left(\lim_{\Delta V_{i}\rightarrow0}\dfrac{1}{\Delta V_{i}}\oint_{S_{i}}\vec{F}\cdot\hat{n}~da\right)\Delta V_{i}=\int_{V}\nabla\cdot\vec{F}~dV.\end{aligned}

Teorema de Stokes
^^^^^^^^^^^^^^^^^

O Teorema de Stokes relaciona uma integral sobre uma superfície aberta
com a integral sobre o caminho que delimita esta superfície. O Teorema
de Stokes é dado por

.. math::

   \begin{aligned}
   \label{Stokes}
   \oint_{C}\vec{F}\cdot d\vec{l}=\int_{S}(\nabla\times\vec{F})\cdot\hat{n}~da.\end{aligned}

Para verificar o Teorema de Stokes, nos valemos do mesmo raciocínio que
fizemos para o caso do teorema do divergente. Consideramos que uma dada
superfície delimitada pelo caminho :math:`C` pode ser subdivida em
pequenos elementos de superfície :math:`\Delta S_{i}` de modo que a
integral de caminho sobre o perímetro da superfície pode ser considerada
como uma soma de todas as integrais de caminho :math:`C_{i}` onde
:math:`C_{i}` é o caminho que delimita a superfície :math:`S_{i}`. Neste
caso, vemos novamente que as integrais internas se cancelam mutamente
restando apenas a contribuição externa, o que nos permite escrever:

.. math::

   \begin{aligned}
   \oint_{C}\vec{F}\cdot d\vec{l}=\sum_{i}\oint_{C_{i}}\vec{F}\cdot d\vec{l}\end{aligned}

fazendo o tamanho das células tender a zero, podemos escrever ainda: [2]_

.. math::

   \begin{aligned}
   \oint_{C}\vec{F}\cdot d\vec{l}&=\lim_{\Delta S_{i}\rightarrow0}\sum_{i}\left(\dfrac{1}{\Delta S_{i}}\oint_{C_{i}}\vec{F}\cdot d\vec{l}\right)\Delta S_{i}\\&=\int_{S}(\nabla\times\vec{F})\cdot\hat{n}~da.\end{aligned}

Desenvolvimentos Adicionais
---------------------------

Existem muitas outras identidades envolvendo combinações de operadores
diferenciais e integrais. Com efeito, é possível verificar outras
identidades vetoriais oriundas da combinação entre operadores
diferenciais e funções escalares e vetoriais. Alguns exemplos são
ilustrados pelas equações no quadro abaixo: 

.. admonition:: Algumas identidades integrais

   .. math::
      :label:  identidades:integrais:eq1

      \begin{aligned}
      \int_{S}\hat{n}\times\nabla\varphi~da&=\oint_{C}\varphi~d\vec{l}\end{aligned}

   .. math::
      :label:  identidades:integrais:eq2

      \begin{aligned}
      \int_{V}\nabla\varphi~dV&=\oint_{S}\varphi \hat{n}~da\end{aligned}
      
   .. math::
      :label:  identidades:integrais:eq3

      \begin{aligned}
      \int_{V}\nabla\times\vec{F}~dV&=\oint_{S}\hat{n}\times\vec{F}~da\end{aligned}

   .. math::
      :label:  identidades:integrais:eq4

      \begin{aligned}
      \int_{V}(\nabla\cdot\vec{G}+\vec{G}\cdot\nabla)\vec{F}~dV&=\oint_{S}\vec{F}(\vec{G}\cdot\hat{n})~da\end{aligned}

Estas identidades são obtidas combinando os Teoremas do Divergente e de
Stokes com as chamadas Identidades Vetoriais Diferenciais, algumas delas
dadas pela equações ilustradas no quadro abaixo:

.. admonition:: Algumas identidades vetoriais

   .. math::
      :label: identidades:diferenciais:eq1

      \begin{aligned}
      \nabla\cdot\nabla\varphi&=\nabla^{2}\varphi~(\text{definição do Laplaciano})\end{aligned}

   .. math::
      :label: identidades:diferenciais:eq2

      \begin{aligned}
      \nabla\cdot\nabla\times\vec{F}&=0
      \end{aligned}

   .. math::
      :label: identidades:diferenciais:eq3
      
      \begin{aligned}
      \nabla\times\nabla\varphi&=0
      \end{aligned}

   .. math::
      :label: identidades:diferenciais:eq4

      \begin{aligned}
      \nabla\times(\nabla\times\vec{F})&=\nabla(\nabla\cdot\vec{F})-\nabla^{2}\vec{F}
      \end{aligned}

   .. math::
      :label: identidades:diferenciais:eq5

      \begin{aligned}
      \nabla(\varphi\psi)&=\psi\nabla\varphi+\varphi\nabla\psi\end{aligned}

Na pg. 31 do livro-texto existem outras identidades adicionais.
Considerando o vetor posição
:math:`\vec{r}=x\hat{\mathbf{i}}+y\hat{\mathbf{j}}+z\hat{\mathbf{k}}`,
algumas identidades são particularmente úteis pois aparecem de modo
recorrente na teoria eletromagnética:

.. math::

   \begin{aligned}
   \label{uteis}
   \nabla\cdot\vec{r}=3
   \\
   \nabla\times\vec{r}=0
   \\
   \vec{G}\cdot\nabla\vec{r}=\vec{G}
   \\
   \nabla^{2}\vec{r}=0.\end{aligned}

Conforme iremos mostrar no próximo capítulo, o potencial devido a cargas
elétricas é um potencial central, ou seja, depende apenas do módulo da
distância em relação à carga de prova. Com isso, iremos lidar com o
cálculo de funções que dependem apenas do módulo do vetor
:math:`\vec{r}`, desde que :math:`r=|\vec{r}|=\sqrt{x^{2}+y^{2}+z^{2}}`.
Assim, para uma dada função escalar genérica :math:`\varphi(r)` ou uma
função vetorial :math:`\vec{F}(r)` valem as seguintes relações:

.. admonition:: Identidades vetoriais úteis

   .. math::
      :label: ids:eq1

      \begin{aligned}
      \nabla\varphi(r)&=\dfrac{\vec{r}}{r}\dfrac{d\varphi}{dr}\end{aligned}

   .. math::
      :label: ids:eq2

      \begin{aligned}
      \nabla\cdot\vec{F}(r)&=\dfrac{\vec{r}}{r}\cdot\dfrac{d\vec{F}}{dr}\end{aligned}
      
   .. math::
      :label: ids:eq3

      \begin{aligned}
      \nabla\varphi(\xi)&=\vec{A}\dfrac{d\varphi}{d\xi}\end{aligned}

onde :math:`\xi=\vec{A}\cdot\vec{r}`, com :math:`\vec{A}` é um vetor
constante.

.. [1]
   Esta é a chamada regra da mão direta que foi mencionada na aula.

.. [2]
   Aqui nos valemos de uma definição alternativa para o rotacional de
   uma função vetorial :math:`\vec{F}`. Com efeito, dado um vetor
   :math:`\vec{a}` normal à uma superfície :math:`S` delimitada por um
   contorno :math:`C`, então a componente do rotacional de
   :math:`\vec{F}` na direção de :math:`\hat{a}` pode ser escrita na
   forma:

   .. math::

      \begin{aligned}
      \vec{a}\cdot(\nabla\times\vec{F})=\lim_{S\rightarrow0}\dfrac{1}{S}\oint_{C}\vec{F}\cdot d\vec{l}\end{aligned}

   dado pela Eq. (1-39) do livro-texto.
